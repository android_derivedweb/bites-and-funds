package com.bites.funds1.API;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;
import com.bites.funds1.Utils.ServerUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by kshitij on 12/17/17.
 */

public class SearchGetUserTransactionListRequest extends StringRequest {

    private Map<String, String> parameters;

    public SearchGetUserTransactionListRequest(int Page ,String search, Response.Listener<String> listener, Response.ErrorListener errorListener) {
        super(Method.GET, ServerUtils.BASE_URL+"user/transaction-history?page="+Page+"&search="+search, listener, errorListener);
        parameters = new HashMap<>();
        // parameters.put("OldPassword ", OldPassword );
      //  parameters.put("UserOrderID", MethodName);
    }


    @Override
    protected Map<String, String> getParams() throws AuthFailureError {
        return parameters;
    }

}
