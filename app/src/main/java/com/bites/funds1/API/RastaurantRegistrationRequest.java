package com.bites.funds1.API;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;
import com.bites.funds1.Utils.ServerUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by kshitij on 12/17/17.
 */

public class RastaurantRegistrationRequest extends StringRequest {

    private Map<String, String> parameters;

    public RastaurantRegistrationRequest(String restaurant_name,
                                         String city_id,
                                         String email,
                                         String password,
                                         String restaurant_type,
                                         String crypto_wallet_token,
                                         Response.Listener<String> listener, Response.ErrorListener errorListener) {
        super(Method.POST, ServerUtils.BASE_URL+"restaurant/register", listener, errorListener);
        parameters = new HashMap<>();
        // parameters.put("OldPassword ", OldPassword );
        parameters.put("restaurant_name", restaurant_name);
        parameters.put("city_id", city_id);
        parameters.put("email", email);
        parameters.put("password", password);
        parameters.put("restaurant_type", restaurant_type);
        parameters.put("crypto_wallet_token", crypto_wallet_token);
    }


    @Override
    protected Map<String, String> getParams() throws AuthFailureError {
        return parameters;
    }

}
