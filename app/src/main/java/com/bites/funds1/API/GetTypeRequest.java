package com.bites.funds1.API;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;
import com.bites.funds1.Utils.ServerUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by kshitij on 12/17/17.
 */

public class GetTypeRequest extends StringRequest {

    private Map<String, String> parameters;

    public GetTypeRequest( Response.Listener<String> listener, Response.ErrorListener errorListener) {
        super(Method.GET, ServerUtils.BASE_URL+"restaurant/get-restaurant-type", listener, errorListener);
        parameters = new HashMap<>();
    }


    @Override
    protected Map<String, String> getParams() throws AuthFailureError {
        return parameters;
    }

}
