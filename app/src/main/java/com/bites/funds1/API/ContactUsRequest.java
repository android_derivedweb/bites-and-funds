package com.bites.funds1.API;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;
import com.bites.funds1.Utils.ServerUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by kshitij on 12/17/17.
 */

public class ContactUsRequest extends StringRequest {

    private Map<String, String> parameters;

    public ContactUsRequest(String first_name,
                            String last_name,
                            String email,
                            String phone,
                            String notes, Response.Listener<String> listener, Response.ErrorListener errorListener) {
        super(Method.POST, ServerUtils.BASE_URL+"contact-us", listener, errorListener);
        parameters = new HashMap<>();
        // parameters.put("OldPassword ", OldPassword );
        parameters.put("first_name", first_name);
        parameters.put("last_name", last_name);
        parameters.put("email", email);
        parameters.put("phone", phone);
        parameters.put("notes", notes);
    }

    @Override
    protected Map<String, String> getParams() throws AuthFailureError {
        return parameters;
    }

}
