package com.bites.funds1.API;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;
import com.bites.funds1.Utils.ServerUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by kshitij on 12/17/17.
 */

public class BuyBiteRequest extends StringRequest {

    private Map<String, String> parameters;

    public BuyBiteRequest(String restaurant_id, String voucher_amount,String stripe_token, Response.Listener<String> listener, Response.ErrorListener errorListener) {
        super(Method.POST, ServerUtils.BASE_URL+"user/buy-bite", listener, errorListener);
        parameters = new HashMap<>();
        // parameters.put("OldPassword ", OldPassword );
        parameters.put("restaurant_id", restaurant_id);
        parameters.put("stripe_token", stripe_token);
        parameters.put("bite_payment_amount", voucher_amount);
    }

    @Override
    protected Map<String, String> getParams() throws AuthFailureError {
        return parameters;
    }

}
