package com.bites.funds1.API;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;
import com.bites.funds1.Utils.ServerUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by kshitij on 12/17/17.
 */

public class UpdateRestaurantProfileRequest extends StringRequest {

    private Map<String, String> parameters;

    public UpdateRestaurantProfileRequest(String restaurant_name,
                                          String description,
                                          String email,
                                          String restaurant_type,
                                          String city_id,
                                          String address,
                                          String percentage_you_want,
                                          String offer,
                                          Response.Listener<String> listener, Response.ErrorListener errorListener) {
        super(Method.POST, ServerUtils.BASE_URL+"restaurant/update-profile", listener, errorListener);
        parameters = new HashMap<>();
        // parameters.put("OldPassword ", OldPassword );
        parameters.put("restaurant_image", null);
        parameters.put("restaurant_name", restaurant_name);
        parameters.put("description", description);
        parameters.put("email", email);
        parameters.put("restaurant_type", restaurant_type);
        parameters.put("city_id", city_id);
        parameters.put("address", address);
        parameters.put("percentage_you_want", percentage_you_want);
        parameters.put("offer", offer);

    }

    @Override
    protected Map<String, String> getParams() throws AuthFailureError {
        return parameters;
    }

}
