package com.bites.funds1.API;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;
import com.bites.funds1.Utils.ServerUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by kshitij on 12/17/17.
 */

public class UserRegistrationRequest extends StringRequest {

    private Map<String, String> parameters;

    public UserRegistrationRequest(String first_name,
            String last_name,
            String email,
            String password,
            String phone_no,
            String crypto_wallet_token,
             Response.Listener<String> listener, Response.ErrorListener errorListener) {
        super(Method.POST, ServerUtils.BASE_URL+"user/register", listener, errorListener);
        parameters = new HashMap<>();
        // parameters.put("OldPassword ", OldPassword );
        parameters.put("first_name", first_name);
        parameters.put("last_name", last_name);
        parameters.put("email", email);
        parameters.put("password", password);
        parameters.put("phone_no", phone_no);
        parameters.put("crypto_wallet_token", crypto_wallet_token);
    }


    @Override
    protected Map<String, String> getParams() throws AuthFailureError {
        return parameters;
    }

}
