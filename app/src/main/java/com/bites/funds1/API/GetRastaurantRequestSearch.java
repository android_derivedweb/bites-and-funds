package com.bites.funds1.API;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;
import com.bites.funds1.Utils.ServerUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by kshitij on 12/17/17.
 */

public class GetRastaurantRequestSearch extends StringRequest {

    private Map<String, String> parameters;

    public GetRastaurantRequestSearch( String search, Response.Listener<String> listener, Response.ErrorListener errorListener) {
      //  super(Method.GET, ServerUtils.BASE_URL+"get-restaurent?latitude="+lat+"&longitude="+mlong+"&page="+Page, listener, errorListener);
        super(Method.GET, ServerUtils.BASE_URL+"get-restaurent?&search_name_city="+search, listener, null);
        parameters = new HashMap<>();
        // parameters.put("OldPassword ", OldPassword );
      //  parameters.put("UserOrderID", MethodName);
    }

    @Override
    protected Map<String, String> getParams() throws AuthFailureError {
        return parameters;
    }

}
