package com.bites.funds1.API;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;
import com.bites.funds1.Utils.ServerUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by kshitij on 12/17/17.
 */

public class UpdateUserProfileRequest extends StringRequest {

    private Map<String, String> parameters;

    public UpdateUserProfileRequest(String first_name,
                                    String last_name,
                                    String email,
                                    String phone_no,
             Response.Listener<String> listener, Response.ErrorListener errorListener) {
        super(Method.POST, ServerUtils.BASE_URL+"user/update-profile", listener, errorListener);
        parameters = new HashMap<>();
        // parameters.put("OldPassword ", OldPassword );
        parameters.put("profile_image", null);
        parameters.put("first_name", first_name);
        parameters.put("last_name", last_name);
        parameters.put("email", email);
        parameters.put("phone_no", phone_no);
    }

    @Override
    protected Map<String, String> getParams() throws AuthFailureError {
        return parameters;
    }

}
