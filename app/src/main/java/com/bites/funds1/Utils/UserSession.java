package com.bites.funds1.Utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import java.util.HashMap;

/**
 * Created by kshitij on 12/18/17.
 */

public class UserSession {

    // Shared Preferences
    SharedPreferences pref;

    // Editor for Shared preferences
    SharedPreferences.Editor editor;

    // Context
    Context context;

    // Shared pref mode
    int PRIVATE_MODE = 0;

    // Sharedpref file name
    private static final String PREF_NAME = "UserSessionPref";

    // First time logic Check

    // All Shared Preferences Keys
    private static final String IS_LOGIN = "IsLoggedIn";
    private String KEY_USER_ID = "KEY_USER_ID";
    private String KEY_PROFILE_PIC = "KEY_PROFILE_PIC";
    private String KEY_FIRST_NAME = "KEY_FIRST_NAME";
    private String KEY_LAST_NAME = "KEY_LAST_NAME";
    private String KEY_EMAIL = "KEY_EMAIL";
    private String KEY_PHONE_NO = "KEY_PHONE_NO";
    private String KEY_APITOKEN = "KEY_APITOKEN";
    private String KEY_LOGIN_TYPE = "KEY_LOGIN_TYPE";


    public UserSession(Context context) {
        this.context = context;
        pref = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }


    public void createLoginSession(String UserId,String ProfilePic,String FirstName,String LastName,String Email,String PhoneNo,String APIToken) {
        // Storing login value as TRUE
        editor.putBoolean(IS_LOGIN, true);
        // Storing name in pref
        editor.putString(KEY_USER_ID, UserId);
        editor.putString(KEY_PROFILE_PIC, ProfilePic);
        editor.putString(KEY_FIRST_NAME, FirstName);
        editor.putString(KEY_LAST_NAME, LastName);
        editor.putString(KEY_EMAIL, Email);
        editor.putString(KEY_PHONE_NO, PhoneNo);
        editor.putString(KEY_APITOKEN, APIToken);


        editor.commit();
    }

    public void LoginType(String Type){
        editor.putString(KEY_LOGIN_TYPE, Type);
        editor.commit();
    }
    public String getUserId() {
        return pref.getString(KEY_USER_ID, "");
    }


    public String getLoginType() {
        return pref.getString(KEY_LOGIN_TYPE, "");
    }


    public String getFirstName() {
        return pref.getString(KEY_FIRST_NAME, "");
    }

    public String getLastName() {
        return pref.getString(KEY_LAST_NAME, "");
    }

    public String getEmail() {
        return pref.getString(KEY_EMAIL, "");
    }

    public String getPhoneNo() {
        return pref.getString(KEY_PHONE_NO, "");
    }

    public String getAPIToken() {
        return pref.getString(KEY_APITOKEN, "");
    }

    public String getProfilePic() {
        return pref.getString(KEY_PROFILE_PIC, "");
    }

    public boolean isLoggedIn() {
        return pref.getBoolean(IS_LOGIN, false);
    }

    public boolean logout() {
        return pref.edit().clear().commit();
    }
}