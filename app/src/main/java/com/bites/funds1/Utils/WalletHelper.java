package com.bites.funds1.Utils;

import android.content.Context;

import milfont.com.tezosj_android.model.TezosWallet;

public class WalletHelper {
    private TezosWallet wallet;

    // Create wallet without supplying mnemonic. This generates a mnemonic randomly
    // and saves to SharedPreferences.
    public WalletHelper(String passphrase, Context ctx) {
        try {
            // use default passphrase if given null
            passphrase = passphrase == null ? "chefsclubcollective" : passphrase;
            // initialize new wallet or recover existing one from mnemonic
            this.wallet = new TezosWallet(passphrase);
            // save to SharedPreferences
            this.wallet.save(ctx);
        } catch (Exception e) {
            this.wallet = null;
            e.printStackTrace();
        }
    }

    // Create wallet with given mnemonic. This is used to restore a wallet and save it
    // to SharedPreferences.
    public WalletHelper(String mnemonic, String passphrase, Context ctx) {
        try {
            // use default passphrase if given null
            passphrase = passphrase == null ? "chefsclubcollective" : passphrase;
//             initialize new wallet or recover existing one from mnemonic
            this.wallet = new TezosWallet(mnemonic, passphrase);
            // save to SharedPreferences
            this.wallet.save(ctx);
        } catch (Exception e) {
           // this.wallet = null;
            e.printStackTrace();
        }
    }

    // Constructor used when restoring from SharedPreferences.
    private WalletHelper(Context ctx, String passphrase) {
        try {
            // use default passphrase if given null
            passphrase = passphrase == null ? "chefsclubcollective" : passphrase;
            // load from SharedPreferences
           // this.wallet = new TezosWallet(ctx, passphrase);
        } catch (Exception e) {
           // this.wallet = null;
            e.printStackTrace();
        }
    }

    public static WalletHelper restoreFromSharedPreferences(Context ctx, String passphrase) {
        try {
            return new WalletHelper(ctx, passphrase);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public String getAddress() {
        return wallet == null ? "Wallet not initialized" : wallet.getPublicKeyHash();
    }

    public String getMnemonic() {
        return wallet == null ? "Wallet not initialized" : wallet.getMnemonicWords();
    }
}