package com.bites.funds1.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bites.funds1.Model.FundsModel;
import com.bites.funds1.R;

import java.util.ArrayList;

public class FundsAdapter extends RecyclerView.Adapter<FundsAdapter.MyViewHolder> {

    private final OnItemClickListener listener;
    private ArrayList<FundsModel> mDataset;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class MyViewHolder extends RecyclerView.ViewHolder {
        private final TextView fund;
        // each data item is just a string in this case
        public TextView user_name;
        public TextView date;


        public MyViewHolder(View v) {
            super(v);
            this.user_name = (TextView) itemView.findViewById(R.id.user_name);
            this.date = (TextView) itemView.findViewById(R.id.date);
            this.fund = (TextView) itemView.findViewById(R.id.fund);
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public FundsAdapter(ArrayList<FundsModel> categoryModels, OnItemClickListener listener) {
        mDataset = categoryModels;
        this.listener = listener;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent,
                                           int viewType) {

        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        // create a new view
        View v = layoutInflater
                .inflate(R.layout.item_funds, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        // holder.textView.setText(mDataset[position]);
        holder.user_name.setText(mDataset.get(position).getUser_name().toUpperCase());
        holder.date.setText(mDataset.get(position).getDate());
        holder.fund.setText(mDataset.get(position).getBite());
      //  holder.img.setBackgroundResource(mmyDataset[position]);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onItemClick(position);
            }
        });


    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public interface OnItemClickListener {
        void onItemClick(int item);
    }


}
