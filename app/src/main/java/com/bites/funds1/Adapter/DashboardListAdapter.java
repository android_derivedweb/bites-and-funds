package com.bites.funds1.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bites.funds1.Utils.UserSession;
import com.bites.funds1.Utils.AppContants;
import com.bites.funds1.Model.DashboardModel;
import com.bites.funds1.R;

import java.util.ArrayList;

public class DashboardListAdapter extends RecyclerView.Adapter<DashboardListAdapter.MyViewHolder> {

    private final OnItemClickListener listener;
    private ArrayList<DashboardModel> mDataset;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class MyViewHolder extends RecyclerView.ViewHolder {

        // each data item is just a string in this case
        public TextView name;
        public TextView date,bite,code,status,open,txt_name;


        public MyViewHolder(View v) {
            super(v);
            this.name = (TextView) itemView.findViewById(R.id.name);
            this.code = (TextView) itemView.findViewById(R.id.code);
            this.bite = (TextView) itemView.findViewById(R.id.bite);
            this.date = (TextView) itemView.findViewById(R.id.date);
            this.status = (TextView) itemView.findViewById(R.id.status);
            this.open = (TextView) itemView.findViewById(R.id.open);
            this.txt_name = (TextView) itemView.findViewById(R.id.txt_name);
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public DashboardListAdapter(ArrayList<DashboardModel> categoryModels, OnItemClickListener listener) {
        mDataset = categoryModels;
        this.listener = listener;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent,
                                           int viewType) {

        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        // create a new view
        View v = layoutInflater
                .inflate(R.layout.item_user_dashboard, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        // holder.textView.setText(mDataset[position]);

        UserSession Session = new UserSession(holder.name.getContext());

        if(Session.getLoginType().equals(AppContants.TYPEUSER)){
            holder.txt_name.setText("Rastaurant Name");
        }else {
            holder.txt_name.setText("User Name");
        }
        holder.name.setText(mDataset.get(position).getRestaurant_name());
        holder.date.setText(mDataset.get(position).getDate());
        holder.bite.setText(mDataset.get(position).getBite());
        holder.code.setText(mDataset.get(position).getVoucher_code());
        holder.status.setText(mDataset.get(position).getStatus().toUpperCase());
      //  holder.img.setBackgroundResource(mmyDataset[position]);
        holder.open.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onItemClick(position);
            }
        });


    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public interface OnItemClickListener {
        void onItemClick(int item);
    }


}
