package com.bites.funds1.Model;

public class RestaurantModel {

    public String restaurant_name;
    public String restaurant_image;
    public String Id;
    private double lat;
    private double Long;

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getRestaurant_name() {
        return restaurant_name;
    }

    public void setRestaurant_name(String restaurant_name) {
        this.restaurant_name = restaurant_name;
    }

    public String getRestaurant_image() {
        return restaurant_image;
    }

    public void setRestaurant_image(String restaurant_image) {
        this.restaurant_image = restaurant_image;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String city;

    public void setLat(double lat) {

        this.lat = lat;
    }

    public double getLat() {
        return lat;
    }

    public void setLong(double aLong) {
        this.Long = aLong;
    }

    public double getaLong() {
        return Long;
    }
}
