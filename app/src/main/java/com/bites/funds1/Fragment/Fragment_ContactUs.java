package com.bites.funds1.Fragment;

import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.bites.funds1.API.ContactUsRequest;
import com.bites.funds1.API.GetCityRequest;
import com.bites.funds1.Activity.Activity_FindRestaurantMap;
import com.bites.funds1.R;
import com.bites.funds1.Utils.ServerUtils;
import com.bites.funds1.Utils.UserSession;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class Fragment_ContactUs extends Fragment {
	private EditText mFirstName,mLastName,mEmail,mPhone_no,mNotes;
	// Store instance variables
	String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
	private UserSession Session;
	private RequestQueue requestQueue;


	// Store instance variables based on arguments passed
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

	}

	// Inflate the view for the fragment based on layout XML
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_contact, container, false);

		requestQueue = Volley.newRequestQueue(getActivity());//Creating the RequestQueue

		mFirstName = view.findViewById(R.id.first_name);
		mLastName = view.findViewById(R.id.last_name);
		mEmail = view.findViewById(R.id.email);
		mPhone_no = view.findViewById(R.id.phone_no);
		mNotes = view.findViewById(R.id.notes);

		Session = new UserSession(getActivity());
		view.findViewById(R.id.submit).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				if (mFirstName.getText().toString().isEmpty()) {
					Toast.makeText(getActivity(), "Please Enter Your FirstName", Toast.LENGTH_SHORT).show();
				}else if (mLastName.getText().toString().isEmpty()) {
					Toast.makeText(getActivity(), "Please Enter Your LastName", Toast.LENGTH_SHORT).show();
				}else if (mEmail.getText().toString().isEmpty()) {
					Toast.makeText(getActivity(), "Please Enter Your Email", Toast.LENGTH_SHORT).show();
				} else if (!mEmail.getText().toString().trim().matches(emailPattern)) {
					Toast.makeText(getActivity(), "Invalid email address", Toast.LENGTH_SHORT).show();
				} else if (mPhone_no.getText().toString().isEmpty()) {
					Toast.makeText(getActivity(), "Please Enter Your Phone Number", Toast.LENGTH_SHORT).show();
				}else if (mNotes.getText().toString().isEmpty()) {
					Toast.makeText(getActivity(), "Please Enter Your Notes", Toast.LENGTH_SHORT).show();
				} else {

					ContactUs(mFirstName.getText().toString(),mLastName.getText().toString(),mEmail.getText().toString(),mPhone_no.getText().toString(),mNotes.getText().toString());

				/*	final KProgressHUD progressDialog = KProgressHUD.create(getActivity())
							.setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
							.setLabel("Please wait")
							.setCancellable(false)
							.setAnimationSpeed(2)
							.setDimAmount(0.5f)
							.show();

					AndroidNetworking.post( ServerUtils.BASE_URL+"contact-us")
							.addBodyParameter("first_name",mFirstName.getText().toString())
							.addBodyParameter("last_name",mLastName.getText().toString())
							.addBodyParameter("email",mEmail.getText().toString())
							.addBodyParameter("phone",mPhone_no.getText().toString())
							.addBodyParameter("notes",mNotes.getText().toString())
							.addHeaders("Accept","application/json")
							.addHeaders("Authorization","Bearer "+Session.getAPIToken())							.build()
							.getAsString(new StringRequestListener() {
								@Override
								public void onResponse(String response) {
									Log.e("Response", response + " null");
									progressDialog.dismiss();


									JSONObject jsonObject = null;
									try {
										jsonObject = new JSONObject(response);
										Toast.makeText(getActivity(),jsonObject.getString("ResponseMsg"),Toast.LENGTH_SHORT).show();
									}catch (Exception e){

									}
								}

								@Override
								public void onError(ANError anError) {

								}
							});*/

				}
			}
		});


		return view;
	}


	protected void replaceFragment(@IdRes int containerViewId,
								   @NonNull Fragment fragment,
								   @NonNull String fragmentTag) {
		getActivity().getSupportFragmentManager()
				.beginTransaction()
				.replace(containerViewId, fragment, fragmentTag)
				.disallowAddToBackStack()
				.commit();
	}



	private void ContactUs(String first_name,String last_name,String email,String phone,String notes)  {

		final KProgressHUD progressDialog = KProgressHUD.create(getActivity())
				.setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
				.setLabel("Please wait")
				.setCancellable(false)
				.setAnimationSpeed(2)
				.setDimAmount(0.5f)
				.show();

		ContactUsRequest loginRequest = new ContactUsRequest(first_name,last_name,email,phone,notes,new Response.Listener<String>() {
			@Override
			public void onResponse(String response) {
				Log.e("Response", response + " null");
				progressDialog.dismiss();


				JSONObject jsonObject = null;
				try {
					jsonObject = new JSONObject(response);
					Toast.makeText(getActivity(),jsonObject.getString("ResponseMsg"),Toast.LENGTH_SHORT).show();
				}catch (Exception e){

				}

			}
		}, new Response.ErrorListener() {
			@Override
			public void onErrorResponse(VolleyError error) {
				error.printStackTrace();
				progressDialog.dismiss();
				if (error instanceof ServerError)
					Toast.makeText(getActivity(), "Server Error", Toast.LENGTH_SHORT).show();
				else if (error instanceof TimeoutError)
					Toast.makeText(getActivity(), "Connection Timed Out", Toast.LENGTH_SHORT).show();
				else if (error instanceof NetworkError)
					Toast.makeText(getActivity(), "Bad Network Connection", Toast.LENGTH_SHORT).show();
			}
		}){
			@Override
			public Map<String, String> getHeaders() throws AuthFailureError {
				Map<String, String> params = new HashMap<String, String>();
				// params.put("Accept", "application/json");
				// params.put("Authorization","Bearer "+ Session.getAPIToken());
				return params;
			}};
		loginRequest.setTag("TAG");
		loginRequest.setShouldCache(false);
		requestQueue.add(loginRequest);

	}


}