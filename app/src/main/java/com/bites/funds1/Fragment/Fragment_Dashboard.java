package com.bites.funds1.Fragment;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.bites.funds1.API.RastaurantDashboardRequest;
import com.bites.funds1.API.SearchRastaurantDashboardRequest;
import com.bites.funds1.API.SearchUserDashboardRequest;
import com.bites.funds1.API.UserDashboardRequest;
import com.bites.funds1.Activity.Activity_BarcodeScanner;
import com.bites.funds1.Activity.Activity_Intro_F;
import com.bites.funds1.Activity.Activity_VoucherDetailsActivity;
import com.bites.funds1.R;
import com.bites.funds1.Utils.EndlessRecyclerViewScrollListener;
import com.bites.funds1.Utils.UserSession;
import com.bites.funds1.Adapter.DashboardListAdapter;
import com.bites.funds1.Model.DashboardModel;
import com.bites.funds1.Utils.AppContants;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Fragment_Dashboard extends Fragment {
	private UserSession Session;
	private RequestQueue requestQueue;
	private int last_size;
	private ArrayList<DashboardModel> categoryModels = new ArrayList<>();
	private RecyclerView category_view;
	private DashboardListAdapter mAdapter;
	private GridLayoutManager gridLayoutManager;
	public static TextView wallet,active_voucher;
	private View view;
	// Store instance variables



	// Store instance variables based on arguments passed
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

	}

	// Inflate the view for the fragment based on layout XML
	@RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		view = inflater.inflate(R.layout.fragment_user_dashboard, container, false);



		view.findViewById(R.id.wallet).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				replaceFragment(R.id.nav_host_fragment, new Fragment_User_VoucherHistory(), "Fragment_User_VoucherHistory");

			}
		});

		TextView txt_name = view.findViewById(R.id.txt_name);
		final TextView search = view.findViewById(R.id.search);
		final TextView search_btn = view.findViewById(R.id.search_btn);
		TextView title = view.findViewById(R.id.title);



		Session = new UserSession(getActivity());

		requestQueue = Volley.newRequestQueue(getActivity());//Creating the RequestQueue

		if(Session.getLoginType().equals(AppContants.TYPEUSER)){
			title.setText("User Dashboard");
			view.findViewById(R.id.wallet).setBackground(view.getResources().getDrawable(R.drawable.txt_red_bg));
		}else {
			//txt_name.setText("User Name");
			title.setText("Restaurant Dashboard");

			view.findViewById(R.id.wallet).setBackground(view.getResources().getDrawable(R.drawable.txt_skyblue_bg));

		}

		view.findViewById(R.id.bottom).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				Fragment_User_Redeemded_VoucherList fragobj = new Fragment_User_Redeemded_VoucherList();
				Bundle bundle = new Bundle();
				bundle.putString("VouchersDetails",active_voucher.getText().toString());
				fragobj.setArguments(bundle);
				replaceFragment(R.id.nav_host_fragment,fragobj, "Fragment_User_Redeemded_VoucherList");

			}
		});


		wallet = view.findViewById(R.id.wallet);
		active_voucher = view.findViewById(R.id.active_voucher);


		category_view = view.findViewById(R.id.category_view);
		mAdapter = new DashboardListAdapter(categoryModels, new DashboardListAdapter.OnItemClickListener() {
			@Override
			public void onItemClick(int item) {
				Intent i = new Intent(getActivity(), Activity_VoucherDetailsActivity.class);
				i.putExtra("Id",categoryModels.get(item).getVoucher_id());
				startActivity(i);
			}
		});
		category_view.setHasFixedSize(true);
		gridLayoutManager = new GridLayoutManager(getActivity().getApplicationContext(), 1);
		category_view.setLayoutManager(gridLayoutManager);
		category_view.setAdapter(mAdapter);
		category_view.setNestedScrollingEnabled(true);


		category_view.addOnScrollListener(new EndlessRecyclerViewScrollListener(gridLayoutManager) {
			@Override
			public void onLoadMore(int page, int totalItemsCount) {

				if (page!=last_size){
					Log.e("FinalOAgeSIze",""+page);
					int finalintpage = page +1;
					if(Session.getLoginType().equals(AppContants.TYPEUSER)){
						view.findViewById(R.id.qr).setVisibility(View.INVISIBLE);
						GetDashboard(finalintpage);
					}else {
						GetDashboard2(finalintpage);
					}

				}
			}
		});


	/*	if(Session.getLoginType().equals(AppContants.TYPEUSER)){
			view.findViewById(R.id.qr).setVisibility(View.INVISIBLE);
			GetDashboard(1);
		}else {
			GetDashboard2(1);
		}
*/
		view.findViewById(R.id.qr).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {

				selectImage();



			}
		});

		search_btn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				if(search.getText().toString().isEmpty()){
					Toast.makeText(getActivity(),"Please enter some word for search...",Toast.LENGTH_SHORT).show();
				}else {
					if(Session.getLoginType().equals(AppContants.TYPEUSER)){
						GetDashboard_Search_User(1,search.getText().toString());
					}else {
						GetDashboard_Search_Restaurant(1,search.getText().toString());
					}


				}
			}
		});


		search.addTextChangedListener(new TextWatcher() {
			@Override
			public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

			}

			@Override
			public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

				if(charSequence.toString().isEmpty()){
					categoryModels.clear();

					if(Session.getLoginType().equals(AppContants.TYPEUSER)){
						GetDashboard(1);
					}else {
						GetDashboard2(1);
					}
				}

			}

			@Override
			public void afterTextChanged(Editable editable) {

			}
		});


		return view;
	}

	private void selectImage() {
		try {
			PackageManager pm = getActivity().getPackageManager();
			int hasPerm = pm.checkPermission(Manifest.permission.CAMERA, getActivity().getPackageName());
			if (hasPerm == PackageManager.PERMISSION_GRANTED) {
				Intent i = new Intent(getActivity(), Activity_BarcodeScanner.class);
				startActivity(i);
			} else
				checkAndroidVersion();
			//Toast.makeText(getActivity(), "Camera Permission error", Toast.LENGTH_SHORT).show();
		} catch (Exception e) {
			checkAndroidVersion();
			//Toast.makeText(getActivity(), "Camera Permission error", Toast.LENGTH_SHORT).show();
			e.printStackTrace();
		}
	}


	private void checkAndroidVersion() {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
			checkAndRequestPermissions();

		} else {
			// code for lollipop and pre-lollipop devices
		}

	}


	private boolean checkAndRequestPermissions() {
		int camera = ContextCompat.checkSelfPermission(getActivity(),
				Manifest.permission.CAMERA);
		int wtite = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE);
		int read = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE);
		List<String> listPermissionsNeeded = new ArrayList<>();
		if (wtite != PackageManager.PERMISSION_GRANTED) {
			listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
		}
		if (camera != PackageManager.PERMISSION_GRANTED) {
			listPermissionsNeeded.add(Manifest.permission.CAMERA);
		}
		if (read != PackageManager.PERMISSION_GRANTED) {
			listPermissionsNeeded.add(Manifest.permission.READ_EXTERNAL_STORAGE);
		}
		if (!listPermissionsNeeded.isEmpty()) {
			ActivityCompat.requestPermissions(getActivity(), listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), REQUEST_ID_MULTIPLE_PERMISSIONS);
			return false;
		}
		return true;
	}

	public static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 7;

	@Override
	public void onRequestPermissionsResult(int requestCode,
										   String permissions[], int[] grantResults) {
		Log.d("in fragment on request", "Permission callback called-------");
		switch (requestCode) {
			case REQUEST_ID_MULTIPLE_PERMISSIONS: {

				Map<String, Integer> perms = new HashMap<>();
				// Initialize the map with both permissions
				perms.put(Manifest.permission.WRITE_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
				perms.put(Manifest.permission.CAMERA, PackageManager.PERMISSION_GRANTED);
				perms.put(Manifest.permission.READ_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
				// Fill with actual results from user
				if (grantResults.length > 0) {
					for (int i = 0; i < permissions.length; i++)
						perms.put(permissions[i], grantResults[i]);
					// Check for both permissions
					if (perms.get(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
							&& perms.get(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED && perms.get(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
						Log.d("in fragment on request", "CAMERA & WRITE_EXTERNAL_STORAGE READ_EXTERNAL_STORAGE permission granted");
						// process the normal flow
						//else any one or both the permissions are not granted
					} else {
						Log.d("in fragment on request", "Some permissions are not granted ask again ");
						//permission is denied (this is the first time, when "never ask again" is not checked) so ask again explaining the usage of permission
//                        // shouldShowRequestPermissionRationale will return true
						//show the dialog or snackbar saying its necessary and try again otherwise proceed with setup.
						if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) || ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.CAMERA) || ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE)) {
							showDialogOK("Camera and Storage Permission required for this app",
									new DialogInterface.OnClickListener() {
										@Override
										public void onClick(DialogInterface dialog, int which) {
											switch (which) {
												case DialogInterface.BUTTON_POSITIVE:
													checkAndRequestPermissions();
													break;
												case DialogInterface.BUTTON_NEGATIVE:
													// proceed with logic by disabling the related features or quit the app.
													break;
											}
										}
									});
						}
						//permission is denied (and never ask again is  checked)
						//shouldShowRequestPermissionRationale will return false
						else {
							Toast.makeText(getActivity(), "Go to settings and enable permissions", Toast.LENGTH_LONG)
									.show();
							//                            //proceed with logic by disabling the related features or quit the app.
						}
					}
				}
			}
		}

	}

	private void showDialogOK(String message, DialogInterface.OnClickListener okListener) {
		new AlertDialog.Builder(getActivity())
				.setMessage(message)
				.setPositiveButton("OK", okListener)
				.setNegativeButton("Cancel", okListener)
				.create()
				.show();
	}




	protected void replaceFragment(@IdRes int containerViewId,
								   @NonNull Fragment fragment,
								   @NonNull String fragmentTag) {
		getActivity().getSupportFragmentManager()
				.beginTransaction()
				.replace(containerViewId, fragment, fragmentTag)
				.addToBackStack(fragmentTag)
				.commit();
	}

	private void GetDashboard(int page)  {


		Log.e("TOken",Session.getAPIToken());
		final KProgressHUD progressDialog = KProgressHUD.create(getActivity())
				.setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
				.setLabel("Please wait")
				.setCancellable(false)
				.setAnimationSpeed(2)
				.setDimAmount(0.5f)
				.show();

		UserDashboardRequest loginRequest = new UserDashboardRequest(page,new Response.Listener<String>() {
			@SuppressLint("SetTextI18n")
			@Override
			public void onResponse(String response) {
				Log.e("Response", response + " null");
				progressDialog.dismiss();

				categoryModels.clear();

				try {

					JSONObject jsonObject = new JSONObject(response);
					// Toast.makeText(AllCategories.this,jsonObject.getString("ResponseMsg"),Toast.LENGTH_SHORT).show();
					if (jsonObject.getString("ResponseCode").equals("200")) {

						JSONObject objectJSONObject = jsonObject.getJSONObject("data");

						wallet.setText(objectJSONObject.getString("bite_in_wallet") + " BITE  \n in Wallet");
						active_voucher.setText(objectJSONObject.getString("total_active_voucher_bite") + " BITE \n in " +objectJSONObject.getString("active_voucher") + " Active Vouchers");
						JSONObject redeemed_vouchers = objectJSONObject.getJSONObject("vouchers");
						JSONArray jsonArray = redeemed_vouchers.getJSONArray("data");

						last_size = redeemed_vouchers.getInt("last_page");
						for(int i =0 ; i<jsonArray.length();i++){
							JSONObject object = jsonArray.getJSONObject(i);

							DashboardModel rastaurantModel = new DashboardModel();
							rastaurantModel.setVoucher_id(object.getString("voucher_id"));
							rastaurantModel.setRestaurant_name(object.getString("restaurant_name"));
							rastaurantModel.setVoucher_code(object.getString("voucher_code"));
							rastaurantModel.setBite(object.getString("bite"));
							rastaurantModel.setDate(object.getString("date"));
							rastaurantModel.setStatus(object.getString("status"));
							categoryModels.add(rastaurantModel);
						}


						mAdapter.notifyDataSetChanged();
					}


				}catch (Exception e){

					Toast.makeText(getActivity(),e.getMessage(),Toast.LENGTH_SHORT).show();
				}


			}
		}, new Response.ErrorListener() {
			@Override
			public void onErrorResponse(VolleyError error) {
				error.printStackTrace();
				progressDialog.dismiss();


				if (error instanceof ServerError)
					Toast.makeText(getActivity(), "Server Error", Toast.LENGTH_SHORT).show();
				else if (error instanceof TimeoutError)
					Toast.makeText(getActivity(), "Connection Timed Out", Toast.LENGTH_SHORT).show();
				else if (error instanceof NetworkError)
					Toast.makeText(getActivity(), "Bad Network Connection", Toast.LENGTH_SHORT).show();
			}
		}){
			@Override
			public Map<String, String> getHeaders() throws AuthFailureError {
				Map<String, String> params = new HashMap<String, String>();
				params.put("Accept", "application/json");
				params.put("Authorization","Bearer "+ Session.getAPIToken());
				return params;
			}};
		loginRequest.setTag("TAG");loginRequest.setShouldCache(false);
		requestQueue.add(loginRequest);

	}

	private void GetDashboard2(int page)  {

		final KProgressHUD progressDialog = KProgressHUD.create(getActivity())
				.setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
				.setLabel("Please wait")
				.setCancellable(false)
				.setAnimationSpeed(2)
				.setDimAmount(0.5f)
				.show();

		RastaurantDashboardRequest loginRequest = new RastaurantDashboardRequest(page,new Response.Listener<String>() {
			@SuppressLint("SetTextI18n")
			@Override
			public void onResponse(String response) {
				Log.e("Response", response + " null");
				progressDialog.dismiss();

				categoryModels.clear();

				try {

					JSONObject jsonObject = new JSONObject(response);
					// Toast.makeText(AllCategories.this,jsonObject.getString("ResponseMsg"),Toast.LENGTH_SHORT).show();
					if (jsonObject.getString("ResponseCode").equals("200")) {

						JSONObject objectJSONObject = jsonObject.getJSONObject("data");

						wallet.setText(objectJSONObject.getString("bite_in_wallet") + " BITE \n in Wallet");
						active_voucher.setText(objectJSONObject.getString("total_active_voucher_bite") + " BITE \n in " +objectJSONObject.getString("active_voucher") + " Active Vouchers");
						JSONObject redeemed_vouchers = objectJSONObject.getJSONObject("vouchers");
						JSONArray jsonArray = redeemed_vouchers.getJSONArray("data");

						last_size = redeemed_vouchers.getInt("last_page");
						for(int i =0 ; i<jsonArray.length();i++){
							JSONObject object = jsonArray.getJSONObject(i);

							DashboardModel rastaurantModel = new DashboardModel();
							rastaurantModel.setVoucher_id(object.getString("voucher_id"));
							rastaurantModel.setRestaurant_name(object.getString("name"));
							rastaurantModel.setVoucher_code(object.getString("voucher_code"));
							rastaurantModel.setBite(object.getString("bite"));
							rastaurantModel.setDate(object.getString("date"));
							rastaurantModel.setStatus(object.getString("status"));
							categoryModels.add(rastaurantModel);
						}


						mAdapter.notifyDataSetChanged();
					}


				}catch (Exception e){
					Toast.makeText(getActivity(),e.getMessage(),Toast.LENGTH_SHORT).show();

				}


			}
		}, new Response.ErrorListener() {
			@Override
			public void onErrorResponse(VolleyError error) {
				error.printStackTrace();
				progressDialog.dismiss();
				if (error instanceof ServerError)
					Toast.makeText(getActivity(), "Server Error", Toast.LENGTH_SHORT).show();
				else if (error instanceof TimeoutError)
					Toast.makeText(getActivity(), "Connection Timed Out", Toast.LENGTH_SHORT).show();
				else if (error instanceof NetworkError)
					Toast.makeText(getActivity(), "Bad Network Connection", Toast.LENGTH_SHORT).show();
			}
		}){
			@Override
			public Map<String, String> getHeaders() throws AuthFailureError {
				Map<String, String> params = new HashMap<String, String>();
				params.put("Accept", "application/json");
				params.put("Authorization","Bearer "+ Session.getAPIToken());
				return params;
			}};
		loginRequest.setTag("TAG");loginRequest.setShouldCache(false);
		requestQueue.add(loginRequest);

	}

	private void GetDashboard_Search_Restaurant(int page,String Search)  {

		final KProgressHUD progressDialog = KProgressHUD.create(getActivity())
				.setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
				.setLabel("Please wait")
				.setCancellable(false)
				.setAnimationSpeed(2)
				.setDimAmount(0.5f)
				.show();

		SearchRastaurantDashboardRequest loginRequest = new SearchRastaurantDashboardRequest(page,Search,new Response.Listener<String>() {
			@SuppressLint("SetTextI18n")
			@Override
			public void onResponse(String response) {
				Log.e("Response", response + " null");
				progressDialog.dismiss();
				categoryModels.clear();


				try {

					JSONObject jsonObject = new JSONObject(response);
					Toast.makeText(getActivity(),jsonObject.getString("ResponseMsg"),Toast.LENGTH_SHORT).show();
					if (jsonObject.getString("ResponseCode").equals("200")) {

						JSONObject objectJSONObject = jsonObject.getJSONObject("data");

						wallet.setText(objectJSONObject.getString("bite_in_wallet") + " BITE \n in Wallet");
						active_voucher.setText(objectJSONObject.getString("total_active_voucher_bite") + " BITE \n in " +objectJSONObject.getString("active_voucher") + " Active Vouchers");
						JSONObject redeemed_vouchers = objectJSONObject.getJSONObject("vouchers");
						JSONArray jsonArray = redeemed_vouchers.getJSONArray("data");

						last_size = redeemed_vouchers.getInt("last_page");
						for(int i =0 ; i<jsonArray.length();i++){
							JSONObject object = jsonArray.getJSONObject(i);

							DashboardModel rastaurantModel = new DashboardModel();
							rastaurantModel.setVoucher_id(object.getString("voucher_id"));
							rastaurantModel.setRestaurant_name(object.getString("name"));
							rastaurantModel.setVoucher_code(object.getString("voucher_code"));
							rastaurantModel.setBite(object.getString("bite"));
							rastaurantModel.setDate(object.getString("date"));
							rastaurantModel.setStatus(object.getString("status"));
							categoryModels.add(rastaurantModel);
						}


						mAdapter.notifyDataSetChanged();
					}


				}catch (Exception e){
					Toast.makeText(getActivity(),e.getMessage(),Toast.LENGTH_SHORT).show();

				}


			}
		}, new Response.ErrorListener() {
			@Override
			public void onErrorResponse(VolleyError error) {
				error.printStackTrace();
				progressDialog.dismiss();
				if (error instanceof ServerError)
					Toast.makeText(getActivity(), "Server Error", Toast.LENGTH_SHORT).show();
				else if (error instanceof TimeoutError)
					Toast.makeText(getActivity(), "Connection Timed Out", Toast.LENGTH_SHORT).show();
				else if (error instanceof NetworkError)
					Toast.makeText(getActivity(), "Bad Network Connection", Toast.LENGTH_SHORT).show();
			}
		}){
			@Override
			public Map<String, String> getHeaders() throws AuthFailureError {
				Map<String, String> params = new HashMap<String, String>();
				params.put("Accept", "application/json");
				params.put("Authorization","Bearer "+ Session.getAPIToken());
				return params;
			}};
		loginRequest.setTag("TAG");loginRequest.setShouldCache(false);
		requestQueue.add(loginRequest);

	}

	private void GetDashboard_Search_User(int page,String search)  {


		Log.e("TOken",Session.getAPIToken());
		final KProgressHUD progressDialog = KProgressHUD.create(getActivity())
				.setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
				.setLabel("Please wait")
				.setCancellable(false)
				.setAnimationSpeed(2)
				.setDimAmount(0.5f)
				.show();

		SearchUserDashboardRequest loginRequest = new SearchUserDashboardRequest(page,search,new Response.Listener<String>() {
			@SuppressLint("SetTextI18n")
			@Override
			public void onResponse(String response) {
				Log.e("Response", response + " null");
				progressDialog.dismiss();
				categoryModels.clear();



				try {

					JSONObject jsonObject = new JSONObject(response);
					 Toast.makeText(getActivity(),jsonObject.getString("ResponseMsg"),Toast.LENGTH_SHORT).show();
					if (jsonObject.getString("ResponseCode").equals("200")) {

						JSONObject objectJSONObject = jsonObject.getJSONObject("data");

						wallet.setText(objectJSONObject.getString("bite_in_wallet") + " BITE  \n in Wallet");
						active_voucher.setText(objectJSONObject.getString("total_active_voucher_bite") + " BITE \n in " +objectJSONObject.getString("active_voucher") + " Active Vouchers");
						JSONObject redeemed_vouchers = objectJSONObject.getJSONObject("vouchers");
						JSONArray jsonArray = redeemed_vouchers.getJSONArray("data");

						last_size = redeemed_vouchers.getInt("last_page");
						for(int i =0 ; i<jsonArray.length();i++){
							JSONObject object = jsonArray.getJSONObject(i);

							DashboardModel rastaurantModel = new DashboardModel();
							rastaurantModel.setVoucher_id(object.getString("voucher_id"));
							rastaurantModel.setRestaurant_name(object.getString("restaurant_name"));
							rastaurantModel.setVoucher_code(object.getString("voucher_code"));
							rastaurantModel.setBite(object.getString("bite"));
							rastaurantModel.setDate(object.getString("date"));
							rastaurantModel.setStatus(object.getString("status"));
							categoryModels.add(rastaurantModel);
						}


						mAdapter.notifyDataSetChanged();
					}


				}catch (Exception e){

					Toast.makeText(getActivity(),e.getMessage(),Toast.LENGTH_SHORT).show();
				}


			}
		}, new Response.ErrorListener() {
			@Override
			public void onErrorResponse(VolleyError error) {
				error.printStackTrace();
				progressDialog.dismiss();
				if (error instanceof ServerError)
					Toast.makeText(getActivity(), "Server Error", Toast.LENGTH_SHORT).show();
				else if (error instanceof TimeoutError)
					Toast.makeText(getActivity(), "Connection Timed Out", Toast.LENGTH_SHORT).show();
				else if (error instanceof NetworkError)
					Toast.makeText(getActivity(), "Bad Network Connection", Toast.LENGTH_SHORT).show();
			}
		}){
			@Override
			public Map<String, String> getHeaders() throws AuthFailureError {
				Map<String, String> params = new HashMap<String, String>();
				params.put("Accept", "application/json");
				params.put("Authorization","Bearer "+ Session.getAPIToken());
				return params;
			}};
		loginRequest.setTag("TAG");loginRequest.setShouldCache(false);
		requestQueue.add(loginRequest);

	}




	@Override
	public void onResume() {
		super.onResume();


		if(Session.getLoginType().equals(AppContants.TYPEUSER)){
			view.findViewById(R.id.qr).setVisibility(View.INVISIBLE);
			GetDashboard(1);
		}else {
			GetDashboard2(1);
		}
	}
}