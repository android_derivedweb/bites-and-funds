package com.bites.funds1.Fragment;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.bites.funds1.API.ChangePasswordRequest;
import com.bites.funds1.API.ChangePasswordRequest2;
import com.bites.funds1.API.GetCityRequest;
import com.bites.funds1.API.GetResProfileRequest;
import com.bites.funds1.API.GetStatesRequest;
import com.bites.funds1.API.GetTypeRequest;
import com.bites.funds1.API.UpdateRestaurantProfileRequest;
import com.bites.funds1.API.UpdateUserProfileRequest;
import com.bites.funds1.API.VolleyMultipartRequest;
import com.bites.funds1.Activity.Activity_ResturantRegistrations;
import com.bites.funds1.Adapter.SpinAdapter;
import com.bites.funds1.Model.NameModel;
import com.bites.funds1.R;
import com.bites.funds1.Utils.ServerUtils;
import com.bites.funds1.Utils.UserSession;
import com.bumptech.glide.Glide;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.subgraph.orchid.dashboard.Dashboard;
/*
import com.schibstedspain.leku.LocationPickerActivity;
*/

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

public class Fragment_RestaurantProfile extends Fragment {
	private TextView edit_profile,change_password;
	private RelativeLayout edit_profile_layout,change_password_layout;
	private KProgressHUD progressDialog;
	private UserSession Session;
	private EditText FirstName,LastName,Email,PhoneNumber,OldPass,NewPass,New2Pass;
	// Store instance variables
	private String mCityId,mTypeId,mOffersId;
	private static final int PERMISSION_REQUEST_CODE = 200;

	private Spinner mType,mCity,mState;
	private String strLat = "40.730610";
	private String strLng = "-73.935242";
	String[] country = {"50","60","70","80","90","100"};

	String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
	private RequestQueue requestQueue;
	private ImageButton buttonBilAddress;
	private EditText mAddress;
	private String mCityName,mRestaurant_Type,mStateName;
	private String mCityNameID,mRestaurant_TypeID,mStateNameID;
	private ArrayList<NameModel> mDataset_City = new ArrayList<>();
	private ArrayList<NameModel> mDataset_State = new ArrayList<>();
	private ArrayList<NameModel> mDataset_Type = new ArrayList<>();

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

	}

	// Inflate the view for the fragment based on layout XML
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_profile_2, container, false);

		edit_profile = view.findViewById(R.id.edit_profile);
		edit_profile_layout = view.findViewById(R.id.edit_profile_layout);
		change_password = view.findViewById(R.id.change_password);
		change_password_layout = view.findViewById(R.id.change_password_layout);




		Session = new UserSession(getActivity());

		requestQueue = Volley.newRequestQueue(getActivity());//Creating the RequestQueue


		mType = view.findViewById(R.id.type);
		mCity =  view.findViewById(R.id.city);
		mState =  view.findViewById(R.id.state);
		
		
		FirstName = view.findViewById(R.id.rs_name);
		LastName = view.findViewById(R.id.rs_dec);
		Email = view.findViewById(R.id.rs_email);
		PhoneNumber = view.findViewById(R.id.offers_txt);


		FirstName.setText(Session.getFirstName());
		LastName.setText(Session.getLastName());
		Email.setText(Session.getEmail());
		PhoneNumber.setText(Session.getPhoneNo());




		OldPass = view.findViewById(R.id.old);
		NewPass = view.findViewById(R.id.new1);
		New2Pass = view.findViewById(R.id.new2);


		Spinner spin = (Spinner) view.findViewById(R.id.offers);
		//Creating the ArrayAdapter instance having the country list
		ArrayAdapter aa = new ArrayAdapter(getActivity(), android.R.layout.simple_spinner_item, country);
		aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		//Setting the ArrayAdapter data on the Spinner
		spin.setAdapter(aa);

		spin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

				mOffersId = country[i];
			}

			@Override
			public void onNothingSelected(AdapterView<?> adapterView) {

			}
		});


		RastaurantLogin();



		mCity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
									   int position, long id) {
				mCityId = mDataset_City.get(position).getId();
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				// TODO Auto-generated method stub
			}
		});


		mState.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
									   int position, long id) {


				GetCity(mDataset_State.get(position).getId());
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				// TODO Auto-generated method stub
			}
		});

		mType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
									   int position, long id) {
				mTypeId = mDataset_Type.get(position).getId();

			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				// TODO Auto-generated method stub
			}
		});


		edit_profile.setOnClickListener(new View.OnClickListener() {
			@RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
			@Override
			public void onClick(View view) {
				edit_profile.setBackground(getActivity().getResources().getDrawable(R.drawable.white_fill_border));
				change_password.setBackground(getActivity().getResources().getDrawable(R.drawable.white_blank_border));
				edit_profile.setTextColor(getActivity().getResources().getColor(R.color.colorPrimary));
				change_password.setTextColor(getActivity().getResources().getColor(R.color.white));
				edit_profile_layout.setVisibility(View.VISIBLE);
				change_password_layout.setVisibility(View.GONE);
			}
		});

		change_password.setOnClickListener(new View.OnClickListener() {
			@RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
			@Override
			public void onClick(View view) {
				edit_profile.setBackground(getActivity().getResources().getDrawable(R.drawable.white_blank_border));
				change_password.setBackground(getActivity().getResources().getDrawable(R.drawable.white_fill_border));
				edit_profile.setTextColor(getActivity().getResources().getColor(R.color.white));
				change_password.setTextColor(getActivity().getResources().getColor(R.color.colorPrimary));
				edit_profile_layout.setVisibility(View.GONE);
				change_password_layout.setVisibility(View.VISIBLE);
			}
		});

		upload_result = view.findViewById(R.id.user_img);
		view.findViewById(R.id.edit_icon).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				selectImage();
			}
		});


		view.findViewById(R.id.update2).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {

				if(OldPass.getText().toString().isEmpty()){
					Toast.makeText(getActivity(),"Please Enter Your Old Password",Toast.LENGTH_SHORT).show();
				}else if(NewPass.getText().toString().isEmpty()){
					Toast.makeText(getActivity(),"Please Enter Your New Password",Toast.LENGTH_SHORT).show();
				}else if(New2Pass.getText().toString().isEmpty()){
					Toast.makeText(getActivity(),"Please Enter Your Email",Toast.LENGTH_SHORT).show();
				}else if(!New2Pass.getText().toString().equals(NewPass.getText().toString())){
					Toast.makeText(getActivity(),"Password does not match",Toast.LENGTH_SHORT).show();

				}else {
					ChangePassword();
				}
			}
		});

		view.findViewById(R.id.update).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {

				if(FirstName.getText().toString().isEmpty()){
					Toast.makeText(getActivity(),"Please Enter Your Rastaurant Name",Toast.LENGTH_SHORT).show();
				}else if(LastName.getText().toString().isEmpty()){
					Toast.makeText(getActivity(),"Please Enter Your Rastaurant Descriptions",Toast.LENGTH_SHORT).show();
				}else if(Email.getText().toString().isEmpty()){
					Toast.makeText(getActivity(),"Please Enter Rastaurant Email",Toast.LENGTH_SHORT).show();
				}else if (!Email.getText().toString().trim().matches(emailPattern)) {
					Toast.makeText(getActivity(),"Invalid email address",Toast.LENGTH_SHORT).show();
				}else if(mAddress.getText().toString().isEmpty()){
					Toast.makeText(getActivity(),"Please Enter Your Rastaurant Address",Toast.LENGTH_SHORT).show();
				}else if(PhoneNumber.getText().toString().isEmpty()){
					Toast.makeText(getActivity(),"Please Enter Your Rastaurant Offers",Toast.LENGTH_SHORT).show();
				}else {
					getCategories();
				}
			}
		});
		mAddress = (EditText) view.findViewById(R.id.address);
		buttonBilAddress = (ImageButton) view.findViewById(R.id.buttonBilAddress);
		buttonBilAddress.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				/*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
					if (!checkPermission()) {
						requestPermission();
					} else {
						double lat = Double.valueOf(strLat);
						double lng = Double.valueOf(strLng);
						Intent locationPickerIntent = new LocationPickerActivity.Builder()
								.withLocation(lat, lng)
								.withGeolocApiKey(getString(R.string.google_maps_key))
								.withGooglePlacesEnabled()
								.withSatelliteViewHidden()
								.withVoiceSearchHidden()
								.shouldReturnOkOnBackPressed()
								.build(getActivity());
						startActivityForResult(locationPickerIntent, 99);
					}
				} *//*else {
					double lat = Double.valueOf(strLat);
					double lng = Double.valueOf(strLng);
					Intent locationPickerIntent = new LocationPickerActivity.Builder()
							.withLocation(lat, lng)
							.withGeolocApiKey(getString(R.string.google_maps_key))
							.withGooglePlacesEnabled()
							.withSatelliteViewHidden()
							.withVoiceSearchHidden()
							.shouldReturnOkOnBackPressed()
							.build(getActivity());
					startActivityForResult(locationPickerIntent, 99);
				}*/
			}
		});


		return view;
	}


	protected void replaceFragment(@IdRes int containerViewId,
								   @NonNull Fragment fragment,
								   @NonNull String fragmentTag) {
		getActivity().getSupportFragmentManager()
				.beginTransaction()
				.replace(containerViewId, fragment, fragmentTag)
				.disallowAddToBackStack()
				.commit();
	}


	private Bitmap bitmap = null;
	private File destination = null;
	private InputStream inputStreamImg;
	private String imgPath = null;
	private final int PICK_IMAGE_CAMERA = 1, PICK_IMAGE_GALLERY = 2;
	private ImageView upload_result, re_change, upload_img;


	// Select image from camera and gallery
	private void selectImage() {
		try {
			PackageManager pm = getActivity().getPackageManager();
			int hasPerm = pm.checkPermission(Manifest.permission.CAMERA, getActivity().getPackageName());
			if (hasPerm == PackageManager.PERMISSION_GRANTED) {
				final CharSequence[] options = {getString(R.string.take_photo), getString(R.string.gallary), getString(R.string.cancel)};
				AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
				builder.setTitle(R.string.select_option);
				builder.setItems(options, new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int item) {
						if (options[item].equals(getResources().getString(R.string.take_photo))) {
							dialog.dismiss();
							Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
							startActivityForResult(intent, PICK_IMAGE_CAMERA);
						} else if (options[item].equals(getResources().getString(R.string.gallary))) {
							dialog.dismiss();
							Intent pickPhoto = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
							startActivityForResult(pickPhoto, PICK_IMAGE_GALLERY);
						} else if (options[item].equals(getResources().getString(R.string.cancel))) {
							dialog.dismiss();
						}
					}
				});
				builder.show();
			} else
				checkAndroidVersion();
			//Toast.makeText(getActivity(), "Camera Permission error", Toast.LENGTH_SHORT).show();
		} catch (Exception e) {
			checkAndroidVersion();
			//Toast.makeText(getActivity(), "Camera Permission error", Toast.LENGTH_SHORT).show();
			e.printStackTrace();
		}
	}

	@SuppressLint("SetTextI18n")
	@RequiresApi(api = Build.VERSION_CODES.KITKAT)
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		inputStreamImg = null;
		if (requestCode == PICK_IMAGE_CAMERA) {
			try {
				Uri selectedImage = data.getData();
				bitmap = (Bitmap) data.getExtras().get("data");
				ByteArrayOutputStream bytes = new ByteArrayOutputStream();
				bitmap.compress(Bitmap.CompressFormat.JPEG, 50, bytes);

				Log.e("Activity", "Pick from Camera::>>> ");

				File folder = new File(Environment.getExternalStorageDirectory() +
						File.separator);
				if (!folder.exists()) {
					folder.mkdirs();
				}
				String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
				destination = new File(Environment.getExternalStorageDirectory() + "/" , "IMG_" + timeStamp + ".jpg");
				FileOutputStream fo;
				try {
					destination.createNewFile();
					fo = new FileOutputStream(destination);
					fo.write(bytes.toByteArray());
					fo.close();
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}

				Log.e("ImageData",bitmap + "  " + destination);

				Glide.with(Objects.requireNonNull(getActivity())).asBitmap().load(bitmap).circleCrop().into(upload_result);


				/*upload_img.setVisibility(View.INVISIBLE);
				re_change.setVisibility(View.VISIBLE);
				imgPath = destination.getAbsolutePath();
				upload_result.setImageBitmap(bitmap);
				Glide.with(Objects.requireNonNull(getActivity())).asBitmap().load(bitmap).circleCrop().into(upload_result);
				//txt_injury.setText(imgPath);*/
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else if (requestCode == PICK_IMAGE_GALLERY) {
			Uri selectedImage = data.getData();
			try {
				bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), selectedImage);
				ByteArrayOutputStream bytes = new ByteArrayOutputStream();
				bitmap.compress(Bitmap.CompressFormat.JPEG, 50, bytes);
				Log.e("Activity", "Pick from Gallery::>>> ");

				imgPath = getRealPathFromURI(selectedImage);
				destination = new File(imgPath.toString());
				//  txt_injury.setText(imgPath);
				Log.e("ImageData",bitmap + "  " + destination);Glide.with(Objects.requireNonNull(getActivity())).asBitmap().load(bitmap).circleCrop().into(upload_result);
			/*	Glide.with(Objects.requireNonNull(getActivity())).asBitmap().load(bitmap).circleCrop().into(upload_result);
				upload_result.setImageBitmap(bitmap);
				upload_img.setVisibility(View.INVISIBLE);
				re_change.setVisibility(View.VISIBLE);*/
			} catch (Exception e) {
				e.printStackTrace();
			}
		}else if (requestCode == 99) {
			if (data != null) {
				double latitude = data.getDoubleExtra("latitude", 0.0);
				strLat = String.valueOf(latitude);
				double longitude = data.getDoubleExtra("longitude", 0.0);
				strLng = String.valueOf(longitude);
				Geocoder geocoder = new Geocoder(getActivity(), Locale.getDefault());
				List<Address> addresses = new ArrayList<>();
				try {
					addresses = geocoder.getFromLocation(latitude, longitude, 1);
				} catch (IOException e) {
					e.printStackTrace();
				}
				String strNewAddress = addresses.get(0).getSubThoroughfare() + ", " + addresses.get(0).getThoroughfare();
				String strNewCity;
				String strNewState;
				String strNewCountry;
				String strNewPostal;
				if (strNewAddress.contains(",")) {
					strNewAddress = strNewAddress.replace(",", " ");
				}
				if (addresses.get(0).getLocality() != null) {
					strNewCity = addresses.get(0).getLocality();
				} else {
					strNewCity = addresses.get(0).getSubLocality();
				}
				if (addresses.get(0).getAdminArea() != null) {
					strNewState = addresses.get(0).getAdminArea();
				} else {
					strNewState = addresses.get(0).getSubAdminArea();
				}
				strNewCountry = addresses.get(0).getCountryName();
				strNewPostal = addresses.get(0).getPostalCode();
				/*Log.d("TAG_LATITUDE****", strNewLat);
				Log.d("TAG_LONGITUDE****", strNewLng);
				Log.d("TAG_ADDRESS****", strNewAddress);
				Log.d("TAG_CITY****", strNewCity);
				Log.d("TAG_STATE****", strNewState);
				Log.d("TAG_COUNTRY****", strNewCountry);
				Log.d("TAG_POSTAL****", strNewPostal);*/
				mAddress.setText(strNewAddress + " , "+strNewCity+" , "+strNewState+" , "+strNewCountry+" , "+strNewPostal);
			} else {
				Log.d("RESULT", "CANCELLED");
			}
		}

	}

	public String getRealPathFromURI(Uri contentUri) {
		String[] proj = {MediaStore.Audio.Media.DATA};
		Cursor cursor = getActivity().managedQuery(contentUri, proj, null, null, null);
		int column_index = cursor.getColumnIndexOrThrow(MediaStore.Audio.Media.DATA);
		cursor.moveToFirst();
		return cursor.getString(column_index);
	}

	private void checkAndroidVersion() {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
			checkAndRequestPermissions();

		} else {
			// code for lollipop and pre-lollipop devices
		}

	}

	private boolean checkAndRequestPermissions() {
		int camera = ContextCompat.checkSelfPermission(getActivity(),
				Manifest.permission.CAMERA);
		int wtite = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE);
		int read = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE);
		List<String> listPermissionsNeeded = new ArrayList<>();
		if (wtite != PackageManager.PERMISSION_GRANTED) {
			listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
		}
		if (camera != PackageManager.PERMISSION_GRANTED) {
			listPermissionsNeeded.add(Manifest.permission.CAMERA);
		}
		if (read != PackageManager.PERMISSION_GRANTED) {
			listPermissionsNeeded.add(Manifest.permission.READ_EXTERNAL_STORAGE);
		}
		if (!listPermissionsNeeded.isEmpty()) {
			ActivityCompat.requestPermissions(getActivity(), listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), REQUEST_ID_MULTIPLE_PERMISSIONS);
			return false;
		}
		return true;
	}

	public static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 7;

	@Override
	public void onRequestPermissionsResult(int requestCode,
										   String permissions[], int[] grantResults) {
		Log.d("in fragment on request", "Permission callback called-------");
		switch (requestCode) {
			case PERMISSION_REQUEST_CODE:
				if (grantResults.length > 0) {
					boolean cameraAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
					boolean locationFineAccepted = grantResults[1] == PackageManager.PERMISSION_GRANTED;
					boolean locationCoarseAccepted = grantResults[2] == PackageManager.PERMISSION_GRANTED;
					boolean callAccepted = grantResults[3] == PackageManager.PERMISSION_GRANTED;
					boolean readAccepted = grantResults[4] == PackageManager.PERMISSION_GRANTED;
					boolean writeAccepted = grantResults[5] == PackageManager.PERMISSION_GRANTED;
					if (cameraAccepted &&
							locationFineAccepted &&
							locationCoarseAccepted &&
							callAccepted &&
							readAccepted &&
							writeAccepted)
						Toast.makeText(getActivity(), "Permission Granted.",Toast.LENGTH_SHORT).show();
					else {
						Toast.makeText(getActivity(), "Permission Denied.",Toast.LENGTH_SHORT).show();
					}
				}
				break;
			case REQUEST_ID_MULTIPLE_PERMISSIONS: {

				Map<String, Integer> perms = new HashMap<>();
				// Initialize the map with both permissions
				perms.put(Manifest.permission.WRITE_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
				perms.put(Manifest.permission.CAMERA, PackageManager.PERMISSION_GRANTED);
				perms.put(Manifest.permission.READ_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
				// Fill with actual results from user
				if (grantResults.length > 0) {
					for (int i = 0; i < permissions.length; i++)
						perms.put(permissions[i], grantResults[i]);
					// Check for both permissions
					if (perms.get(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
							&& perms.get(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED && perms.get(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
						Log.d("in fragment on request", "CAMERA & WRITE_EXTERNAL_STORAGE READ_EXTERNAL_STORAGE permission granted");
						// process the normal flow
						//else any one or both the permissions are not granted
					} else {
						Log.d("in fragment on request", "Some permissions are not granted ask again ");
						//permission is denied (this is the first time, when "never ask again" is not checked) so ask again explaining the usage of permission
//                        // shouldShowRequestPermissionRationale will return true
						//show the dialog or snackbar saying its necessary and try again otherwise proceed with setup.
						if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) || ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.CAMERA) || ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE)) {
							showDialogOK("Camera and Storage Permission required for this app",
									new DialogInterface.OnClickListener() {
										@Override
										public void onClick(DialogInterface dialog, int which) {
											switch (which) {
												case DialogInterface.BUTTON_POSITIVE:
													checkAndRequestPermissions();
													break;
												case DialogInterface.BUTTON_NEGATIVE:
													// proceed with logic by disabling the related features or quit the app.
													break;
											}
										}
									});
						}
						//permission is denied (and never ask again is  checked)
						//shouldShowRequestPermissionRationale will return false
						else {
							Toast.makeText(getActivity(), "Go to settings and enable permissions", Toast.LENGTH_LONG)
									.show();
							//                            //proceed with logic by disabling the related features or quit the app.
						}
					}
				}
			}
		}

	}

	private void showDialogOK(String message, DialogInterface.OnClickListener okListener) {
		new AlertDialog.Builder(getActivity())
				.setMessage(message)
				.setPositiveButton("OK", okListener)
				.setNegativeButton("Cancel", okListener)
				.create()
				.show();
	}




	private void UpdateProfile2() {

		final KProgressHUD progressDialog = KProgressHUD.create(getActivity())
				.setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
				.setLabel("Please wait")
				.setCancellable(false)
				.setAnimationSpeed(2)
				.setDimAmount(0.5f)
				.show();

		UpdateRestaurantProfileRequest loginRequest = new UpdateRestaurantProfileRequest(
				FirstName.getText().toString(),
				LastName.getText().toString(),
				Email.getText().toString(),
				mTypeId,
				mCityId,
				mAddress.getText().toString(),
				mOffersId,
				PhoneNumber.getText().toString(),new Response.Listener<String>() {
			@Override
			public void onResponse(String response) {
				Log.e("Response : ", response);
				progressDialog.dismiss();
				try {
					JSONObject jsonObject = new JSONObject(response);


					if (jsonObject.getInt("ResponseCode") == 200) {
						Toast.makeText(getActivity(), jsonObject.getString("ResponseMsg"), Toast.LENGTH_SHORT).show();

						RastaurantLogin();
// Reload current fragment
					} else {

						Toast.makeText(getActivity(), jsonObject.getString("ResponseMsg"), Toast.LENGTH_SHORT).show();

					}
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		}, new Response.ErrorListener() {
			@Override
			public void onErrorResponse(VolleyError error) {
				error.printStackTrace();
				progressDialog.dismiss();
				if (error instanceof ServerError)
					Toast.makeText(getActivity(), "Server Error", Toast.LENGTH_SHORT).show();
				else if (error instanceof TimeoutError)
					Toast.makeText(getActivity(), "Connection Timed Out", Toast.LENGTH_SHORT).show();
				else if (error instanceof NetworkError)
					Toast.makeText(getActivity(), "Bad Network Connection", Toast.LENGTH_SHORT).show();
			}
		}) {
			@Override
			public Map<String, String> getHeaders() throws AuthFailureError {
				Map<String, String> params = new HashMap<String, String>();
				params.put("Accept", "application/json");
				params.put("Authorization","Bearer "+ Session.getAPIToken());
				return params;
			}
		};
		loginRequest.setTag("TAG");
		loginRequest.setShouldCache(false);
		requestQueue.add(loginRequest);

	}

/*
	public void ChangeProfile() {
		progressDialog = KProgressHUD.create(getActivity())
				.setStyle(KProgressHUD.Style.PIE_DETERMINATE)
				.setLabel("Please wait")
				.setCancellable(false)
				.setAnimationSpeed(2)
				.setDimAmount(0.5f);

		progressDialog.show();


		AndroidNetworking.upload(ServerUtils.BASE_URL + "restaurant/change-password")
				.addHeaders("Accept", "application/json")
				.addHeaders("Authorization", "Bearer " + Session.getAPIToken())
				.addMultipartParameter("old_password", OldPass.getText().toString())
				.addMultipartParameter("password", NewPass.getText().toString())
				.setTag("uploadTest")
				.setPriority(Priority.HIGH)
				.build()
				.setUploadProgressListener(new UploadProgressListener() {
					@Override
					public void onProgress(long bytesUploaded, long totalBytes) {
						// do anything with progress
					}
				}).getAsString(new StringRequestListener() {
			@Override
			public void onResponse(String response) {
				Log.e("Response : ", response);
				progressDialog.dismiss();
				try {
					JSONObject jsonObject = new JSONObject(response);


					if (jsonObject.getInt("ResponseCode") == 200) {
						Toast.makeText(getActivity(), jsonObject.getString("ResponseMsg"), Toast.LENGTH_SHORT).show();

// Reload current fragment
					} else {

						Toast.makeText(getActivity(), jsonObject.getString("ResponseMsg"), Toast.LENGTH_SHORT).show();

					}
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}

			@Override
			public void onError(ANError anError) {
				progressDialog.dismiss();
				Log.e("Error", anError.getErrorBody());
				Toast.makeText(getActivity(), "Unauthenticated", Toast.LENGTH_SHORT).show();
			}
		});

	}
*/

	private void ChangePassword() {

		final KProgressHUD progressDialog = KProgressHUD.create(getActivity())
				.setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
				.setLabel("Please wait")
				.setCancellable(false)
				.setAnimationSpeed(2)
				.setDimAmount(0.5f)
				.show();

		ChangePasswordRequest2 loginRequest = new ChangePasswordRequest2(OldPass.getText().toString(),NewPass.getText().toString(),new Response.Listener<String>() {
			@Override
			public void onResponse(String response) {
				Log.e("Response : ", response);
				progressDialog.dismiss();
				try {
					JSONObject jsonObject = new JSONObject(response);


					if (jsonObject.getInt("ResponseCode") == 200) {
						Toast.makeText(getActivity(), jsonObject.getString("ResponseMsg"), Toast.LENGTH_SHORT).show();

						replaceFragment(R.id.nav_host_fragment,new Fragment_Dashboard(),"Fragment");

// Reload current fragment
					} else {

						Toast.makeText(getActivity(), jsonObject.getString("ResponseMsg"), Toast.LENGTH_SHORT).show();

					}
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		}, new Response.ErrorListener() {
			@Override
			public void onErrorResponse(VolleyError error) {
				error.printStackTrace();
				progressDialog.dismiss();
				if (error instanceof ServerError)
					Toast.makeText(getActivity(), "Server Error", Toast.LENGTH_SHORT).show();
				else if (error instanceof TimeoutError)
					Toast.makeText(getActivity(), "Connection Timed Out", Toast.LENGTH_SHORT).show();
				else if (error instanceof NetworkError)
					Toast.makeText(getActivity(), "Bad Network Connection", Toast.LENGTH_SHORT).show();
			}
		}) {
			@Override
			public Map<String, String> getHeaders() throws AuthFailureError {
				Map<String, String> params = new HashMap<String, String>();
				 params.put("Accept", "application/json");
				params.put("Authorization","Bearer "+ Session.getAPIToken());
				return params;
			}
		};
		loginRequest.setTag("TAG");
		loginRequest.setShouldCache(false);
		requestQueue.add(loginRequest);

	}


	private void GetType()  {

		final KProgressHUD progressDialog = KProgressHUD.create(getActivity())
				.setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
				.setLabel("Please wait")
				.setCancellable(false)
				.setAnimationSpeed(2)
				.setDimAmount(0.5f)
				.show();

		GetTypeRequest loginRequest = new GetTypeRequest(new Response.Listener<String>() {
			@Override
			public void onResponse(String response) {
				Log.e("Response", response + " null");
				progressDialog.dismiss();

				mDataset_Type.clear();

				try {

					JSONObject jsonObject = new JSONObject(response);

					JSONArray jsonObject1 = jsonObject.getJSONArray("data");


					for (int i = 0 ; i<jsonObject1.length() ; i++){
						JSONObject object = jsonObject1.getJSONObject(i);
						NameModel BatchModel = new NameModel();
						BatchModel.setId(object.getString("restaurant_type_id"));
						BatchModel.setName(object.getString("restaurant_type"));
						mDataset_Type.add(BatchModel);
					}


					NameModel BatchModel = new NameModel();
					BatchModel.setId(mRestaurant_TypeID);
					BatchModel.setName(mRestaurant_Type);
					mDataset_Type.add(BatchModel);
					SpinAdapter adapter = new SpinAdapter(getContext(),
							android.R.layout.simple_spinner_item,
							mDataset_Type);


					adapter.setDropDownViewTheme(getActivity().getResources().newTheme());
					adapter.setDropDownViewResource(R.layout.spinner_item);
					mType.setAdapter(adapter);
					mType.setSelection(adapter.getCount());



				}catch (Exception e){

				}


			}
		}, new Response.ErrorListener() {
			@Override
			public void onErrorResponse(VolleyError error) {
				error.printStackTrace();
				progressDialog.dismiss();
				if (error instanceof ServerError)
					Toast.makeText(getActivity(), "Server Error", Toast.LENGTH_SHORT).show();
				else if (error instanceof TimeoutError)
					Toast.makeText(getActivity(), "Connection Timed Out", Toast.LENGTH_SHORT).show();
				else if (error instanceof NetworkError)
					Toast.makeText(getActivity(), "Bad Network Connection", Toast.LENGTH_SHORT).show();
			}
		}){
			@Override
			public Map<String, String> getHeaders() throws AuthFailureError {
				Map<String, String> params = new HashMap<String, String>();
				// params.put("Accept", "application/json");
				// params.put("Authorization","Bearer "+ Session.getAPIToken());
				return params;
			}};
		loginRequest.setTag("TAG");loginRequest.setShouldCache(false);
		requestQueue.add(loginRequest);

	}

	private void GetCity(String id)  {

		final KProgressHUD progressDialog = KProgressHUD.create(getActivity())
				.setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
				.setLabel("Please wait")
				.setCancellable(false)
				.setAnimationSpeed(2)
				.setDimAmount(0.5f)
				.show();


		GetCityRequest loginRequest = new GetCityRequest(id,new Response.Listener<String>() {
			@Override
			public void onResponse(String response) {
				Log.e("Response", response + " null");
				progressDialog.dismiss();

				mDataset_City.clear();

				try {

					JSONObject jsonObject = new JSONObject(response);

					JSONObject jsonObject1 = jsonObject.getJSONObject("data");
					JSONArray jsonObject12 = jsonObject1.getJSONArray("data");


					for (int i = 0 ; i<jsonObject12.length() ; i++){
						JSONObject object = jsonObject12.getJSONObject(i);
						NameModel BatchModel = new NameModel();
						BatchModel.setId(object.getString("city_id"));
						BatchModel.setName(object.getString("city"));
						mDataset_City.add(BatchModel);
					}


					NameModel BatchModel = new NameModel();
					BatchModel.setId(mCityNameID);
					BatchModel.setName(mCityName);
					mDataset_City.add(BatchModel);
					SpinAdapter adapter = new SpinAdapter(getContext(),
							android.R.layout.simple_spinner_item,
							mDataset_City);


					adapter.setDropDownViewTheme(getActivity().getResources().newTheme());
					adapter.setDropDownViewResource(R.layout.spinner_item);
					mCity.setAdapter(adapter);
					mCity.setSelection(adapter.getCount());

				}catch (Exception e){

				}


			}
		}, new Response.ErrorListener() {
			@Override
			public void onErrorResponse(VolleyError error) {
				error.printStackTrace();
				progressDialog.dismiss();
				if (error instanceof ServerError)
					Toast.makeText(getActivity(), "Server Error", Toast.LENGTH_SHORT).show();
				else if (error instanceof TimeoutError)
					Toast.makeText(getActivity(), "Connection Timed Out", Toast.LENGTH_SHORT).show();
				else if (error instanceof NetworkError)
					Toast.makeText(getActivity(), "Bad Network Connection", Toast.LENGTH_SHORT).show();
			}
		}){
			@Override
			public Map<String, String> getHeaders() throws AuthFailureError {
				Map<String, String> params = new HashMap<String, String>();
				// params.put("Accept", "application/json");
				// params.put("Authorization","Bearer "+ Session.getAPIToken());
				return params;
			}};
		loginRequest.setTag("TAG");
		loginRequest.setShouldCache(false);
		requestQueue.add(loginRequest);

	}


	private void GetStates()  {

		final KProgressHUD progressDialog = KProgressHUD.create(getActivity())
				.setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
				.setLabel("Please wait")
				.setCancellable(false)
				.setAnimationSpeed(2)
				.setDimAmount(0.5f)
				.show();

		GetStatesRequest loginRequest = new GetStatesRequest(new Response.Listener<String>() {
			@Override
			public void onResponse(String response) {
				Log.e("Response", response + " null");
				progressDialog.dismiss();
				mDataset_State.clear();


				try {

					JSONObject jsonObject = new JSONObject(response);

					JSONObject jsonObject1 = jsonObject.getJSONObject("data");
					JSONArray jsonObject12 = jsonObject1.getJSONArray("data");


					for (int i = 0 ; i<jsonObject12.length() ; i++){
						JSONObject object = jsonObject12.getJSONObject(i);
						NameModel BatchModel = new NameModel();
						BatchModel.setId(object.getString("state_id"));
						BatchModel.setName(object.getString("state_name"));
						mDataset_State.add(BatchModel);
					}


					NameModel BatchModel = new NameModel();
					BatchModel.setId(mStateNameID);
					BatchModel.setName(mStateName);
					mDataset_State.add(BatchModel);
					SpinAdapter adapter = new SpinAdapter(getContext(),
							android.R.layout.simple_spinner_item,
							mDataset_State);


					adapter.setDropDownViewTheme(getActivity().getResources().newTheme());
					adapter.setDropDownViewResource(R.layout.spinner_item);
					mState.setAdapter(adapter);
					mState.setSelection(adapter.getCount());

				}catch (Exception e){

				}


			}
		}, new Response.ErrorListener() {
			@Override
			public void onErrorResponse(VolleyError error) {
				error.printStackTrace();
				progressDialog.dismiss();
				if (error instanceof ServerError)
					Toast.makeText(getActivity(), "Server Error", Toast.LENGTH_SHORT).show();
				else if (error instanceof TimeoutError)
					Toast.makeText(getActivity(), "Connection Timed Out", Toast.LENGTH_SHORT).show();
				else if (error instanceof NetworkError)
					Toast.makeText(getActivity(), "Bad Network Connection", Toast.LENGTH_SHORT).show();
			}
		}){
			@Override
			public Map<String, String> getHeaders() throws AuthFailureError {
				Map<String, String> params = new HashMap<String, String>();
				// params.put("Accept", "application/json");
				// params.put("Authorization","Bearer "+ Session.getAPIToken());
				return params;
			}};
		loginRequest.setTag("TAG");
		loginRequest.setShouldCache(false);
		requestQueue.add(loginRequest);

	}


	public void getCategories(){
		final KProgressHUD progressDialog = KProgressHUD.create(getContext())
				.setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
				.setLabel("Please wait")
				.setCancellable(false)
				.setAnimationSpeed(2)
				.setDimAmount(0.5f)
				.show();
		//getting the tag from the edittext

		//our custom volley request
		VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.POST, ServerUtils.BASE_URL + "restaurant/update-profile",
				new Response.Listener<NetworkResponse>() {
					@Override
					public void onResponse(NetworkResponse response) {

						progressDialog.dismiss();


						try {
							JSONObject jsonObject = new JSONObject(new String(response.data));
							Log.e("Response",jsonObject.toString() + " dd");
							if (jsonObject.getString("ResponseCode").equals("200")){

								try {

										Toast.makeText(getActivity(), jsonObject.getString("ResponseMsg"), Toast.LENGTH_SHORT).show();
									replaceFragment(R.id.nav_host_fragment,new Fragment_Dashboard(),"Fragment");

										//RastaurantLogin();
// Reload current fragment
								} catch (JSONException e) {
									e.printStackTrace();
									Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
								}
							}else {
								Toast.makeText(getActivity(), jsonObject.getString("ResponseMsg"), Toast.LENGTH_SHORT).show();

							}

						} catch (Exception e) {
							Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();

						}

					}
				},
				new Response.ErrorListener() {
					@Override
					public void onErrorResponse(VolleyError error) {
						progressDialog.dismiss();
						replaceFragment(R.id.nav_host_fragment,new Fragment_Dashboard(),"Fragment");

						//Log.e("error",error.getMessage());

						//Toast.makeText(getContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
					}
				}) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();


				params.put("restaurant_name", FirstName.getText().toString());
				params.put("description", LastName.getText().toString());
				params.put("email", Email.getText().toString());
				params.put("restaurant_type", mTypeId);
				params.put("city_id", mCityId);
				params.put("address", mAddress.getText().toString());
				params.put("percentage_you_want", mOffersId);
				params.put("offer", PhoneNumber.getText().toString());
                return params;
            }


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                params.put("Authorization", "Bearer " + Session.getAPIToken());
                return params;
            }
            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();
				long imagename = System.currentTimeMillis();

				if(bitmap!=null){
					params.put("restaurant_image", new DataPart(imagename + ".png", getFileDataFromDrawable(bitmap)));

				}

				return params;
            }
        };
        Volley.newRequestQueue(getContext()).add(volleyMultipartRequest);
    }


	public byte[] getFileDataFromDrawable(Bitmap bitmap) {
		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
		bitmap.compress(Bitmap.CompressFormat.PNG, 80, byteArrayOutputStream);
		return byteArrayOutputStream.toByteArray();
	}


	private void RastaurantLogin()  {

		final KProgressHUD progressDialog = KProgressHUD.create(getActivity())
				.setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
				.setLabel("Please wait")
				.setCancellable(false)
				.setAnimationSpeed(2)
				.setDimAmount(0.5f)
				.show();

		GetResProfileRequest loginRequest = new GetResProfileRequest(new Response.Listener<String>() {
			@Override
			public void onResponse(String response) {
				Log.e("Response", response + " null");
				progressDialog.dismiss();


				JSONObject jsonObject = null;
				try {
					jsonObject = new JSONObject(response);
				//	Toast.makeText(getActivity(),jsonObject.getString("ResponseMsg"),Toast.LENGTH_SHORT).show();

					PhoneNumber.setText(jsonObject.getJSONObject("data").getString("offer"));
					mAddress.setText(jsonObject.getJSONObject("data").getString("address"));
					Session.createLoginSession(jsonObject.getJSONObject("data").getString("restaurant_id"),
							jsonObject.getJSONObject("data").getString("restaurant_image"),
							jsonObject.getJSONObject("data").getString("restaurant_name"),
							jsonObject.getJSONObject("data").getString("restaurant_name"),
							jsonObject.getJSONObject("data").getString("email"),
							"",
							jsonObject.getJSONObject("data").getString("api_token")
					);



					mRestaurant_Type = jsonObject.getJSONObject("data").getString("restaurant_type");
					mRestaurant_TypeID = jsonObject.getJSONObject("data").getString("restaurant_type_id");
					mStateName = jsonObject.getJSONObject("data").getString("state_name");
					mStateNameID = jsonObject.getJSONObject("data").getString("state_id");
					mCityName = jsonObject.getJSONObject("data").getString("city");
					mCityNameID = jsonObject.getJSONObject("data").getString("city_id");




					GetStates();
					GetType();

					Glide.with(getActivity()).load(jsonObject.getJSONObject("data").getString("restaurant_image")).circleCrop().into(upload_result);

				}catch (Exception ignored){

				}


			}
		}, new Response.ErrorListener() {
			@Override
			public void onErrorResponse(VolleyError error) {
				error.printStackTrace();
				progressDialog.dismiss();
				if (error instanceof ServerError)
					Toast.makeText(getActivity(), "Server Error", Toast.LENGTH_SHORT).show();
				else if (error instanceof TimeoutError)
					Toast.makeText(getActivity(), "Connection Timed Out", Toast.LENGTH_SHORT).show();
				else if (error instanceof NetworkError)
					Toast.makeText(getActivity(), "Bad Network Connection", Toast.LENGTH_SHORT).show();
			}
		}){
			@Override
			public Map<String, String> getHeaders() throws AuthFailureError {
				Map<String, String> params = new HashMap<String, String>();
				 params.put("Accept", "application/json");
				 params.put("Authorization","Bearer "+ Session.getAPIToken());
				return params;
			}};
		loginRequest.setTag("TAG");loginRequest.setShouldCache(false);
		requestQueue.add(loginRequest);

	}


	public boolean checkPermission() {
		int result1 = ContextCompat.checkSelfPermission(getActivity(), CAMERA);
		int result2 = ContextCompat.checkSelfPermission(getActivity(), ACCESS_FINE_LOCATION);
		int result3 = ContextCompat.checkSelfPermission(getActivity(), ACCESS_COARSE_LOCATION);
		int result5 = ContextCompat.checkSelfPermission(getActivity(), READ_EXTERNAL_STORAGE);
		int result6 = ContextCompat.checkSelfPermission(getActivity(), WRITE_EXTERNAL_STORAGE);
		return
				result1 == PackageManager.PERMISSION_GRANTED
						&& result2 == PackageManager.PERMISSION_GRANTED
						&& result3 == PackageManager.PERMISSION_GRANTED
						&& result5 == PackageManager.PERMISSION_GRANTED
						&& result6 == PackageManager.PERMISSION_GRANTED;
	}

	public void requestPermission() {
		ActivityCompat.requestPermissions(getActivity(), new String[]{
						CAMERA,
						ACCESS_FINE_LOCATION,
						ACCESS_COARSE_LOCATION,
						READ_EXTERNAL_STORAGE,
						WRITE_EXTERNAL_STORAGE
				},
				PERMISSION_REQUEST_CODE);
	}







}