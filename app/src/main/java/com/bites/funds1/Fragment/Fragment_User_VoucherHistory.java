package com.bites.funds1.Fragment;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.bites.funds1.API.GetTransactionListRequest;
import com.bites.funds1.API.GetUserTransactionListRequest;
import com.bites.funds1.API.SearchGetTransactionListRequest;
import com.bites.funds1.API.SearchGetUserTransactionListRequest;
import com.bites.funds1.R;
import com.bites.funds1.Utils.EndlessRecyclerViewScrollListener;
import com.bites.funds1.Utils.UserSession;
import com.bites.funds1.Adapter.TransactionAdapter;
import com.bites.funds1.Model.TransactionModel;
import com.bites.funds1.Utils.AppContants;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Fragment_User_VoucherHistory extends Fragment {
	// Store instance variables

	private RecyclerView category_view;
	private TransactionAdapter mAdapter;
	private ArrayList<TransactionModel> categoryModels = new ArrayList<>();
	private GridLayoutManager gridLayoutManager;
	private int last_size;
	private RequestQueue requestQueue;
	// Store instance variables
	private UserSession Session;
	private TextView name;



	// Store instance variables based on arguments passed
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

	}

	// Inflate the view for the fragment based on layout XML
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_user_history, container, false);

		Session = new UserSession(getActivity());

		final TextView search = view.findViewById(R.id.search);
		final TextView search_btn = view.findViewById(R.id.search_btn);
		requestQueue = Volley.newRequestQueue(getActivity());//Creating the RequestQueue

		category_view = view.findViewById(R.id.category_view);
		name = view.findViewById(R.id.name);
		mAdapter = new TransactionAdapter(categoryModels, new TransactionAdapter.OnItemClickListener() {
			@Override
			public void onItemClick(int item) {

			}
		});
		category_view.setHasFixedSize(true);
		gridLayoutManager = new GridLayoutManager(getActivity().getApplicationContext(), 1);
		category_view.setLayoutManager(gridLayoutManager);
		category_view.setAdapter(mAdapter);
		category_view.setNestedScrollingEnabled(true);


		category_view.addOnScrollListener(new EndlessRecyclerViewScrollListener(gridLayoutManager) {
			@Override
			public void onLoadMore(int page, int totalItemsCount) {

				if (page!=last_size){
					Log.e("FinalOAgeSize",""+page);
					int finalintpage = page +1;
					if(Session.getLoginType().equals(AppContants.TYPEUSER)){
						GetSupportRastaurant2(finalintpage);
					}else {
						GetSupportRastaurant(finalintpage);
					}

				}
			}
		});

		if(Session.getLoginType().equals(AppContants.TYPEUSER)){
			name.setText("Rastaurant Name");
			GetSupportRastaurant2(1);
		}else {
			name.setText("User Name");
			GetSupportRastaurant(1);
		}


		search_btn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				if(search.getText().toString().isEmpty()){
					Toast.makeText(getActivity(),"Please enter some word for search...",Toast.LENGTH_SHORT).show();
				}else {
					if(Session.getLoginType().equals(AppContants.TYPEUSER)){
						name.setText("Rastaurant Name");
						GetSupportRastaurant2Search(1,search.getText().toString());
					}else {
						name.setText("User Name");

						GetSupportRastaurantSearch(1,search.getText().toString());
					}


				}
			}
		});


		search.addTextChangedListener(new TextWatcher() {
			@Override
			public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

			}

			@Override
			public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

				if(charSequence.toString().isEmpty()){

					categoryModels.clear();

					if(Session.getLoginType().equals(AppContants.TYPEUSER)){
						name.setText("Rastaurant Name");
						GetSupportRastaurant2(1);
					}else {
						name.setText("User Name");
						GetSupportRastaurant(1);
					}
				}

			}

			@Override
			public void afterTextChanged(Editable editable) {

			}
		});



		return view;
	}


	protected void replaceFragment(@IdRes int containerViewId,
								   @NonNull Fragment fragment,
								   @NonNull String fragmentTag) {
		getActivity().getSupportFragmentManager()
				.beginTransaction()
				.replace(containerViewId, fragment, fragmentTag)
				.disallowAddToBackStack()
				.commit();
	}


	public void GetSupportRastaurant(int page) {

		final KProgressHUD progressDialog = KProgressHUD.create(getActivity())
				.setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
				.setLabel("Please wait")
				.setCancellable(false)
				.setAnimationSpeed(2)
				.setDimAmount(0.5f)
				.show();

		GetTransactionListRequest loginRequest = new GetTransactionListRequest(page,new Response.Listener<String>() {
			@Override
			public void onResponse(String response) {
				Log.e("Response", response + " null");
				progressDialog.dismiss();

				JSONObject jsonObject = null;
				try {
					jsonObject = new JSONObject(response);
					// Toast.makeText(AllCategories.this,jsonObject.getString("ResponseMsg"),Toast.LENGTH_SHORT).show();
					if (jsonObject.getString("ResponseCode").equals("200")) {

						JSONObject objectJSONObject = jsonObject.getJSONObject("data");
						//	JSONObject redeemed_vouchers = objectJSONObject.getJSONObject("redeemed_vouchers");
						JSONArray jsonArray = objectJSONObject.getJSONArray("data");

						last_size = objectJSONObject.getInt("last_page");
						for(int i =0 ; i<jsonArray.length();i++){
							JSONObject object = jsonArray.getJSONObject(i);

							TransactionModel rastaurantModel = new TransactionModel();
							rastaurantModel.setUser_name(object.getString("user_name"));
							rastaurantModel.setBite(object.getString("balance"));
							rastaurantModel.setDate(object.getString("date"));
							rastaurantModel.setBalance(object.getString("bite"));
							rastaurantModel.setTransaction_type(object.getString("transaction_type"));
							categoryModels.add(rastaurantModel);
						}

						mAdapter.notifyDataSetChanged();
					}
				} catch (JSONException e) {
					e.printStackTrace();
					Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();
				}

			}
		}, new Response.ErrorListener() {
			@Override
			public void onErrorResponse(VolleyError error) {
				error.printStackTrace();
				progressDialog.dismiss();
				if (error instanceof ServerError)
					Toast.makeText(getActivity(), "Server Error", Toast.LENGTH_SHORT).show();
				else if (error instanceof TimeoutError)
					Toast.makeText(getActivity(), "Connection Timed Out", Toast.LENGTH_SHORT).show();
				else if (error instanceof NetworkError)
					Toast.makeText(getActivity(), "Bad Network Connection", Toast.LENGTH_SHORT).show();
			}
		}){@Override
		public Map<String, String> getHeaders() throws AuthFailureError {
			Map<String, String> params = new HashMap<String, String>();
			params.put("Accept", "application/json");
			params.put("Authorization","Bearer "+ Session.getAPIToken());
			return params;
		}};
		loginRequest.setTag("TAG");loginRequest.setShouldCache(false);
		requestQueue.add(loginRequest);

	}


	public void GetSupportRastaurant2(int page) {

		final KProgressHUD progressDialog = KProgressHUD.create(getActivity())
				.setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
				.setLabel("Please wait")
				.setCancellable(false)
				.setAnimationSpeed(2)
				.setDimAmount(0.5f)
				.show();

		GetUserTransactionListRequest loginRequest = new GetUserTransactionListRequest(page,new Response.Listener<String>() {
			@Override
			public void onResponse(String response) {
				Log.e("ResponseHistory", response + " null");
				progressDialog.dismiss();

				JSONObject jsonObject = null;
				try {
					jsonObject = new JSONObject(response);
					// Toast.makeText(AllCategories.this,jsonObject.getString("ResponseMsg"),Toast.LENGTH_SHORT).show();
					if (jsonObject.getString("ResponseCode").equals("200")) {

						JSONObject objectJSONObject = jsonObject.getJSONObject("data");
						JSONArray jsonArray = objectJSONObject.getJSONArray("data");

						last_size = objectJSONObject.getInt("last_page");
						for(int i =0 ; i<jsonArray.length();i++){
							JSONObject object = jsonArray.getJSONObject(i);

							TransactionModel rastaurantModel = new TransactionModel();
							rastaurantModel.setUser_name(object.getString("restaurant_name"));
							rastaurantModel.setBite(object.getString("balance"));
							rastaurantModel.setDate(object.getString("date"));
							rastaurantModel.setBalance(object.getString("bite"));
							rastaurantModel.setTransaction_type(object.getString("transaction_type"));
							categoryModels.add(rastaurantModel);
						}

						mAdapter.notifyDataSetChanged();
					}
				} catch (JSONException e) {
					e.printStackTrace();
					Toast.makeText(getActivity(),e.getMessage(), Toast.LENGTH_SHORT).show();

				}

			}
		}, new Response.ErrorListener() {
			@Override
			public void onErrorResponse(VolleyError error) {
				error.printStackTrace();
				progressDialog.dismiss();
				if (error instanceof ServerError)
					Toast.makeText(getActivity(), "Server Error", Toast.LENGTH_SHORT).show();
				else if (error instanceof TimeoutError)
					Toast.makeText(getActivity(), "Connection Timed Out", Toast.LENGTH_SHORT).show();
				else if (error instanceof NetworkError)
					Toast.makeText(getActivity(), "Bad Network Connection", Toast.LENGTH_SHORT).show();
			}
		}){@Override
		public Map<String, String> getHeaders() throws AuthFailureError {
			Map<String, String> params = new HashMap<String, String>();
			params.put("Accept", "application/json");
			params.put("Authorization","Bearer "+ Session.getAPIToken());
			return params;
		}};
		loginRequest.setTag("TAG");loginRequest.setShouldCache(false);
		requestQueue.add(loginRequest);

	}

	public void GetSupportRastaurant2Search(int page,String search) {

		final KProgressHUD progressDialog = KProgressHUD.create(getActivity())
				.setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
				.setLabel("Please wait")
				.setCancellable(false)
				.setAnimationSpeed(2)
				.setDimAmount(0.5f)
				.show();

		SearchGetUserTransactionListRequest loginRequest = new SearchGetUserTransactionListRequest(page,search,new Response.Listener<String>() {
			@Override
			public void onResponse(String response) {
				Log.e("ResponseHistory", response + " null");
				progressDialog.dismiss();
				categoryModels.clear();
				JSONObject jsonObject = null;
				try {
					jsonObject = new JSONObject(response);
					// Toast.makeText(AllCategories.this,jsonObject.getString("ResponseMsg"),Toast.LENGTH_SHORT).show();
					if (jsonObject.getString("ResponseCode").equals("200")) {

						JSONObject objectJSONObject = jsonObject.getJSONObject("data");
						JSONArray jsonArray = objectJSONObject.getJSONArray("data");

						last_size = objectJSONObject.getInt("last_page");
						for(int i =0 ; i<jsonArray.length();i++){
							JSONObject object = jsonArray.getJSONObject(i);

							TransactionModel rastaurantModel = new TransactionModel();
							rastaurantModel.setUser_name(object.getString("restaurant_name"));
							rastaurantModel.setBite(object.getString("balance"));
							rastaurantModel.setDate(object.getString("date"));
							rastaurantModel.setBalance(object.getString("bite"));
							rastaurantModel.setTransaction_type(object.getString("transaction_type"));
							categoryModels.add(rastaurantModel);
						}

						mAdapter.notifyDataSetChanged();
					}
				} catch (JSONException e) {
					e.printStackTrace();
					Toast.makeText(getActivity(),e.getMessage(), Toast.LENGTH_SHORT).show();

				}

			}
		}, new Response.ErrorListener() {
			@Override
			public void onErrorResponse(VolleyError error) {
				error.printStackTrace();
				progressDialog.dismiss();
				if (error instanceof ServerError)
					Toast.makeText(getActivity(), "Server Error", Toast.LENGTH_SHORT).show();
				else if (error instanceof TimeoutError)
					Toast.makeText(getActivity(), "Connection Timed Out", Toast.LENGTH_SHORT).show();
				else if (error instanceof NetworkError)
					Toast.makeText(getActivity(), "Bad Network Connection", Toast.LENGTH_SHORT).show();
			}
		}){@Override
		public Map<String, String> getHeaders() throws AuthFailureError {
			Map<String, String> params = new HashMap<String, String>();
			params.put("Accept", "application/json");
			params.put("Authorization","Bearer "+ Session.getAPIToken());
			return params;
		}};
		loginRequest.setTag("TAG");loginRequest.setShouldCache(false);
		requestQueue.add(loginRequest);

	}

	public void GetSupportRastaurantSearch(int page,String Search) {

		final KProgressHUD progressDialog = KProgressHUD.create(getActivity())
				.setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
				.setLabel("Please wait")
				.setCancellable(false)
				.setAnimationSpeed(2)
				.setDimAmount(0.5f)
				.show();

		SearchGetTransactionListRequest loginRequest = new SearchGetTransactionListRequest(page,Search,new Response.Listener<String>() {
			@Override
			public void onResponse(String response) {
				Log.e("Response", response + " null");
				progressDialog.dismiss();
				categoryModels.clear();

				JSONObject jsonObject = null;
				try {
					jsonObject = new JSONObject(response);
					// Toast.makeText(AllCategories.this,jsonObject.getString("ResponseMsg"),Toast.LENGTH_SHORT).show();
					if (jsonObject.getString("ResponseCode").equals("200")) {

						JSONObject objectJSONObject = jsonObject.getJSONObject("data");
						//	JSONObject redeemed_vouchers = objectJSONObject.getJSONObject("redeemed_vouchers");
						JSONArray jsonArray = objectJSONObject.getJSONArray("data");

						last_size = objectJSONObject.getInt("last_page");
						for(int i =0 ; i<jsonArray.length();i++){
							JSONObject object = jsonArray.getJSONObject(i);

							TransactionModel rastaurantModel = new TransactionModel();
							rastaurantModel.setUser_name(object.getString("user_name"));
							rastaurantModel.setBite(object.getString("balance"));
							rastaurantModel.setDate(object.getString("date"));
							rastaurantModel.setBalance(object.getString("bite"));
							rastaurantModel.setTransaction_type(object.getString("transaction_type"));
							categoryModels.add(rastaurantModel);
						}

						mAdapter.notifyDataSetChanged();
					}
				} catch (JSONException e) {
					e.printStackTrace();
					Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();
				}

			}
		}, new Response.ErrorListener() {
			@Override
			public void onErrorResponse(VolleyError error) {
				error.printStackTrace();
				progressDialog.dismiss();
				if (error instanceof ServerError)
					Toast.makeText(getActivity(), "Server Error", Toast.LENGTH_SHORT).show();
				else if (error instanceof TimeoutError)
					Toast.makeText(getActivity(), "Connection Timed Out", Toast.LENGTH_SHORT).show();
				else if (error instanceof NetworkError)
					Toast.makeText(getActivity(), "Bad Network Connection", Toast.LENGTH_SHORT).show();
			}
		}){@Override
		public Map<String, String> getHeaders() throws AuthFailureError {
			Map<String, String> params = new HashMap<String, String>();
			params.put("Accept", "application/json");
			params.put("Authorization","Bearer "+ Session.getAPIToken());
			return params;
		}};
		loginRequest.setTag("TAG");loginRequest.setShouldCache(false);
		requestQueue.add(loginRequest);

	}






}