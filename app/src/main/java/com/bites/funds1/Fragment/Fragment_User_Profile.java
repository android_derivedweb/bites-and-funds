package com.bites.funds1.Fragment;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.bites.funds1.API.ChangePasswordRequest;
import com.bites.funds1.API.GetUserProfileRequest;
import com.bites.funds1.API.UpdateUserProfileRequest;
import com.bites.funds1.R;
import com.bites.funds1.Utils.ServerUtils;
import com.bites.funds1.Utils.UserSession;
import com.bumptech.glide.Glide;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;

public class Fragment_User_Profile extends Fragment {
    private TextView edit_profile, change_password;
    private RelativeLayout edit_profile_layout, change_password_layout;
    private KProgressHUD progressDialog;
    private UserSession Session;
    private EditText FirstName, LastName, Email, PhoneNumber, OldPass, NewPass, New2Pass;
    // Store instance variables
    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    private RequestQueue requestQueue;


    // Store instance variables based on arguments passed
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    // Inflate the view for the fragment based on layout XML
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile, container, false);


        edit_profile = view.findViewById(R.id.edit_profile);
        edit_profile_layout = view.findViewById(R.id.edit_profile_layout);
        change_password = view.findViewById(R.id.change_password);
        change_password_layout = view.findViewById(R.id.change_password_layout);


        requestQueue = Volley.newRequestQueue(getActivity());//Creating the RequestQueue

        Session = new UserSession(getActivity());


        FirstName = view.findViewById(R.id.firstname);
        LastName = view.findViewById(R.id.last_name);
        Email = view.findViewById(R.id.email);
        PhoneNumber = view.findViewById(R.id.phone_no);


        FirstName.setText(Session.getFirstName());
        LastName.setText(Session.getLastName());
        Email.setText(Session.getEmail());
        PhoneNumber.setText(Session.getPhoneNo());

        upload_result = view.findViewById(R.id.user_img);

        Glide.with(getActivity()).load(Session.getProfilePic()).circleCrop().into(upload_result);
        OldPass = view.findViewById(R.id.old);
        NewPass = view.findViewById(R.id.new1);
        New2Pass = view.findViewById(R.id.new2);


        edit_profile.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onClick(View view) {
                edit_profile.setBackground(getActivity().getResources().getDrawable(R.drawable.white_fill_border));
                change_password.setBackground(getActivity().getResources().getDrawable(R.drawable.white_blank_border));
                edit_profile.setTextColor(getActivity().getResources().getColor(R.color.colorPrimary));
                change_password.setTextColor(getActivity().getResources().getColor(R.color.white));
                edit_profile_layout.setVisibility(View.VISIBLE);
                change_password_layout.setVisibility(View.GONE);
            }
        });

        change_password.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onClick(View view) {
                edit_profile.setBackground(getActivity().getResources().getDrawable(R.drawable.white_blank_border));
                change_password.setBackground(getActivity().getResources().getDrawable(R.drawable.white_fill_border));
                edit_profile.setTextColor(getActivity().getResources().getColor(R.color.white));
                change_password.setTextColor(getActivity().getResources().getColor(R.color.colorPrimary));
                edit_profile_layout.setVisibility(View.GONE);
                change_password_layout.setVisibility(View.VISIBLE);
            }
        });


        view.findViewById(R.id.edit_icon).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectImage();
            }
        });


        view.findViewById(R.id.update2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (OldPass.getText().toString().isEmpty()) {
                    Toast.makeText(getActivity(), "Please Enter Your Old Password", Toast.LENGTH_SHORT).show();
                } else if (NewPass.getText().toString().isEmpty()) {
                    Toast.makeText(getActivity(), "Please Enter Your New Password", Toast.LENGTH_SHORT).show();
                } else if (New2Pass.getText().toString().isEmpty()) {
                    Toast.makeText(getActivity(), "Please Enter Your Email", Toast.LENGTH_SHORT).show();
                } else if (!New2Pass.getText().toString().equals(NewPass.getText().toString())) {
                    Toast.makeText(getActivity(), "Password does not match", Toast.LENGTH_SHORT).show();

                } else {
                    ChangePassword();
                }
            }
        });

        view.findViewById(R.id.update).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (FirstName.getText().toString().isEmpty()) {
                    Toast.makeText(getActivity(), "Please Enter Your FirstName", Toast.LENGTH_SHORT).show();
                } else if (LastName.getText().toString().isEmpty()) {
                    Toast.makeText(getActivity(), "Please Enter Your LastName", Toast.LENGTH_SHORT).show();
                } else if (Email.getText().toString().isEmpty()) {
                    Toast.makeText(getActivity(), "Please Enter Your Email", Toast.LENGTH_SHORT).show();
                } else if (!Email.getText().toString().trim().matches(emailPattern)) {
                    Toast.makeText(getActivity(), "Invalid email address", Toast.LENGTH_SHORT).show();
                } else if (PhoneNumber.getText().toString().isEmpty()) {
                    Toast.makeText(getActivity(), "Please Enter Your PhoneNumber", Toast.LENGTH_SHORT).show();
                } else {
                    UpdateProfile2();
                }
            }
        });

        UserLogin();
        return view;
    }


    protected void replaceFragment(@IdRes int containerViewId,
                                   @NonNull Fragment fragment,
                                   @NonNull String fragmentTag) {
        getActivity().getSupportFragmentManager()
                .beginTransaction()
                .replace(containerViewId, fragment, fragmentTag)
                .disallowAddToBackStack()
                .commit();
    }

    private Bitmap bitmap;
    private File destination = null;
    private InputStream inputStreamImg;
    private String imgPath = null;
    private final int PICK_IMAGE_CAMERA = 1, PICK_IMAGE_GALLERY = 2;
    private ImageView upload_result, re_change, upload_img;


    // Select image from camera and gallery
    private void selectImage() {
        try {
            PackageManager pm = getActivity().getPackageManager();
            int hasPerm = pm.checkPermission(Manifest.permission.CAMERA, getActivity().getPackageName());
            if (hasPerm == PackageManager.PERMISSION_GRANTED) {
                final CharSequence[] options = {getString(R.string.take_photo), getString(R.string.gallary), getString(R.string.cancel)};
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle(R.string.select_option);
                builder.setItems(options, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int item) {
                        if (options[item].equals(getResources().getString(R.string.take_photo))) {
                            dialog.dismiss();
                            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                            startActivityForResult(intent, PICK_IMAGE_CAMERA);
                        } else if (options[item].equals(getResources().getString(R.string.gallary))) {
                            dialog.dismiss();
                            Intent pickPhoto = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                            startActivityForResult(pickPhoto, PICK_IMAGE_GALLERY);
                        } else if (options[item].equals(getResources().getString(R.string.cancel))) {
                            dialog.dismiss();
                        }
                    }
                });
                builder.show();
            } else
                checkAndroidVersion();
            //Toast.makeText(getActivity(), "Camera Permission error", Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            checkAndroidVersion();
            //Toast.makeText(getActivity(), "Camera Permission error", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        inputStreamImg = null;
        if (requestCode == PICK_IMAGE_CAMERA) {
            try {
                Uri selectedImage = data.getData();
                bitmap = (Bitmap) data.getExtras().get("data");
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 50, bytes);

                Log.e("Activity", "Pick from Camera::>>> ");

                File folder = new File(Environment.getExternalStorageDirectory() +
                        File.separator + "BonCopBadCop2");
                if (!folder.exists()) {
                    folder.mkdirs();
                }
                String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
                destination = new File(Environment.getExternalStorageDirectory() + "/" +
                        "Bites And Funds", "IMG_" + timeStamp + ".jpg");
                FileOutputStream fo;
                try {
                    destination.createNewFile();
                    fo = new FileOutputStream(destination);
                    fo.write(bytes.toByteArray());
                    fo.close();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                upload_img.setVisibility(View.INVISIBLE);
                re_change.setVisibility(View.VISIBLE);
                imgPath = destination.getAbsolutePath();
                upload_result.setImageBitmap(bitmap);
                Glide.with(Objects.requireNonNull(getActivity())).asBitmap().load(bitmap).circleCrop().into(upload_result);
                //txt_injury.setText(imgPath);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (requestCode == PICK_IMAGE_GALLERY) {
            Uri selectedImage = data.getData();
            try {
                bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), selectedImage);
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 50, bytes);
                Log.e("Activity", "Pick from Gallery::>>> ");

                imgPath = getRealPathFromURI(selectedImage);
                destination = new File(imgPath.toString());
                //  txt_injury.setText(imgPath);
                Glide.with(Objects.requireNonNull(getActivity())).asBitmap().load(bitmap).circleCrop().into(upload_result);
                upload_result.setImageBitmap(bitmap);
                upload_img.setVisibility(View.INVISIBLE);
                re_change.setVisibility(View.VISIBLE);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public String getRealPathFromURI(Uri contentUri) {
        String[] proj = {MediaStore.Audio.Media.DATA};
        Cursor cursor = getActivity().managedQuery(contentUri, proj, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Audio.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    private void checkAndroidVersion() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkAndRequestPermissions();

        } else {
            // code for lollipop and pre-lollipop devices
        }

    }

    private boolean checkAndRequestPermissions() {
        int camera = ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.CAMERA);
        int wtite = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int read = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE);
        List<String> listPermissionsNeeded = new ArrayList<>();
        if (wtite != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        if (camera != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.CAMERA);
        }
        if (read != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(getActivity(), listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), REQUEST_ID_MULTIPLE_PERMISSIONS);
            return false;
        }
        return true;
    }

    public static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 7;

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        Log.d("in fragment on request", "Permission callback called-------");
        switch (requestCode) {
            case REQUEST_ID_MULTIPLE_PERMISSIONS: {

                Map<String, Integer> perms = new HashMap<>();
                // Initialize the map with both permissions
                perms.put(Manifest.permission.WRITE_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.CAMERA, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.READ_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                // Fill with actual results from user
                if (grantResults.length > 0) {
                    for (int i = 0; i < permissions.length; i++)
                        perms.put(permissions[i], grantResults[i]);
                    // Check for both permissions
                    if (perms.get(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                            && perms.get(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED && perms.get(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                        Log.d("in fragment on request", "CAMERA & WRITE_EXTERNAL_STORAGE READ_EXTERNAL_STORAGE permission granted");
                        // process the normal flow
                        //else any one or both the permissions are not granted
                    } else {
                        Log.d("in fragment on request", "Some permissions are not granted ask again ");
                        //permission is denied (this is the first time, when "never ask again" is not checked) so ask again explaining the usage of permission
//                        // shouldShowRequestPermissionRationale will return true
                        //show the dialog or snackbar saying its necessary and try again otherwise proceed with setup.
                        if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) || ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.CAMERA) || ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE)) {
                            showDialogOK("Camera and Storage Permission required for this app",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            switch (which) {
                                                case DialogInterface.BUTTON_POSITIVE:
                                                    checkAndRequestPermissions();
                                                    break;
                                                case DialogInterface.BUTTON_NEGATIVE:
                                                    // proceed with logic by disabling the related features or quit the app.
                                                    break;
                                            }
                                        }
                                    });
                        }
                        //permission is denied (and never ask again is  checked)
                        //shouldShowRequestPermissionRationale will return false
                        else {
                            Toast.makeText(getActivity(), "Go to settings and enable permissions", Toast.LENGTH_LONG)
                                    .show();
                            //                            //proceed with logic by disabling the related features or quit the app.
                        }
                    }
                }
            }
        }

    }

    private void showDialogOK(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(getActivity())
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", okListener)
                .create()
                .show();
    }


/*
    public void UpdateProfile() {
        progressDialog = KProgressHUD.create(getActivity())
                .setStyle(KProgressHUD.Style.PIE_DETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f);

        progressDialog.show();


        AndroidNetworking.upload(ServerUtils.BASE_URL + "user/update-profile")
                .addMultipartFile("profile_image", destination)
                .addHeaders("Accept", "application/json")
                .addHeaders("Authorization", "Bearer " + Session.getAPIToken())
                .addMultipartParameter("first_name", FirstName.getText().toString())
                .addMultipartParameter("last_name", LastName.getText().toString())
                .addMultipartParameter("email", Email.getText().toString())
                .addMultipartParameter("phone_no", PhoneNumber.getText().toString())
                .setTag("uploadTest")
                .setPriority(Priority.HIGH)
                .build()
                .setUploadProgressListener(new UploadProgressListener() {
                    @Override
                    public void onProgress(long bytesUploaded, long totalBytes) {
                        // do anything with progress
                    }
                }).getAsString(new StringRequestListener() {
            @Override
            public void onResponse(String response) {
                Log.e("Response : ", response);
                progressDialog.dismiss();
                try {
                    JSONObject jsonObject = new JSONObject(response);


                    if (jsonObject.getInt("ResponseCode") == 200) {
                        Toast.makeText(getActivity(), jsonObject.getString("ResponseMsg"), Toast.LENGTH_SHORT).show();

                        String imgpath;
                        if (destination == null) {
                            imgpath = Session.getProfilePic();
                        } else {
                            imgpath = imgPath;
                        }

                        UserLogin();

                        Session.createLoginSession(Session.getUserId(), imgpath, FirstName.getText().toString(),
                                LastName.getText().toString(), Email.getText().toString(), PhoneNumber.getText().toString(), Session.getAPIToken());
// Reload current fragment
                    } else {

                        Toast.makeText(getActivity(), jsonObject.getString("ResponseMsg"), Toast.LENGTH_SHORT).show();

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(ANError anError) {
                progressDialog.dismiss();
                Log.e("Error", anError.getErrorBody());
                Toast.makeText(getActivity(), "Unauthenticated", Toast.LENGTH_SHORT).show();
            }
        });


    }
*/

/*
    public void ChangeProfile() {
        progressDialog = KProgressHUD.create(getActivity())
                .setStyle(KProgressHUD.Style.PIE_DETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f);

        progressDialog.show();


        AndroidNetworking.upload(ServerUtils.BASE_URL + "user/change-password")
                .addHeaders("Accept", "application/json")
                .addHeaders("Authorization", "Bearer " + Session.getAPIToken())
                .addMultipartParameter("old_password", OldPass.getText().toString())
                .addMultipartParameter("password", NewPass.getText().toString())
                .setTag("uploadTest")
                .setPriority(Priority.HIGH)
                .build()
                .setUploadProgressListener(new UploadProgressListener() {
                    @Override
                    public void onProgress(long bytesUploaded, long totalBytes) {
                        // do anything with progress
                    }
                }).getAsString(new StringRequestListener() {
            @Override
            public void onResponse(String response) {
                Log.e("Response : ", response);
                progressDialog.dismiss();
                try {
                    JSONObject jsonObject = new JSONObject(response);


                    if (jsonObject.getInt("ResponseCode") == 200) {
                        Toast.makeText(getActivity(), jsonObject.getString("ResponseMsg"), Toast.LENGTH_SHORT).show();

// Reload current fragment
                    } else {

                        Toast.makeText(getActivity(), jsonObject.getString("ResponseMsg"), Toast.LENGTH_SHORT).show();

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(ANError anError) {
                progressDialog.dismiss();
                Log.e("Error", anError.getErrorBody());
                Toast.makeText(getActivity(), "Unauthenticated", Toast.LENGTH_SHORT).show();
            }
        });

    }
*/


    private void UserLogin() {

        final KProgressHUD progressDialog = KProgressHUD.create(getActivity())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();

        GetUserProfileRequest loginRequest = new GetUserProfileRequest(new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("Response", response + " null");
                progressDialog.dismiss();


                JSONObject jsonObject = null;
                try {
                    jsonObject = new JSONObject(response);
                    Toast.makeText(getActivity(), jsonObject.getString("ResponseMsg"), Toast.LENGTH_SHORT).show();


                    Session.createLoginSession(jsonObject.getJSONObject("data").getString("user_id"),
                            jsonObject.getJSONObject("data").getString("profile_pic"),
                            jsonObject.getJSONObject("data").getString("first_name"),
                            jsonObject.getJSONObject("data").getString("last_name"),
                            jsonObject.getJSONObject("data").getString("email"),
                            jsonObject.getJSONObject("data").getString("phone_no"),
                            jsonObject.getJSONObject("data").getString("api_token")
                    );


                } catch (Exception e) {

                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                progressDialog.dismiss();
                if (error instanceof ServerError)
                    Toast.makeText(getActivity(), "Server Error", Toast.LENGTH_SHORT).show();
                else if (error instanceof TimeoutError)
                    Toast.makeText(getActivity(), "Connection Timed Out", Toast.LENGTH_SHORT).show();
                else if (error instanceof NetworkError)
                    Toast.makeText(getActivity(), "Bad Network Connection", Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                 params.put("Accept", "application/json");
                 params.put("Authorization","Bearer "+ Session.getAPIToken());
                return params;
            }
        };
        loginRequest.setTag("TAG");
        loginRequest.setShouldCache(false);
        requestQueue.add(loginRequest);

    }



    private void ChangePassword() {

        final KProgressHUD progressDialog = KProgressHUD.create(getActivity())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();

        ChangePasswordRequest loginRequest = new ChangePasswordRequest(OldPass.getText().toString(),NewPass.getText().toString(),new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("Response : ", response);
                progressDialog.dismiss();
                try {
                    JSONObject jsonObject = new JSONObject(response);


                    if (jsonObject.getInt("ResponseCode") == 200) {
                        Toast.makeText(getActivity(), jsonObject.getString("ResponseMsg"), Toast.LENGTH_SHORT).show();

// Reload current fragment
                    } else {

                        Toast.makeText(getActivity(), jsonObject.getString("ResponseMsg"), Toast.LENGTH_SHORT).show();

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                progressDialog.dismiss();
                if (error instanceof ServerError)
                    Toast.makeText(getActivity(), "Server Error", Toast.LENGTH_SHORT).show();
                else if (error instanceof TimeoutError)
                    Toast.makeText(getActivity(), "Connection Timed Out", Toast.LENGTH_SHORT).show();
                else if (error instanceof NetworkError)
                    Toast.makeText(getActivity(), "Bad Network Connection", Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                 params.put("Accept", "application/json");
                 params.put("Authorization","Bearer "+ Session.getAPIToken());
                return params;
            }
        };
        loginRequest.setTag("TAG");
        loginRequest.setShouldCache(false);
        requestQueue.add(loginRequest);

    }

    private void UpdateProfile2() {

        final KProgressHUD progressDialog = KProgressHUD.create(getActivity())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();

        UpdateUserProfileRequest loginRequest = new UpdateUserProfileRequest(FirstName.getText().toString(),LastName.getText().toString(),Email.getText().toString(),PhoneNumber.getText().toString(),new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("Response : ", response);
                progressDialog.dismiss();
                try {
                    JSONObject jsonObject = new JSONObject(response);


                    if (jsonObject.getInt("ResponseCode") == 200) {
                        Toast.makeText(getActivity(), jsonObject.getString("ResponseMsg"), Toast.LENGTH_SHORT).show();

                        String imgpath;
                        if (destination == null) {
                            imgpath = Session.getProfilePic();
                        } else {
                            imgpath = imgPath;
                        }

                        UserLogin();

                        Session.createLoginSession(Session.getUserId(), imgpath, FirstName.getText().toString(),
                                LastName.getText().toString(), Email.getText().toString(), PhoneNumber.getText().toString(), Session.getAPIToken());
// Reload current fragment
                    } else {

                        Toast.makeText(getActivity(), jsonObject.getString("ResponseMsg"), Toast.LENGTH_SHORT).show();

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                progressDialog.dismiss();
                if (error instanceof ServerError)
                    Toast.makeText(getActivity(), "Server Error", Toast.LENGTH_SHORT).show();
                else if (error instanceof TimeoutError)
                    Toast.makeText(getActivity(), "Connection Timed Out", Toast.LENGTH_SHORT).show();
                else if (error instanceof NetworkError)
                    Toast.makeText(getActivity(), "Bad Network Connection", Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                 params.put("Accept", "application/json");
                 params.put("Authorization","Bearer "+ Session.getAPIToken());
                return params;
            }
        };
        loginRequest.setTag("TAG");
        loginRequest.setShouldCache(false);
        requestQueue.add(loginRequest);

    }





}