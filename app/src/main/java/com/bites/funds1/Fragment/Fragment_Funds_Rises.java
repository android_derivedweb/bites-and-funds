package com.bites.funds1.Fragment;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.bites.funds1.API.GetTotalFundListRequest;
import com.bites.funds1.R;
import com.bites.funds1.Utils.EndlessRecyclerViewScrollListener;
import com.bites.funds1.Utils.UserSession;
import com.bites.funds1.Adapter.FundsAdapter;
import com.bites.funds1.Model.FundsModel;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Fragment_Funds_Rises extends Fragment {
	private RecyclerView category_view;
	private FundsAdapter mAdapter;
	private ArrayList<FundsModel> categoryModels = new ArrayList<>();
	private GridLayoutManager gridLayoutManager;
	private int last_size;
	private RequestQueue requestQueue;
	private UserSession Session;
	// Store instance variables



	// Store instance variables based on arguments passed
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

	}

	// Inflate the view for the fragment based on layout XML
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_fund_raised, container, false);


		Session = new UserSession(getActivity());

		requestQueue = Volley.newRequestQueue(getActivity());//Creating the RequestQueue

		category_view = view.findViewById(R.id.category_view);
		mAdapter = new FundsAdapter(categoryModels, new FundsAdapter.OnItemClickListener() {
			@Override
			public void onItemClick(int item) {

			}
		});
		category_view.setHasFixedSize(true);
		gridLayoutManager = new GridLayoutManager(getActivity().getApplicationContext(), 1);
		category_view.setLayoutManager(gridLayoutManager);
		category_view.setAdapter(mAdapter);
		category_view.setNestedScrollingEnabled(true);


		category_view.addOnScrollListener(new EndlessRecyclerViewScrollListener(gridLayoutManager) {
			@Override
			public void onLoadMore(int page, int totalItemsCount) {

				if (page!=last_size){
					Log.e("FinalOAgeSIze",""+page);
					int finalintpage = page +1;
					GetSupportRastaurant(finalintpage);
				}
			}
		});
		GetSupportRastaurant(1);



		return view;
	}


	protected void replaceFragment(@IdRes int containerViewId,
								   @NonNull Fragment fragment,
								   @NonNull String fragmentTag) {
		getActivity().getSupportFragmentManager()
				.beginTransaction()
				.replace(containerViewId, fragment, fragmentTag)
				.disallowAddToBackStack()
				.commit();
	}

	public void GetSupportRastaurant(int page) {

		final KProgressHUD progressDialog = KProgressHUD.create(getActivity())
				.setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
				.setLabel("Please wait")
				.setCancellable(false)
				.setAnimationSpeed(2)
				.setDimAmount(0.5f)
				.show();

		GetTotalFundListRequest loginRequest = new GetTotalFundListRequest(page,new Response.Listener<String>() {
			@Override
			public void onResponse(String response) {
				Log.e("Response", response + " null");
				progressDialog.dismiss();

				JSONObject jsonObject = null;
				try {
					jsonObject = new JSONObject(response);
					// Toast.makeText(AllCategories.this,jsonObject.getString("ResponseMsg"),Toast.LENGTH_SHORT).show();
					if (jsonObject.getString("ResponseCode").equals("200")) {

						JSONObject objectJSONObject = jsonObject.getJSONObject("data");
						JSONArray jsonArray = objectJSONObject.getJSONArray("data");

						last_size = objectJSONObject.getInt("last_page");
						for(int i =0 ; i<jsonArray.length();i++){
							JSONObject object = jsonArray.getJSONObject(i);

							FundsModel rastaurantModel = new FundsModel();
							rastaurantModel.setUser_name(object.getString("user_name"));
							rastaurantModel.setBite(object.getString("bite"));
							rastaurantModel.setDate(object.getString("date"));
							categoryModels.add(rastaurantModel);
						}

						mAdapter.notifyDataSetChanged();
					}
				} catch (JSONException e) {
					Toast.makeText(getActivity(),e.getMessage(),Toast.LENGTH_SHORT).show();
					e.printStackTrace();
				}

			}
		}, new Response.ErrorListener() {
			@Override
			public void onErrorResponse(VolleyError error) {
				error.printStackTrace();
				progressDialog.dismiss();
				if (error instanceof ServerError)
					Toast.makeText(getActivity(), "Server Error", Toast.LENGTH_SHORT).show();
				else if (error instanceof TimeoutError)
					Toast.makeText(getActivity(), "Connection Timed Out", Toast.LENGTH_SHORT).show();
				else if (error instanceof NetworkError)
					Toast.makeText(getActivity(), "Bad Network Connection", Toast.LENGTH_SHORT).show();
			}
		}){@Override
		public Map<String, String> getHeaders() throws AuthFailureError {
			Map<String, String> params = new HashMap<String, String>();
			params.put("Accept", "application/json");
			params.put("Authorization","Bearer "+ Session.getAPIToken());
			return params;
		}};
		loginRequest.setTag("TAG");loginRequest.setShouldCache(false);
		requestQueue.add(loginRequest);

	}




}