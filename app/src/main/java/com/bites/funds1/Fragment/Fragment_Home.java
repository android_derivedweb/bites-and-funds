package com.bites.funds1.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.bites.funds1.API.GetSupportRastaurantRequest;
import com.bites.funds1.Activity.Activity_FindRestaurant;
import com.bites.funds1.Activity.Activity_FindRestaurantMap;
import com.bites.funds1.Activity.Activity_ResturantRegistrations;
import com.bites.funds1.R;
import com.bites.funds1.Utils.UserSession;
import com.bites.funds1.Adapter.RastaurantAdapter;
import com.bites.funds1.Model.RestaurantModel;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Fragment_Home extends Fragment {
	private UserSession Session;
	private RequestQueue requestQueue;
	private RecyclerView category_view;
	private RastaurantAdapter mAdapter;
	// Store instance variables
	private ArrayList<RestaurantModel> categoryModels = new ArrayList<>();



	// Store instance variables based on arguments passed
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

	}

	// Inflate the view for the fragment based on layout XML
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_home, container, false);

		requestQueue = Volley.newRequestQueue(getActivity());//Creating the RequestQueue

		Session = new UserSession(getActivity());


		category_view = view.findViewById(R.id.category_view);
		mAdapter = new RastaurantAdapter(categoryModels, new RastaurantAdapter.OnItemClickListener() {
			@Override
			public void onItemClick(int item) {
				Intent i = new Intent(getActivity(), Activity_FindRestaurant.class);
				i.putExtra("Id",categoryModels.get(item).getId());
				startActivity(i);
			}
		});
		category_view.setHasFixedSize(true);
		category_view.setLayoutManager(new GridLayoutManager(getActivity(),2));
		category_view.setAdapter(mAdapter);
		category_view.setNestedScrollingEnabled(true);

		GetSupportRastaurant();

		view.findViewById(R.id.see_all).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				Intent i = new Intent(getActivity(), Activity_FindRestaurantMap.class);
				startActivity(i);
			}
		});

		view.findViewById(R.id.bite).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				Intent i = new Intent(getActivity(), Activity_FindRestaurantMap.class);
				startActivity(i);
			}
		});

		view.findViewById(R.id.register).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				Intent i = new Intent(getActivity(), Activity_ResturantRegistrations.class);
				startActivity(i);
			}
		});

		return view;
	}


	protected void replaceFragment(@IdRes int containerViewId,
								   @NonNull Fragment fragment,
								   @NonNull String fragmentTag) {
		getActivity().getSupportFragmentManager()
				.beginTransaction()
				.replace(containerViewId, fragment, fragmentTag)
				.disallowAddToBackStack()
				.commit();
	}


	public void GetSupportRastaurant() {

		final KProgressHUD progressDialog = KProgressHUD.create(getActivity())
				.setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
				.setLabel("Please wait")
				.setCancellable(false)
				.setAnimationSpeed(2)
				.setDimAmount(0.5f)
				.show();

		GetSupportRastaurantRequest loginRequest = new GetSupportRastaurantRequest(new Response.Listener<String>() {
			@Override
			public void onResponse(String response) {
				Log.e("Response", response + " null");
				progressDialog.dismiss();

				JSONObject jsonObject = null;
				try {
					jsonObject = new JSONObject(response);
					// Toast.makeText(AllCategories.this,jsonObject.getString("ResponseMsg"),Toast.LENGTH_SHORT).show();
					if (jsonObject.getString("ResponseCode").equals("200")) {

						JSONArray jsonArray = jsonObject.getJSONArray("data");

						for(int i =0 ; i<jsonArray.length();i++){
							JSONObject object = jsonArray.getJSONObject(i);

							RestaurantModel rastaurantModel = new RestaurantModel();
							rastaurantModel.setRestaurant_image(object.getString("restaurant_image"));
							rastaurantModel.setRestaurant_name(object.getString("restaurant_name"));
							rastaurantModel.setCity(object.getString("city"));
							rastaurantModel.setId(object.getString("restaurant_id"));
							categoryModels.add(rastaurantModel);
						}

						mAdapter.notifyDataSetChanged();
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}

			}
		}, new Response.ErrorListener() {
			@Override
			public void onErrorResponse(VolleyError error) {
				error.printStackTrace();
				progressDialog.dismiss();
				if (error instanceof ServerError)
					Toast.makeText(getActivity(), "Server Error", Toast.LENGTH_SHORT).show();
				else if (error instanceof TimeoutError)
					Toast.makeText(getActivity(), "Connection Timed Out", Toast.LENGTH_SHORT).show();
				else if (error instanceof NetworkError)
					Toast.makeText(getActivity(), "Bad Network Connection", Toast.LENGTH_SHORT).show();
			}
		}){@Override
		public Map<String, String> getHeaders() throws AuthFailureError {
			Map<String, String> params = new HashMap<String, String>();
			params.put("Accept", "application/json");
			//params.put("Authorization","Bearer "+ session.getAPIToken());
			return params;
		}};
		loginRequest.setTag("TAG");loginRequest.setShouldCache(false);
		requestQueue.add(loginRequest);

	}





}