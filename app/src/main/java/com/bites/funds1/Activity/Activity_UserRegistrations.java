package com.bites.funds1.Activity;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.bites.funds1.API.UserRegistrationRequest;
import com.bites.funds1.R;
import com.bites.funds1.Utils.UserSession;
import com.bites.funds1.Utils.WalletHelper;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class Activity_UserRegistrations extends AppCompatActivity {

    private TextView register;
    private EditText mFirstName,mLastName,mEmail,mPassword,mConfrimPassword;
    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    private RequestQueue requestQueue;
    private UserSession Session;
    private WalletHelper wallet;
    private String Crypto_token;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        if (Build.VERSION.SDK_INT >= 21) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(getResources().getColor(R.color.colorPrimary));
        }
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setNavigationBarColor(getResources().getColor(R.color.colorPrimary));
        }
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        requestQueue = Volley.newRequestQueue(Activity_UserRegistrations.this);//Creating the RequestQueue

        Session = new UserSession(Activity_UserRegistrations.this);



        mFirstName = findViewById(R.id.first_name);
        mLastName = findViewById(R.id.last_name);
        mEmail = findViewById(R.id.email);
        mPassword = findViewById(R.id.password);
        mConfrimPassword = findViewById(R.id.password2);


        Long tsLong = System.currentTimeMillis()/1000;
        String ts = tsLong.toString();
        generateWallet(ts);
        findViewById(R.id.submit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (mFirstName.getText().toString().isEmpty()) {
                    Toast.makeText(Activity_UserRegistrations.this, "Please Enter Your FirstName", Toast.LENGTH_SHORT).show();
                }else if (mLastName.getText().toString().isEmpty()) {
                    Toast.makeText(Activity_UserRegistrations.this, "Please Enter Your LastName", Toast.LENGTH_SHORT).show();
                }else if (mEmail.getText().toString().isEmpty()) {
                    Toast.makeText(Activity_UserRegistrations.this, "Please Enter Your Email", Toast.LENGTH_SHORT).show();
                } else if (!mEmail.getText().toString().trim().matches(emailPattern)) {
                    Toast.makeText(Activity_UserRegistrations.this, "Invalid email address", Toast.LENGTH_SHORT).show();
                } else if (mPassword.getText().toString().isEmpty()) {
                    Toast.makeText(Activity_UserRegistrations.this, "Please Enter Your Password", Toast.LENGTH_SHORT).show();
                }else if (!mPassword.getText().toString().equals(mConfrimPassword.getText().toString())) {
                    Toast.makeText(Activity_UserRegistrations.this, "Your Password does not match", Toast.LENGTH_SHORT).show();
                } else {

                    UserRegistration(mFirstName.getText().toString(),
                            mLastName.getText().toString(),
                            mEmail.getText().toString(),
                            mPassword.getText().toString(),
                            mConfrimPassword.getText().toString(),Crypto_token);
                }
            }
        });
        register = findViewById(R.id.register);
        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Activity_UserRegistrations.this, Activity_Login.class);
                startActivity(i);
                finish();
            }
        });
    }


    private void UserRegistration(String FirstNme, String LastNme,String Email,String Password,String Password2,String Crypto_token)  {

        final KProgressHUD progressDialog = KProgressHUD.create(Activity_UserRegistrations.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();

        UserRegistrationRequest loginRequest = new UserRegistrationRequest(FirstNme,LastNme,Email,Password,Password2,Crypto_token,new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("Response", response + " null");
                progressDialog.dismiss();


                JSONObject jsonObject = null;
                try {
                    jsonObject = new JSONObject(response);

                    if(jsonObject.getInt("ResponseCode")==422){
                        Toast.makeText(Activity_UserRegistrations.this,jsonObject.getJSONObject("errors").getJSONArray("email").getString(0),Toast.LENGTH_SHORT).show();

                    }else {

                        showPopup2();
                        Toast.makeText(Activity_UserRegistrations.this, jsonObject.getString("ResponseMsg"), Toast.LENGTH_SHORT).show();

                    }
                }catch (Exception e){
                    Toast.makeText(Activity_UserRegistrations.this, e.getMessage(), Toast.LENGTH_SHORT).show();

                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                progressDialog.dismiss();
                if (error instanceof ServerError)
                    Toast.makeText(Activity_UserRegistrations.this, "Server Error", Toast.LENGTH_SHORT).show();
                else if (error instanceof TimeoutError)
                    Toast.makeText(Activity_UserRegistrations.this, "Connection Timed Out", Toast.LENGTH_SHORT).show();
                else if (error instanceof NetworkError)
                    Toast.makeText(Activity_UserRegistrations.this, "Bad Network Connection", Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                // params.put("Accept", "application/json");
                // params.put("Authorization","Bearer "+ Session.getAPIToken());
                return params;
            }};
        loginRequest.setTag("TAG");loginRequest.setShouldCache(false);
        requestQueue.add(loginRequest);

    }
    private void generateWallet(String Id) {
        wallet = new WalletHelper(Id, this);

        Crypto_token = wallet.getAddress();
        // Toast.makeText(Activity_Login.this,wallet.getAddress(),Toast.LENGTH_SHORT).show();

    }


    private void showPopup2() {
        // custom dialog
        final Dialog dialog = new Dialog(Activity_UserRegistrations.this);
        dialog.setContentView(R.layout.register_dialog);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(true);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);


        dialog.findViewById(R.id.done).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                finish();
            }
        });

        dialog.show();
    }



}