package com.bites.funds1.Activity;

import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bites.funds1.Fragment.Fragment_About;
import com.bites.funds1.Fragment.Fragment_ContactUs;
import com.bites.funds1.Fragment.Fragment_Home;
import com.bites.funds1.R;
import com.bites.funds1.Utils.UserSession;
import com.bites.funds1.Utils.AppContants;

public class Activity_Main extends AppCompatActivity {

    private ImageView drawer_menu;
    private LinearLayout ln_home,ln_bite,ln_about,ln_contact;
    private TextView user,restaurant;
    private UserSession Session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.navigation_view);

        if (Build.VERSION.SDK_INT >= 21) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(getResources().getColor(R.color.colorPrimary));
        }

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setNavigationBarColor(getResources().getColor(R.color.colorPrimary));
        }

        Session = new UserSession(getApplicationContext());
        drawer_menu = findViewById(R.id.menu);
        ln_home = findViewById(R.id.ln_home);
        ln_bite = findViewById(R.id.ln_bite);
        ln_about = findViewById(R.id.ln_about);
        ln_contact = findViewById(R.id.ln_contact);
        user = findViewById(R.id.user);
        restaurant = findViewById(R.id.restaurant);
        final ImageView logo = findViewById(R.id.logo);
       final TextView name = findViewById(R.id.name);


        final DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawer.openDrawer(Gravity.LEFT);
            }
        });

        addFragment(R.id.nav_host_fragment,new Fragment_Home(),"Fragment");

        ln_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                logo.setVisibility(View.VISIBLE);
                name.setVisibility(View.GONE);
                replaceFragment(R.id.nav_host_fragment,new Fragment_Home(),"Fragment");
                drawer.closeDrawer(Gravity.LEFT);
            }
        });
        ln_bite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              /*  logo.setVisibility(View.GONE);
                name.setVisibility(View.VISIBLE);
                name.setText("USER PAID BITE BOUGHT");
                replaceFragment(R.id.nav_host_fragment,new Bites_fragment(),"Fragment");*/

                Intent i = new Intent(Activity_Main.this, Activity_FindRestaurant.class);
                startActivity(i);
                drawer.closeDrawer(Gravity.LEFT);
            }
        });
        ln_about.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                logo.setVisibility(View.GONE);
                name.setVisibility(View.VISIBLE);
                name.setText("ABOUT US");
                replaceFragment(R.id.nav_host_fragment,new Fragment_About(),"Fragment");
                drawer.closeDrawer(Gravity.LEFT);
            }
        });
        ln_contact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                logo.setVisibility(View.GONE);
                name.setVisibility(View.VISIBLE);
                name.setText("CONTACT US");
                replaceFragment(R.id.nav_host_fragment,new Fragment_ContactUs(),"Fragment");
                drawer.closeDrawer(Gravity.LEFT);
            }
        });

        user.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Session.LoginType(AppContants.TYPEUSER);

                Intent i = new Intent(Activity_Main.this, Activity_Login.class);
                startActivity(i);
                finish();
            }
        });

        restaurant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Session.LoginType(AppContants.TYPERASTAURANT);
                Intent i = new Intent(Activity_Main.this, Activity_Login.class);
                startActivity(i);
                finish();
            }
        });

        findViewById(R.id.pp_profile).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showPopup2();
            }
        });
    }



    private void showPopup2() {
        // custom dialog
        final Dialog dialog = new Dialog(Activity_Main.this);
        dialog.setContentView(R.layout.login_dialog);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        dialog.findViewById(R.id.close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        dialog.findViewById(R.id.restaurant).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();

                Session.logout();
                Intent i = new Intent(Activity_Main.this, Activity_Intro_F.class);
                startActivity(i);
                finish();
            }
        });

        dialog.findViewById(R.id.user).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                Session.logout();
                Intent i = new Intent(Activity_Main.this, Activity_Intro_F.class);
                startActivity(i);
                finish();
            }
        });


        dialog.show();
    }


    protected void addFragment(@IdRes int containerViewId,
                               @NonNull Fragment fragment,
                               @NonNull String fragmentTag) {
        getSupportFragmentManager()
                .beginTransaction()
                .add(containerViewId, fragment, fragmentTag)
                .disallowAddToBackStack()
                .commit();
    }

    protected void replaceFragment(@IdRes int containerViewId,
                                   @NonNull Fragment fragment,
                                   @NonNull String fragmentTag) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(containerViewId, fragment, fragmentTag)
                .disallowAddToBackStack()
                .commit();

    }

    @Override
    protected void onStart() {
        super.onStart();

        if(Session.isLoggedIn()){
            if(Session.getLoginType().equals(AppContants.TYPEUSER)){
                Intent i = new Intent(Activity_Main.this, Activity_UserSide.class);
                startActivity(i);
                finish();
            }else {
                Intent i = new Intent(Activity_Main.this, Activity_RestaurantSide.class);
                startActivity(i);
                finish();
            }

        }
    }
}