package com.bites.funds1.Activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.bites.funds1.API.RedeemVoucherRequest;
import com.bites.funds1.API.RestaurantDetailsRequest;
import com.bites.funds1.R;
import com.bites.funds1.Utils.ServerUtils;
import com.bites.funds1.API.UserRestaurantDetailsRequest2;
import com.bites.funds1.Utils.UserSession;
import com.bites.funds1.Utils.AppContants;
import com.bumptech.glide.Glide;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Activity_VoucherDetailsWithBarcode extends AppCompatActivity {

    private TextView mName,mBite,mPercentage,mOffers,mDate,mStatus;
    private RequestQueue requestQueue;
    private UserSession Session;
    private String Id;
    private TextView mVoucher;
    private ImageView mImg;
    private Bitmap myBitmap;
    private ImageView imageview;
    private TextView mReed;
    private LinearLayout mLnstatus;
    private TextView name_is;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_voucher_details);

        if (Build.VERSION.SDK_INT >= 21) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(getResources().getColor(R.color.colorPrimary));
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setNavigationBarColor(getResources().getColor(R.color.colorPrimary));
        }

        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);


        findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        requestQueue = Volley.newRequestQueue(Activity_VoucherDetailsWithBarcode.this);//Creating the RequestQueue

        Session = new UserSession(Activity_VoucherDetailsWithBarcode.this);



        imageview = findViewById(R.id.imageview);
        mLnstatus = findViewById(R.id.mstatus);
        mName = findViewById(R.id.ras_name);
        mBite = findViewById(R.id.ras_bite);
        mVoucher = findViewById(R.id.voucher_id);
        mPercentage = findViewById(R.id.percentage_you_want);
        mOffers = findViewById(R.id.offer);
        mDate = findViewById(R.id.date);
        mStatus = findViewById(R.id.status);
        mReed = findViewById(R.id.reed);
        mImg = findViewById(R.id.img);
        name_is = findViewById(R.id.name_is);


        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();

        if(bundle != null){
            Id = bundle.getString("Id");
        }



            mReed.setText("   Redeem vouchers   ");
            name_is.setText("User Name : ");
            UserDetais2(Id);


        findViewById(R.id.reed).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(Session.getLoginType().equals(AppContants.TYPEUSER)){
                    selectImage();
                }else {
                    mReed.setText("   Redeem vouchers   ");

                    if(mReed.getText().toString().equals("   Redeem vouchers   ")){
                        PostRedeem(Id);
                    }else {
                        Toast.makeText(Activity_VoucherDetailsWithBarcode.this,"You have alredy Redeem this Voucher",Toast.LENGTH_SHORT).show();
                    }

                }

            }
        });

    }


    private void selectImage() {
        try {
            PackageManager pm = Activity_VoucherDetailsWithBarcode.this.getPackageManager();
            int hasPerm = pm.checkPermission(Manifest.permission.CAMERA, Activity_VoucherDetailsWithBarcode.this.getPackageName());
            if (hasPerm == PackageManager.PERMISSION_GRANTED) {
                takeScreenshot();
            } else
                checkAndroidVersion();
            //Toast.makeText(VoucherDetailsActivity.this, "Camera Permission error", Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            checkAndroidVersion();
            //Toast.makeText(VoucherDetailsActivity.this, "Camera Permission error", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }

    private void takeScreenshot() {
        Date now = new Date();
        android.text.format.DateFormat.format("yyyy-MM-dd_hh:mm:ss", now);

        try {
            // image naming and path  to include sd card  appending name you choose for file
            String mPath = Environment.getExternalStorageDirectory().toString() + "/" + now + ".jpg";

            // create bitmap screen capture
            View v1 = getWindow().getDecorView().getRootView();
            v1.setDrawingCacheEnabled(true);
            Bitmap bitmap = Bitmap.createBitmap(v1.getDrawingCache());
            v1.setDrawingCacheEnabled(false);

            File imageFile = new File(mPath);

            FileOutputStream outputStream = new FileOutputStream(imageFile);
            int quality = 100;
            bitmap.compress(Bitmap.CompressFormat.JPEG, quality, outputStream);
            outputStream.flush();
            outputStream.close();

            Toast.makeText(Activity_VoucherDetailsWithBarcode.this,"Screenshot take successfully.",Toast.LENGTH_SHORT).show();

            openScreenshot(imageFile);
        } catch (Throwable e) {
            // Several error may come out with file handling or DOM
            e.printStackTrace();
        }
    }


    private void openScreenshot(File imageFile) {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_VIEW);
        Uri uri = Uri.fromFile(imageFile);
        intent.setDataAndType(uri, "image/*");
        startActivity(intent);
    }

    private void checkAndroidVersion() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkAndRequestPermissions();

        } else {
            // code for lollipop and pre-lollipop devices
        }

    }


    private boolean checkAndRequestPermissions() {
        int camera = ContextCompat.checkSelfPermission(Activity_VoucherDetailsWithBarcode.this,
                Manifest.permission.CAMERA);
        int wtite = ContextCompat.checkSelfPermission(Activity_VoucherDetailsWithBarcode.this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int read = ContextCompat.checkSelfPermission(Activity_VoucherDetailsWithBarcode.this, Manifest.permission.READ_EXTERNAL_STORAGE);
        List<String> listPermissionsNeeded = new ArrayList<>();
        if (wtite != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        if (camera != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.CAMERA);
        }
        if (read != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(Activity_VoucherDetailsWithBarcode.this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), REQUEST_ID_MULTIPLE_PERMISSIONS);
            return false;
        }
        return true;
    }

    public static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 7;

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        Log.d("in fragment on request", "Permission callback called-------");
        switch (requestCode) {
            case REQUEST_ID_MULTIPLE_PERMISSIONS: {

                Map<String, Integer> perms = new HashMap<>();
                // Initialize the map with both permissions
                perms.put(Manifest.permission.WRITE_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.CAMERA, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.READ_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                // Fill with actual results from user
                if (grantResults.length > 0) {
                    for (int i = 0; i < permissions.length; i++)
                        perms.put(permissions[i], grantResults[i]);
                    // Check for both permissions
                    if (perms.get(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                            && perms.get(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED && perms.get(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                        Log.d("in fragment on request", "CAMERA & WRITE_EXTERNAL_STORAGE READ_EXTERNAL_STORAGE permission granted");
                        // process the normal flow
                        //else any one or both the permissions are not granted
                    } else {
                        Log.d("in fragment on request", "Some permissions are not granted ask again ");
                        //permission is denied (this is the first time, when "never ask again" is not checked) so ask again explaining the usage of permission
//                        // shouldShowRequestPermissionRationale will return true
                        //show the dialog or snackbar saying its necessary and try again otherwise proceed with setup.
                        if (ActivityCompat.shouldShowRequestPermissionRationale(Activity_VoucherDetailsWithBarcode.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) || ActivityCompat.shouldShowRequestPermissionRationale(Activity_VoucherDetailsWithBarcode.this, Manifest.permission.CAMERA) || ActivityCompat.shouldShowRequestPermissionRationale(Activity_VoucherDetailsWithBarcode.this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                            showDialogOK("Camera and Storage Permission required for this app",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            switch (which) {
                                                case DialogInterface.BUTTON_POSITIVE:
                                                    checkAndRequestPermissions();
                                                    break;
                                                case DialogInterface.BUTTON_NEGATIVE:
                                                    // proceed with logic by disabling the related features or quit the app.
                                                    break;
                                            }
                                        }
                                    });
                        }
                        //permission is denied (and never ask again is  checked)
                        //shouldShowRequestPermissionRationale will return false
                        else {
                            Toast.makeText(Activity_VoucherDetailsWithBarcode.this, "Go to settings and enable permissions", Toast.LENGTH_LONG)
                                    .show();
                            //                            //proceed with logic by disabling the related features or quit the app.
                        }
                    }
                }
            }
        }

    }

    private void showDialogOK(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(Activity_VoucherDetailsWithBarcode.this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", okListener)
                .create()
                .show();
    }




    private void UserDetais(String id)  {


        Log.e("Token",Session.getAPIToken());
        final KProgressHUD progressDialog = KProgressHUD.create(Activity_VoucherDetailsWithBarcode.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();

        UserRestaurantDetailsRequest2 loginRequest = new UserRestaurantDetailsRequest2(id,new Response.Listener<String>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(String response) {
                Log.e("Response", response + " null");
                progressDialog.dismiss();


                JSONObject jsonObject = null;
                try {
                    jsonObject = new JSONObject(response);

                    JSONObject jsonObject1 = jsonObject.getJSONObject("data");

                    mVoucher.setText("Vouchers Id :  "+jsonObject1.getString("voucher_code"));
                    mStatus.setText(jsonObject1.getString("status").toUpperCase());
                    mBite.setText(jsonObject1.getString("bite"));
                    mDate.setText(jsonObject1.getString("date_added"));
                    mName.setText(jsonObject1.getString("first_name")+" "+jsonObject1.getString("last_name"));
                  //  mOffers.setText(jsonObject1.getString("offer"));
                  //  mPercentage.setText(jsonObject1.getString("percentage_you_want"));

                    Glide.with(Activity_VoucherDetailsWithBarcode.this).load(jsonObject1.getString("profile_pic")).placeholder(R.drawable.placeholder).into(mImg);



                    try
                    {
                        myBitmap = CreateImage(jsonObject1.getString("qr_url"));
                        imageview.setImageBitmap(myBitmap);
                    }
                    catch (WriterException we)
                    {
                        we.printStackTrace();
                    }
                    if (myBitmap != null)
                    {
                        imageview.setImageBitmap(myBitmap);
                    }
                    Toast.makeText(Activity_VoucherDetailsWithBarcode.this,jsonObject.getString("ResponseMsg"),Toast.LENGTH_SHORT).show();



                }catch (Exception e){
                    Toast.makeText(Activity_VoucherDetailsWithBarcode.this,e.getMessage(),Toast.LENGTH_SHORT).show();

                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                progressDialog.dismiss();
                Session.logout();
                Intent i = new Intent(getApplicationContext(), Activity_Intro_F.class);
                finish();
                startActivity(i);
                if (error instanceof ServerError)
                    Toast.makeText(Activity_VoucherDetailsWithBarcode.this, "Server Error", Toast.LENGTH_SHORT).show();
                else if (error instanceof TimeoutError)
                    Toast.makeText(Activity_VoucherDetailsWithBarcode.this, "Connection Timed Out", Toast.LENGTH_SHORT).show();
                else if (error instanceof NetworkError)
                    Toast.makeText(Activity_VoucherDetailsWithBarcode.this, "Bad Network Connection", Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                params.put("Authorization","Bearer "+ Session.getAPIToken());
                return params;
            }};
        loginRequest.setTag("TAG");loginRequest.setShouldCache(false);
        requestQueue.add(loginRequest);

    }



    public Bitmap CreateImage(String message) throws WriterException
    {
        BitMatrix bitMatrix = null;
        // BitMatrix bitMatrix = new MultiFormatWriter().encode(message, BarcodeFormat.QR_CODE, size, size);

        // bitMatrix = new MultiFormatWriter().encode(message, BarcodeFormat.QR_CODE, 150, 150);break;
        bitMatrix = new MultiFormatWriter().encode(message, BarcodeFormat.QR_CODE, 150, 150);

        int width = bitMatrix.getWidth();
        int height = bitMatrix.getHeight();
        int [] pixels = new int [width * height];
        for (int i = 0 ; i < height ; i++)
        {
            for (int j = 0 ; j < width ; j++)
            {
                if (bitMatrix.get(j, i))
                {
                    pixels[i * width + j] = 0xff000000;
                }
                else
                {
                    pixels[i * width + j] = 0xffffffff;
                }
            }
        }
        Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        bitmap.setPixels(pixels, 0, width, 0, 0, width, height);
        return bitmap;
    }



    private void Redeem(final String id)  {

        Log.e("Response", id + " null");
        final KProgressHUD progressDialog = KProgressHUD.create(Activity_VoucherDetailsWithBarcode.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();

     /*   AndroidNetworking.post(ServerUtils.BASE_URL+"restaurant/redeem-voucher")
                .addBodyParameter("voucher_id",id)
                .addHeaders("Accept","application/json")
                .addHeaders("Authorization","Bearer "+ Session.getAPIToken())
                .build()
                .getAsString(new StringRequestListener() {
                    @Override
                    public void onResponse(String response) {
                        progressDialog.dismiss();
                        Log.e("Response", response + " null");

                        JSONObject jsonObject = null;
                        try {
                            jsonObject = new JSONObject(response);

                            UserDetais2(id);

                            mLnstatus.setBackgroundColor(getResources().getColor(R.color.red));
                            mReed.setText("   Redeemed Vouchers   ");
                            mReed.setBackground(getResources().getDrawable(R.drawable.red_border2));
                            mReed.setTextColor(getResources().getColor(R.color.red));

                            Toast.makeText(Activity_VoucherDetailsWithBarcode.this,jsonObject.getString("ResponseMsg"),Toast.LENGTH_SHORT).show();
                        }catch (Exception ignored){

                        }

                    }

                    @Override
                    public void onError(ANError anError) {

                    }
                });
*/
        RedeemVoucherRequest loginRequest = new RedeemVoucherRequest(id,new Response.Listener<String>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(String response) {
                progressDialog.dismiss();
                Log.e("Response", response + " null");

                JSONObject jsonObject = null;
                try {
                    jsonObject = new JSONObject(response);

                    UserDetais2(id);

                    mLnstatus.setBackgroundColor(getResources().getColor(R.color.red));
                    mReed.setText("   Redeemed Vouchers   ");
                    mReed.setBackground(getResources().getDrawable(R.drawable.red_border2));
                    mReed.setTextColor(getResources().getColor(R.color.red));

                    Toast.makeText(Activity_VoucherDetailsWithBarcode.this,jsonObject.getString("ResponseMsg"),Toast.LENGTH_SHORT).show();
                }catch (Exception ignored){

                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                progressDialog.dismiss();
                if (error instanceof ServerError)
                    Toast.makeText(Activity_VoucherDetailsWithBarcode.this, "Server Error", Toast.LENGTH_SHORT).show();
                else if (error instanceof TimeoutError)
                    Toast.makeText(Activity_VoucherDetailsWithBarcode.this, "Connection Timed Out", Toast.LENGTH_SHORT).show();
                else if (error instanceof NetworkError)
                    Toast.makeText(Activity_VoucherDetailsWithBarcode.this, "Bad Network Connection", Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                params.put("Authorization","Bearer "+ Session.getAPIToken());
                return params;
            }};
        loginRequest.setTag("TAG");
        requestQueue.add(loginRequest);

    }


    private void PostRedeem(final String id) {

        final KProgressHUD progressDialog = KProgressHUD.create(Activity_VoucherDetailsWithBarcode.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();

        final HashMap<String, String> postParams = new HashMap<String, String>();
        postParams.put("voucher_id", id);


        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                ServerUtils.BASE_URL+"restaurant/redeem-voucher", new JSONObject(postParams),
                new com.android.volley.Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("ResponseNet", response + " null");

                        progressDialog.dismiss();
                        Log.e("Response", response + " null");

                        try {


                            UserDetais2(id);

                            mLnstatus.setBackgroundColor(getResources().getColor(R.color.red));
                            mReed.setText("   Redeemed Vouchers   ");
                            mReed.setBackground(getResources().getDrawable(R.drawable.red_border2));
                            mReed.setTextColor(getResources().getColor(R.color.red));

                            Toast.makeText(Activity_VoucherDetailsWithBarcode.this, response.getString("ResponseMsg"),Toast.LENGTH_SHORT).show();
                        }catch (Exception ignored){

                        }

                    }
                }, new com.android.volley.Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("Error" ,""+ error.getLocalizedMessage());
                Toast.makeText(Activity_VoucherDetailsWithBarcode.this, ""+error.getLocalizedMessage(), Toast.LENGTH_SHORT).show();

                progressDialog.dismiss();
                Session.logout();
                Intent i = new Intent(getApplicationContext(), Activity_Intro_F.class);
                finish();
                startActivity(i);
            }
        }) {

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                params.put("Authorization","Bearer "+ Session.getAPIToken());
                return params;
            }
        };

        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(30000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        // Adding request to request queue
        requestQueue.add(jsonObjReq);
    }


    private void UserDetais2(final String id)  {

        final KProgressHUD progressDialog = KProgressHUD.create(Activity_VoucherDetailsWithBarcode.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();

        RestaurantDetailsRequest loginRequest = new RestaurantDetailsRequest(id,new Response.Listener<String>() {
            @SuppressLint({"SetTextI18n", "UseCompatLoadingForDrawables"})
            @Override
            public void onResponse(String response) {
                Log.e("Response", response + " null");
                progressDialog.dismiss();


                JSONObject jsonObject = null;
                try {
                    jsonObject = new JSONObject(response);

                    if(jsonObject.getInt("ResponseCode")==200){
                        JSONObject jsonObject1 = jsonObject.getJSONObject("data");

                        mVoucher.setText("Vouchers Id :  "+jsonObject1.getString("voucher_code"));
                        mStatus.setText(jsonObject1.getString("status").toUpperCase());
                        mBite.setText(jsonObject1.getString("bite"));
                        mDate.setText(jsonObject1.getString("date_added"));
                        mName.setText(jsonObject1.getString("first_name") + " "+ jsonObject1.getString("last_name"));
                        //   mOffers.setText(jsonObject1.getString("offer"));
                        //  mPercentage.setText(jsonObject1.getString("percentage_you_want"));


                        if(jsonObject1.getString("status").equals("active")){
                            mReed.setText("   Redeem   ");
                        }else {
                            mLnstatus.setBackgroundColor(getResources().getColor(R.color.red));
                            mReed.setText("   Redeemed Vouchers   ");
                            mReed.setBackground(getResources().getDrawable(R.drawable.red_border2));
                            mReed.setTextColor(getResources().getColor(R.color.red));
                        }

                        try
                        {
                            myBitmap = CreateImage(id);
                            imageview.setImageBitmap(myBitmap);
                        }
                        catch (WriterException we)
                        {
                            we.printStackTrace();
                        }
                        if (myBitmap != null)
                        {
                            imageview.setImageBitmap(myBitmap);
                        }


                        Glide.with(Activity_VoucherDetailsWithBarcode.this).load(jsonObject1.getString("profile_pic")).placeholder(R.drawable.placeholder).into(mImg);



                        try
                        {
                            myBitmap = CreateImage(id);
                            imageview.setImageBitmap(myBitmap);
                        }
                        catch (WriterException we)
                        {
                            we.printStackTrace();
                        }
                        if (myBitmap != null)
                        {
                            imageview.setImageBitmap(myBitmap);
                        }

                        Toast.makeText(Activity_VoucherDetailsWithBarcode.this,jsonObject.getString("ResponseMsg"),Toast.LENGTH_SHORT).show();

                    }else {
                        finish();
                        Toast.makeText(Activity_VoucherDetailsWithBarcode.this,jsonObject.getString("ResponseMsg"),Toast.LENGTH_SHORT).show();

                    }




                }catch (Exception e){

                    Toast.makeText(Activity_VoucherDetailsWithBarcode.this,e.getMessage(),Toast.LENGTH_SHORT).show();

                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                progressDialog.dismiss();
                if (error instanceof ServerError)
                    Toast.makeText(Activity_VoucherDetailsWithBarcode.this, "Server Error", Toast.LENGTH_SHORT).show();
                else if (error instanceof TimeoutError)
                    Toast.makeText(Activity_VoucherDetailsWithBarcode.this, "Connection Timed Out", Toast.LENGTH_SHORT).show();
                else if (error instanceof NetworkError)
                    Toast.makeText(Activity_VoucherDetailsWithBarcode.this, "Bad Network Connection", Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                params.put("Authorization","Bearer "+ Session.getAPIToken());
                return params;
            }};
        loginRequest.setTag("TAG");loginRequest.setShouldCache(false);
        requestQueue.add(loginRequest);

    }


}