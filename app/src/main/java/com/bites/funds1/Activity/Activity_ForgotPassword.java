package com.bites.funds1.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.bites.funds1.API.RastaurantForgotRequest;
import com.bites.funds1.API.UserForgotRequest;
import com.bites.funds1.R;
import com.bites.funds1.Utils.ServerUtils;
import com.bites.funds1.Utils.UserSession;
import com.bites.funds1.Utils.AppContants;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class Activity_ForgotPassword extends AppCompatActivity {

    RequestQueue requestQueue;
    private UserSession Session;
    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    EditText Email;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot);

        if (Build.VERSION.SDK_INT >= 21) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(getResources().getColor(R.color.colorPrimary));
        }

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setNavigationBarColor(getResources().getColor(R.color.colorPrimary));
        }
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        requestQueue = Volley.newRequestQueue(Activity_ForgotPassword.this);//Creating the RequestQueue

        Session = new UserSession(Activity_ForgotPassword.this);


        Email = findViewById(R.id.email);

        findViewById(R.id.submit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(Email.getText().toString().isEmpty()){
                    Toast.makeText(Activity_ForgotPassword.this,"Please Enter Your Email",Toast.LENGTH_SHORT).show();
                }else if (!Email.getText().toString().trim().matches(emailPattern)) {
                    Toast.makeText(getApplicationContext(),"Invalid email address",Toast.LENGTH_SHORT).show();
                }else {
                    if(Session.getLoginType().equals(AppContants.TYPEUSER)){
                        PostUsers(Email.getText().toString());
                       // UserForgot(Email.getText().toString());
                    }else {
                        PostRestaurant(Email.getText().toString());
//                        RastaurantForgot(Email.getText().toString());

                    }
                }
            }
        });

    }


    private void UserForgot(final String EmailId)  {

        final KProgressHUD progressDialog = KProgressHUD.create(Activity_ForgotPassword.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();

       /* AndroidNetworking.post(ServerUtils.BASE_URL+"user/forgot-password")
                .addBodyParameter("email",EmailId)
                .build()
                .getAsString(new StringRequestListener() {
                    @Override
                    public void onResponse(String response) {
                        progressDialog.dismiss();
                        Log.e("Response", response + " null");

                        JSONObject jsonObject = null;
                        try {
                            jsonObject = new JSONObject(response);
                            Toast.makeText(Activity_ForgotPassword.this,jsonObject.getString("ResponseMsg"),Toast.LENGTH_SHORT).show();

                            if(jsonObject.getInt("ResponseCode")==200){
                                Intent intent1 = new Intent(Activity_ForgotPassword.this, Activity_VerifyOTP.class);
                                intent1.putExtra("email",EmailId);
                                startActivity(intent1);
                                finish();
                            }

                        }catch (Exception ignored){

                        }

                    }

                    @Override
                    public void onError(ANError anError) {

                    }
                });*/

        UserForgotRequest loginRequest = new UserForgotRequest(EmailId,new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                progressDialog.dismiss();
                Log.e("Response", response + " null");

                JSONObject jsonObject = null;
                try {
                    jsonObject = new JSONObject(response);
                    Toast.makeText(Activity_ForgotPassword.this,jsonObject.getString("ResponseMsg"),Toast.LENGTH_SHORT).show();

                    if(jsonObject.getInt("ResponseCode")==200){
                        Intent intent1 = new Intent(Activity_ForgotPassword.this, Activity_VerifyOTP.class);
                        intent1.putExtra("email",EmailId);
                        startActivity(intent1);
                        finish();
                    }

                }catch (Exception ignored){

                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                progressDialog.dismiss();
                if (error instanceof ServerError)
                    Toast.makeText(Activity_ForgotPassword.this, "Server Error", Toast.LENGTH_SHORT).show();
                else if (error instanceof TimeoutError)
                    Toast.makeText(Activity_ForgotPassword.this, "Connection Timed Out", Toast.LENGTH_SHORT).show();
                else if (error instanceof NetworkError)
                    Toast.makeText(Activity_ForgotPassword.this, "Bad Network Connection", Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                 params.put("Accept", "application/json");
                 params.put("Authorization","Bearer "+ Session.getAPIToken());
                return params;
            }};
        loginRequest.setTag("TAG");
        requestQueue.add(loginRequest);

    }

    private void RastaurantForgot(final String Email)  {

        final KProgressHUD progressDialog = KProgressHUD.create(Activity_ForgotPassword.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();

     /*   AndroidNetworking.post(ServerUtils.BASE_URL+"restaurant/forgot-password")
                .addBodyParameter("email",Email)
                .build()
                .getAsString(new StringRequestListener() {
                    @Override
                    public void onResponse(String response) {
                        progressDialog.dismiss();
                        Log.e("Response", response + " null");

                        JSONObject jsonObject = null;
                        try {
                            jsonObject = new JSONObject(response);
                            Toast.makeText(Activity_ForgotPassword.this,jsonObject.getString("ResponseMsg"),Toast.LENGTH_SHORT).show();

                            if(jsonObject.getInt("ResponseCode")==200){
                                Intent intent1 = new Intent(Activity_ForgotPassword.this, Activity_VerifyOTP.class);
                                intent1.putExtra("email",Email);
                                startActivity(intent1);
                                finish();
                            }

                        }catch (Exception ignored){

                        }

                    }

                    @Override
                    public void onError(ANError anError) {

                    }
                });*/

        RastaurantForgotRequest loginRequest = new RastaurantForgotRequest(Email,new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                progressDialog.dismiss();
                Log.e("Response", response + " null");

                JSONObject jsonObject = null;
                try {
                    jsonObject = new JSONObject(response);
                    Toast.makeText(Activity_ForgotPassword.this,jsonObject.getString("ResponseMsg"),Toast.LENGTH_SHORT).show();

                    if(jsonObject.getInt("ResponseCode")==200){
                        Intent intent1 = new Intent(Activity_ForgotPassword.this, Activity_VerifyOTP.class);
                        intent1.putExtra("email",Email);
                        startActivity(intent1);
                        finish();
                    }

                }catch (Exception ignored){

                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                progressDialog.dismiss();
                if (error instanceof ServerError)
                    Toast.makeText(Activity_ForgotPassword.this, "Server Error", Toast.LENGTH_SHORT).show();
                else if (error instanceof TimeoutError)
                    Toast.makeText(Activity_ForgotPassword.this, "Connection Timed Out", Toast.LENGTH_SHORT).show();
                else if (error instanceof NetworkError)
                    Toast.makeText(Activity_ForgotPassword.this, "Bad Network Connection", Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                 params.put("Accept", "application/json");
                 params.put("Authorization","Bearer "+ Session.getAPIToken());
                return params;
            }};
        loginRequest.setTag("TAG");
        requestQueue.add(loginRequest);

    }


    private void PostUsers(final String emailPattern) {

        final KProgressHUD progressDialog = KProgressHUD.create(Activity_ForgotPassword.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();

        final HashMap<String, String> postParams = new HashMap<String, String>();
        postParams.put("email", emailPattern);

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                ServerUtils.BASE_URL+"user/forgot-password", new JSONObject(postParams),
                new com.android.volley.Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        progressDialog.dismiss();
                        Log.e("Response", response + " null");

                        try {

                            Toast.makeText(Activity_ForgotPassword.this, response.getString("ResponseMsg"),Toast.LENGTH_SHORT).show();

                            if(response.getInt("ResponseCode")==200){
                                Intent intent1 = new Intent(Activity_ForgotPassword.this, Activity_VerifyOTP.class);
                                intent1.putExtra("email",emailPattern);
                                startActivity(intent1);
                                finish();
                            }

                        }catch (Exception ignored){
                            try {
                                Toast.makeText(Activity_ForgotPassword.this, response.getString("ResponseMsg"),Toast.LENGTH_SHORT).show();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    }
                }, new com.android.volley.Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.e("TAG", "Error: " + error.getMessage());
                progressDialog.dismiss();
                Session.logout();
                Intent i = new Intent(getApplicationContext(), Activity_Intro_F.class);
                finish();
                startActivity(i);
            }
        }) {

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                params.put("Authorization","Bearer "+ Session.getAPIToken());
                return params;
            }
        };

        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(8000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        // Adding request to request queue
        requestQueue.add(jsonObjReq);
    }

    private void PostRestaurant(final String emailPattern) {

        final KProgressHUD progressDialog = KProgressHUD.create(Activity_ForgotPassword.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();

        final HashMap<String, String> postParams = new HashMap<String, String>();
        postParams.put("email", emailPattern);

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                ServerUtils.BASE_URL+"restaurant/forgot-password", new JSONObject(postParams),
                new com.android.volley.Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        progressDialog.dismiss();
                        Log.e("Response", response + " null");

                        try {

                            Toast.makeText(Activity_ForgotPassword.this, response.getString("ResponseMsg"),Toast.LENGTH_SHORT).show();

                            if(response.getInt("ResponseCode")==200){
                                Intent intent1 = new Intent(Activity_ForgotPassword.this, Activity_VerifyOTP.class);
                                intent1.putExtra("email",emailPattern);
                                startActivity(intent1);
                                finish();
                            }

                        }catch (Exception ignored){
                            try {
                                Toast.makeText(Activity_ForgotPassword.this, response.getString("ResponseMsg"),Toast.LENGTH_SHORT).show();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    }
                }, new com.android.volley.Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.e("TAG", "Error: " + error.getMessage());
                progressDialog.dismiss();
                Session.logout();
                Intent i = new Intent(getApplicationContext(), Activity_Intro_F.class);
                finish();
                startActivity(i);
            }
        }) {

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                params.put("Authorization","Bearer "+ Session.getAPIToken());
                return params;
            }
        };

        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(8000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        // Adding request to request queue
        requestQueue.add(jsonObjReq);
    }



}