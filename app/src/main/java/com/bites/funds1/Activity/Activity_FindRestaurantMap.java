package com.bites.funds1.Activity;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.bites.funds1.API.GetCityRequest;
import com.bites.funds1.API.GetRastaurantRequest;
import com.bites.funds1.API.GetRastaurantRequestSearch;
import com.bites.funds1.API.GetTypeRequest;
import com.bites.funds1.R;
import com.bites.funds1.Utils.EndlessRecyclerViewScrollListener;
import com.bites.funds1.Utils.UserSession;
import com.bites.funds1.Adapter.RastaurantAdapter;
import com.bites.funds1.Model.RestaurantModel;
import com.bites.funds1.Utils.AppContants;
import com.bumptech.glide.Glide;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static com.bites.funds1.Utils.GetLocation.RequestPermissionCode;
import static com.bites.funds1.Activity.Activity_UserSide.IsBuyBITE;

public class Activity_FindRestaurantMap extends AppCompatActivity  implements OnMapReadyCallback , LocationListener,GoogleMap.OnMarkerClickListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private UserSession Session;
    private ArrayList<RestaurantModel> categoryModels = new ArrayList<>();
    private RequestQueue requestQueue;
    private RecyclerView category_view;
    private RastaurantAdapter mAdapter;
    private String mCityId,mTypeId;
    private Spinner mType,mCity;
    private GridLayoutManager gridLayoutManager;
    private int last_size;
    String latitude = "1.0", longitude = "1.0";

    private HashMap<Marker, RestaurantModel> markerMap = new HashMap<Marker, RestaurantModel>();

    public double currentLatitude;
    public double currentLongitude;
    private GoogleMap mMap;
    public GoogleApiClient googleApiClient;
    Marker marker;
    private double lat,lng;

    private static final int REQUEST_LOCATION = 1;
    private LocationManager nManager;
    private EditText search;
    private ImageView search_btn;

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_find_restaurant_map);


        if (Build.VERSION.SDK_INT >= 21) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(getResources().getColor(R.color.colorPrimary));
        }

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setNavigationBarColor(getResources().getColor(R.color.colorPrimary));
        }
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);




        //defining the buttons
        //calling google api's client to establish location connections
        googleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

        //fusedLocationProviderClient provides the current location coordinates
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            NotificationChannel channel = new NotificationChannel("MyNotification","MyNotification", NotificationManager.IMPORTANCE_DEFAULT);
            NotificationManager manager = getSystemService(NotificationManager.class);
            manager.createNotificationChannel(channel);
        }






        Session = new UserSession(Activity_FindRestaurantMap.this);
        requestQueue = Volley.newRequestQueue(Activity_FindRestaurantMap.this);//Creating the RequestQueue
        
        findViewById(R.id.profile).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showPopup2();
            }
        });

        findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        search = (EditText) findViewById(R.id.search);
        search_btn = (ImageView) findViewById(R.id.search_btn);
        mType = (Spinner) findViewById(R.id.type);
        mCity = (Spinner) findViewById(R.id.city);



        if(IsBuyBITE){
            showPopup();
        }

        category_view = findViewById(R.id.category_view);
        mAdapter = new RastaurantAdapter(categoryModels, new RastaurantAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int item) {

                if(IsBuyBITE){
                    Intent i = new Intent(Activity_FindRestaurantMap.this, Activity_FindRestaurantSecond.class);
                    i.putExtra("Value",1);
                    i.putExtra("Id",categoryModels.get(item).getId());
                    startActivity(i);
                }else {
                    Intent i = new Intent(Activity_FindRestaurantMap.this, Activity_FindRestaurantSecond.class);
                    i.putExtra("Value",2);
                    i.putExtra("Id",categoryModels.get(item).getId());
                    startActivity(i);
                }
/*                Intent i = new Intent(FindRestaurantMap.this, FindRestaurant.class);
                i.putExtra("Id",categoryModels.get(item).getId());
                startActivity(i);*/
            }
        });
        category_view.setHasFixedSize(true);
        gridLayoutManager = new GridLayoutManager(Activity_FindRestaurantMap.this.getApplicationContext(), 2);
        category_view.setLayoutManager(gridLayoutManager);
        category_view.setAdapter(mAdapter);
        category_view.setNestedScrollingEnabled(true);


        category_view.addOnScrollListener(new EndlessRecyclerViewScrollListener(gridLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {

                if (page!=last_size){
                    Log.e("FinalOAgeSIze",""+page);
                    int finalintpage = page +1;
                    GetSupportRastaurant(finalintpage,latitude,longitude);
                }
            }
        });

        GetType();
        GetCity();


        nManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (!nManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
           // OnGPS();
        } else {
          //  getLocation();
        }


        GetSupportRastaurant(1,latitude,longitude);


        search_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(search.getText().toString().isEmpty()){
                    Toast.makeText(Activity_FindRestaurantMap.this,"Please enter some word for search...",Toast.LENGTH_SHORT).show();
                }else {
                    GetSupportRastaurant2(search.getText().toString());
                }
            }
        });


        search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                categoryModels.clear();

                if(charSequence.toString().isEmpty()){
                    GetSupportRastaurant(1,latitude,longitude);
                }

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

    }

    private void showPopup2() {
        // custom dialog
        final Dialog dialog = new Dialog(Activity_FindRestaurantMap.this);
        dialog.setContentView(R.layout.login_dialog);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        dialog.findViewById(R.id.close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        dialog.findViewById(R.id.restaurant).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();

                Session.logout();
                Intent i = new Intent(Activity_FindRestaurantMap.this, Activity_Intro_F.class);
                startActivity(i);
                finish();
            }
        });

        dialog.findViewById(R.id.user).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                Session.logout();
                Intent i = new Intent(Activity_FindRestaurantMap.this, Activity_Intro_F.class);
                startActivity(i);
                finish();
            }
        });


        dialog.show();
    }


    @SuppressLint("SetTextI18n")
    private void showPopupMap(final String Id, String Image, String name, String type ) {
        // custom dialog
        final Dialog dialog = new Dialog(Activity_FindRestaurantMap.this);
        dialog.setContentView(R.layout.map_popup);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        dialog.findViewById(R.id.close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        dialog.findViewById(R.id.view_details).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();

                Intent i = new Intent(Activity_FindRestaurantMap.this, Activity_FindRestaurant.class);
                i.putExtra("Id",Id);
                startActivity(i);
            }
        });

        ImageView imageView = dialog.findViewById(R.id.imageview);
        TextView name_txt = dialog.findViewById(R.id.name);
        TextView type_txt = dialog.findViewById(R.id.type);

        Glide.with(Activity_FindRestaurantMap.this).load(Image).placeholder(R.drawable.placeholder).circleCrop().into(imageView);
        name_txt.setText("Restaurant Name : "+name);
        type_txt.setText("City : "+type);

        dialog.show();
    }



    public void GetSupportRastaurant(int page,String latitude,String longitude) {

        final KProgressHUD progressDialog = KProgressHUD.create(Activity_FindRestaurantMap.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();

        GetRastaurantRequest loginRequest = new GetRastaurantRequest(page,latitude,longitude,new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("Response", response + " null");
                progressDialog.dismiss();

                JSONObject jsonObject = null;
                try {
                    jsonObject = new JSONObject(response);
                    // Toast.makeText(AllCategories.this,jsonObject.getString("ResponseMsg"),Toast.LENGTH_SHORT).show();
                    if (jsonObject.getString("ResponseCode").equals("200")) {

                        JSONObject objectJSONObject = jsonObject.getJSONObject("data");
                        JSONArray jsonArray = objectJSONObject.getJSONArray("data");

                        last_size = objectJSONObject.getInt("last_page");
                        for(int i =0 ; i<jsonArray.length();i++){
                            JSONObject object = jsonArray.getJSONObject(i);

                            RestaurantModel rastaurantModel = new RestaurantModel();
                            rastaurantModel.setRestaurant_image(object.getString("restaurant_image"));
                            rastaurantModel.setRestaurant_name(object.getString("restaurant_name"));
                            rastaurantModel.setCity(object.getString("city"));
                            rastaurantModel.setId(object.getString("restaurant_id"));
                            rastaurantModel.setLat(Double.parseDouble(object.getString("latitude")));
                            rastaurantModel.setLong(Double.parseDouble(object.getString("longitude")));
                            categoryModels.add(rastaurantModel);
                        }

                        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
                        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                                .findFragmentById(R.id.map);
                        mapFragment.getMapAsync(Activity_FindRestaurantMap.this);

                        mAdapter.notifyDataSetChanged();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                progressDialog.dismiss();
                if (error instanceof ServerError)
                    Toast.makeText(Activity_FindRestaurantMap.this, "Server Error", Toast.LENGTH_SHORT).show();
                else if (error instanceof TimeoutError)
                    Toast.makeText(Activity_FindRestaurantMap.this, "Connection Timed Out", Toast.LENGTH_SHORT).show();
                else if (error instanceof NetworkError)
                    Toast.makeText(Activity_FindRestaurantMap.this, "Bad Network Connection", Toast.LENGTH_SHORT).show();
            }
        }){@Override
        public Map<String, String> getHeaders() throws AuthFailureError {
            Map<String, String> params = new HashMap<String, String>();
            params.put("Accept", "application/json");
            //params.put("Authorization","Bearer "+ session.getAPIToken());
            return params;
        }};
        loginRequest.setTag("TAG");loginRequest.setShouldCache(false);
        requestQueue.add(loginRequest);

    }


    public void GetSupportRastaurant2(String search) {

        final KProgressHUD progressDialog = KProgressHUD.create(Activity_FindRestaurantMap.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();

        GetRastaurantRequestSearch loginRequest = new GetRastaurantRequestSearch(search,new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("Response", response + " null");
                progressDialog.dismiss();
                categoryModels.clear();
                JSONObject jsonObject = null;
                try {
                    jsonObject = new JSONObject(response);
                    // Toast.makeText(AllCategories.this,jsonObject.getString("ResponseMsg"),Toast.LENGTH_SHORT).show();
                    if (jsonObject.getString("ResponseCode").equals("200")) {

                        JSONObject objectJSONObject = jsonObject.getJSONObject("data");
                        JSONArray jsonArray = objectJSONObject.getJSONArray("data");

                        last_size = objectJSONObject.getInt("last_page");
                        for(int i =0 ; i<jsonArray.length();i++){
                            JSONObject object = jsonArray.getJSONObject(i);

                            RestaurantModel rastaurantModel = new RestaurantModel();
                            rastaurantModel.setRestaurant_image(object.getString("restaurant_image"));
                            rastaurantModel.setRestaurant_name(object.getString("restaurant_name"));
                            rastaurantModel.setCity(object.getString("city"));
                            rastaurantModel.setId(object.getString("restaurant_id"));
                            rastaurantModel.setLat(Double.parseDouble(object.getString("latitude")));
                            rastaurantModel.setLong(Double.parseDouble(object.getString("longitude")));
                            categoryModels.add(rastaurantModel);
                        }

                        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
                        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                                .findFragmentById(R.id.map);
                        mapFragment.getMapAsync(Activity_FindRestaurantMap.this);

                        mAdapter.notifyDataSetChanged();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                progressDialog.dismiss();
                if (error instanceof ServerError)
                    Toast.makeText(Activity_FindRestaurantMap.this, "Server Error", Toast.LENGTH_SHORT).show();
                else if (error instanceof TimeoutError)
                    Toast.makeText(Activity_FindRestaurantMap.this, "Connection Timed Out", Toast.LENGTH_SHORT).show();
                else if (error instanceof NetworkError)
                    Toast.makeText(Activity_FindRestaurantMap.this, "Bad Network Connection", Toast.LENGTH_SHORT).show();
            }
        }){@Override
        public Map<String, String> getHeaders() throws AuthFailureError {
            Map<String, String> params = new HashMap<String, String>();
            params.put("Accept", "application/json");
            //params.put("Authorization","Bearer "+ session.getAPIToken());
            return params;
        }};
        loginRequest.setTag("TAG");loginRequest.setShouldCache(false);
        requestQueue.add(loginRequest);

    }

    private void GetType()  {

        final KProgressHUD progressDialog = KProgressHUD.create(Activity_FindRestaurantMap.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();

        GetTypeRequest loginRequest = new GetTypeRequest(new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("Response", response + " null");
                progressDialog.dismiss();



                try {

                    JSONObject jsonObject = new JSONObject(response);

                    JSONArray jsonObject1 = jsonObject.getJSONArray("data");

                    String[] City = new String[jsonObject1.length()];
                    final String[] CityId = new String[jsonObject1.length()];

                    for (int i = 0; i < jsonObject1.length(); i++) {
                        JSONObject object = jsonObject1.getJSONObject(i);
                        CityId[i] = object.getString("restaurant_type_id");
                        City[i] = object.getString("restaurant_type");
                    }


                    ArrayAdapter<String> adapter_age = new ArrayAdapter<String>(Activity_FindRestaurantMap.this,
                            R.layout.spiinerlayout, City);
                    mType.setAdapter(adapter_age);
                    mType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view,
                                                   int position, long id) {
                            mTypeId = CityId[position];
                            ((TextView) parent.getChildAt(0)).setTextColor(Color.WHITE);
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {
                            // TODO Auto-generated method stub
                        }
                    });

                }catch (Exception e){

                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                progressDialog.dismiss();
                if (error instanceof ServerError)
                    Toast.makeText(Activity_FindRestaurantMap.this, "Server Error", Toast.LENGTH_SHORT).show();
                else if (error instanceof TimeoutError)
                    Toast.makeText(Activity_FindRestaurantMap.this, "Connection Timed Out", Toast.LENGTH_SHORT).show();
                else if (error instanceof NetworkError)
                    Toast.makeText(Activity_FindRestaurantMap.this, "Bad Network Connection", Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                // params.put("Accept", "application/json");
                // params.put("Authorization","Bearer "+ Session.getAPIToken());
                return params;
            }};
        loginRequest.setTag("TAG");loginRequest.setShouldCache(false);
        requestQueue.add(loginRequest);

    }

    private void GetCity()  {

        final KProgressHUD progressDialog = KProgressHUD.create(Activity_FindRestaurantMap.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();

        GetCityRequest loginRequest = new GetCityRequest("1",new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("Response", response + " null");
                progressDialog.dismiss();



                try {

                    JSONObject jsonObject = new JSONObject(response);

                    JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                    JSONArray jsonObject12 = jsonObject1.getJSONArray("data");

                    String[] City = new String[jsonObject12.length()];
                    final String[] CityId = new String[jsonObject12.length()];

                    for (int i = 0; i < jsonObject12.length(); i++) {
                        JSONObject object = jsonObject12.getJSONObject(i);
                        CityId[i] = object.getString("city_id");
                        City[i] = object.getString("city");
                    }


                    ArrayAdapter<String> adapter_age = new ArrayAdapter<String>(Activity_FindRestaurantMap.this,
                            R.layout.spiinerlayout, City);
                    mCity.setAdapter(adapter_age);
                    mCity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view,
                                                   int position, long id) {
                            mCityId = CityId[position];
                            ((TextView) parent.getChildAt(0)).setTextColor(Color.WHITE);

                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {
                            // TODO Auto-generated method stub
                        }
                    });

                }catch (Exception e){

                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                progressDialog.dismiss();
                if (error instanceof ServerError)
                    Toast.makeText(Activity_FindRestaurantMap.this, "Server Error", Toast.LENGTH_SHORT).show();
                else if (error instanceof TimeoutError)
                    Toast.makeText(Activity_FindRestaurantMap.this, "Connection Timed Out", Toast.LENGTH_SHORT).show();
                else if (error instanceof NetworkError)
                    Toast.makeText(Activity_FindRestaurantMap.this, "Bad Network Connection", Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                // params.put("Accept", "application/json");
                // params.put("Authorization","Bearer "+ Session.getAPIToken());
                return params;
            }};
        loginRequest.setTag("TAG");
        loginRequest.setShouldCache(false);
        requestQueue.add(loginRequest);

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {


        try {
            boolean success = googleMap.setMapStyle(
                    MapStyleOptions.loadRawResourceStyle(this, R.raw.google_style));
            if (!success) {
                // Handle map style load failure
                Log.e("map_style","map style updated please do check it");
            }
        } catch (Resources.NotFoundException e) {
            // Oops, looks like the map style resource couldn't be found!
            Log.e("map_style","map is not updated yet ... do some other stuff");
        }

        mMap = googleMap;
        googleMap.setOnMarkerClickListener(this);
        googleMap.setMapType(R.raw.google_style);

        //setting the size of marker in map by using Bitmap Class
        int height = 80;
        int width = 80;
        BitmapDrawable bitmapdraw=(BitmapDrawable)getResources().getDrawable(R.drawable.map_holder);
        Bitmap b=bitmapdraw.getBitmap();
        final Bitmap smallMarker = Bitmap.createScaledBitmap(b, width, height, false);



        for(int i = 0 ; i < categoryModels.size() ; i++){

            LatLng India = new LatLng(categoryModels.get(i).getLat(), categoryModels.get(i).getaLong());
            mMap.addMarker(new MarkerOptions().position(India).title(categoryModels.get(i).getRestaurant_name())).setIcon(BitmapDescriptorFactory.fromBitmap(smallMarker));
            markerMap.put(marker, categoryModels.get(i));


        }


        mMap.setOnCameraMoveListener(new GoogleMap.OnCameraMoveListener() {
            @Override
            public void onCameraMove() {
                latitude= String.valueOf(mMap.getProjection().getVisibleRegion().latLngBounds.getCenter().latitude);
                longitude= String.valueOf(mMap.getProjection().getVisibleRegion().latLngBounds.getCenter().longitude);
                Log.e("LatLong",""+lat+"---"+lng);
               // GetSupportRastaurant(1,latitude,longitude);

            }
        });


        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {

                for (int i = 0 ; i < categoryModels.size() ; i++){
                    if(marker.getTitle().equals(categoryModels.get(i).getRestaurant_name())){
                        showPopupMap(categoryModels.get(i).getId(),categoryModels.get(i).getRestaurant_image(),categoryModels.get(i).getRestaurant_name(),categoryModels.get(i).getCity());
                    }
                }
                Toast.makeText(Activity_FindRestaurantMap.this,marker.getTitle(),Toast.LENGTH_SHORT).show();

                return false;
            }
        });

    }

    @Override
    public void onLocationChanged(Location location) {
        currentLatitude = location.getLatitude();               //getting current latitude
        currentLongitude = location.getLongitude();             //getting current longitude



    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        return false;
    }

    @Override
    protected void onStart() {
        super.onStart();
        googleApiClient.connect();              //establishment of google api client connection
    }

    @Override
    protected void onStop() {
        if (googleApiClient.isConnected()) {
            googleApiClient.disconnect();       //discarding the google api client service if the activity is in onStop condition
        }
        super.onStop();
    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {

        if (ActivityCompat.checkSelfPermission(this, ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestPermission();
        } else {

        }

    }
    private void requestPermission() {
        ActivityCompat.requestPermissions(Activity_FindRestaurantMap.this, new
                String[]{ACCESS_FINE_LOCATION}, RequestPermissionCode);
    }


    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }


    private void OnGPS() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Enable GPS").setCancelable(false).setPositiveButton("Yes", new  DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
            }
        }).setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }
    private void getLocation() {
        if (ActivityCompat.checkSelfPermission(
                Activity_FindRestaurantMap.this,ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                Activity_FindRestaurantMap.this, ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{ACCESS_FINE_LOCATION}, REQUEST_LOCATION);
        } else {
            Location locationGPS = nManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            if (locationGPS != null) {
                double lat = locationGPS.getLatitude();
                double longi = locationGPS.getLongitude();
               latitude = String.valueOf(lat);
               longitude = String.valueOf(longi);

                Log.e("latlong",latitude+"===="+longitude);
            } else {
                Toast.makeText(this, "Unable to find location.", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void showPopup() {
        // custom dialog
        final Dialog dialog = new Dialog(Activity_FindRestaurantMap.this);
        dialog.setContentView(R.layout.description_dialog);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        dialog.findViewById(R.id.done).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });





        dialog.show();
    }


}