package com.bites.funds1.Activity;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.bites.funds1.API.RastaurantLoginRequest;
import com.bites.funds1.API.UserLoginRequest;
import com.bites.funds1.R;
import com.bites.funds1.Utils.ServerUtils;
import com.bites.funds1.Utils.UserSession;
import com.bites.funds1.Utils.AppContants;
import com.bites.funds1.Utils.WalletHelper;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class Activity_Login extends AppCompatActivity {
    private Context ctx = null;

    private TextView submit,forgot,register,login;
    private EditText Password,Email;
    private UserSession Session;
    private RequestQueue requestQueue;
    private WalletHelper wallet = null;
    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);


        if (Build.VERSION.SDK_INT >= 21) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(getResources().getColor(R.color.colorPrimary));
        }
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setNavigationBarColor(getResources().getColor(R.color.colorPrimary));
        }
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        login = findViewById(R.id.login);
        submit = findViewById(R.id.submit);
        forgot = findViewById(R.id.forgot);
        register = findViewById(R.id.register);
        Email = findViewById(R.id.email);
        Password = findViewById(R.id.password);
        requestQueue = Volley.newRequestQueue(Activity_Login.this);//Creating the RequestQueue
        ctx = this;

        Session = new UserSession(Activity_Login.this);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if(Email.getText().toString().isEmpty()){
                    Toast.makeText(Activity_Login.this,"Please Enter Your Email",Toast.LENGTH_SHORT).show();
                }else if (!Email.getText().toString().trim().matches(emailPattern)) {
                    Toast.makeText(getApplicationContext(),"Invalid email address",Toast.LENGTH_SHORT).show();
                }else if(Password.getText().toString().isEmpty()){
                    Toast.makeText(Activity_Login.this,"Please Enter Your Password",Toast.LENGTH_SHORT).show();
                }else {

                    if(Session.getLoginType().equals(AppContants.TYPEUSER)){
                        UserLogin(Email.getText().toString(),Password.getText().toString());
                    }else {
                        RastaurantLogin(Email.getText().toString(),Password.getText().toString());

                    }
                }



            }
        });


        if(Session.getLoginType().equals(AppContants.TYPEUSER)){
            login.setText("User Login");
        }else {
            login.setText("Restaurant Login");


        }
        forgot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Activity_Login.this, Activity_ForgotPassword.class);
                startActivity(i);
            }
        });

        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(Session.getLoginType().equals(AppContants.TYPEUSER)){
                    Intent i = new Intent(Activity_Login.this, Activity_UserRegistrations.class);
                    startActivity(i);
                }else {
                    Intent i = new Intent(Activity_Login.this, Activity_ResturantRegistrations.class);
                    startActivity(i);
                }

            }
        });

        findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }


    private void UserLogin(String Email, String Password)  {

        final KProgressHUD progressDialog = KProgressHUD.create(Activity_Login.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();


     /*   AndroidNetworking.post( ServerUtils.BASE_URL+"user/login")
                .addBodyParameter("email",Email)
                .addBodyParameter("password",Password)
                .build()
                .getAsString(new StringRequestListener() {
                    @Override
                    public void onResponse(String response) {

                        Log.e("Response", response + " null");
                        progressDialog.dismiss();


                        JSONObject jsonObject = null;
                        try {
                            jsonObject = new JSONObject(response);
                            Toast.makeText(Activity_Login.this,jsonObject.getString("ResponseMsg"),Toast.LENGTH_SHORT).show();


                            Session.createLoginSession(jsonObject.getJSONObject("data").getString("user_id"),
                                    jsonObject.getJSONObject("data").getString("profile_pic"),
                                    jsonObject.getJSONObject("data").getString("first_name"),
                                    jsonObject.getJSONObject("data").getString("last_name"),
                                    jsonObject.getJSONObject("data").getString("email"),
                                    jsonObject.getJSONObject("data").getString("phone_no"),
                                    jsonObject.getJSONObject("data").getString("api_token")
                            );
                            generateWallet(jsonObject.getJSONObject("data").getString("user_id"));
                            Intent i = new Intent(Activity_Login.this, Activity_UserSide.class);
                            startActivity(i);
                            finish();


                        }catch (Exception e){

                        }
                    }

                    @Override
                    public void onError(ANError anError) {

                    }
                });*/


        UserLoginRequest loginRequest = new UserLoginRequest(Email,Password,new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("Response", response + " null");
                progressDialog.dismiss();


                JSONObject jsonObject = null;
                try {
                    jsonObject = new JSONObject(response);
                    Toast.makeText(Activity_Login.this,jsonObject.getString("ResponseMsg"),Toast.LENGTH_SHORT).show();


                    Session.createLoginSession(jsonObject.getJSONObject("data").getString("user_id"),
                            jsonObject.getJSONObject("data").getString("profile_pic"),
                            jsonObject.getJSONObject("data").getString("first_name"),
                            jsonObject.getJSONObject("data").getString("last_name"),
                            jsonObject.getJSONObject("data").getString("email"),
                            jsonObject.getJSONObject("data").getString("phone_no"),
                            jsonObject.getJSONObject("data").getString("api_token")
                    );
                    //generateWallet(jsonObject.getJSONObject("data").getString("user_id"));
                    Intent i = new Intent(Activity_Login.this, Activity_UserSide.class);
                    startActivity(i);
                    finish();


                }catch (Exception e){

                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                progressDialog.dismiss();
                if (error instanceof ServerError)
                    Toast.makeText(Activity_Login.this, "Server Error", Toast.LENGTH_SHORT).show();
                else if (error instanceof TimeoutError)
                    Toast.makeText(Activity_Login.this, "Connection Timed Out", Toast.LENGTH_SHORT).show();
                else if (error instanceof NetworkError)
                    Toast.makeText(Activity_Login.this, "Bad Network Connection", Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                 params.put("Accept", "application/json");
                params.put("Authorization","Bearer "+ Session.getAPIToken());
                return params;
            }};
        loginRequest.setTag("TAG");
        requestQueue.add(loginRequest);

    }

    private void RastaurantLogin(String Email, String Password)  {

        final KProgressHUD progressDialog = KProgressHUD.create(Activity_Login.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();

        RastaurantLoginRequest loginRequest = new RastaurantLoginRequest(Email,Password,new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("Response", response + " null");
                progressDialog.dismiss();


                JSONObject jsonObject = null;
                try {
                    jsonObject = new JSONObject(response);
                    Toast.makeText(Activity_Login.this,jsonObject.getString("ResponseMsg"),Toast.LENGTH_SHORT).show();

                    Session.createLoginSession(jsonObject.getJSONObject("data").getString("restaurant_id"),
                            jsonObject.getJSONObject("data").getString("restaurant_image"),
                            jsonObject.getJSONObject("data").getString("restaurant_name"),
                            jsonObject.getJSONObject("data").getString("restaurant_name"),
                            jsonObject.getJSONObject("data").getString("email"),
                            "",
                            jsonObject.getJSONObject("data").getString("api_token")
                    );

                   // generateWallet(jsonObject.getJSONObject("data").getString("restaurant_id"));

                    Intent i = new Intent(Activity_Login.this, Activity_RestaurantSide.class);
                    startActivity(i);
                    finish();

                }catch (Exception ignored){

                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                progressDialog.dismiss();
                if (error instanceof ServerError)
                    Toast.makeText(Activity_Login.this, "Server Error", Toast.LENGTH_SHORT).show();
                else if (error instanceof TimeoutError)
                    Toast.makeText(Activity_Login.this, "Connection Timed Out", Toast.LENGTH_SHORT).show();
                else if (error instanceof NetworkError)
                    Toast.makeText(Activity_Login.this, "Bad Network Connection", Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                 params.put("Accept", "application/json");
                 params.put("Authorization","Bearer "+ Session.getAPIToken());
                return params;
            }};
        loginRequest.setTag("TAG");loginRequest.setShouldCache(false);
        requestQueue.add(loginRequest);

    }

    private void generateWallet(String Id) {
        wallet = new WalletHelper(Id, this.ctx);

       // Toast.makeText(Activity_Login.this,wallet.getAddress(),Toast.LENGTH_SHORT).show();

    }


}