package com.bites.funds1.Activity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import androidx.appcompat.app.AppCompatActivity;

import com.bites.funds1.R;
import com.bites.funds1.Utils.UserSession;
import com.bites.funds1.Utils.AppContants;

public class Activity_Intro_T extends AppCompatActivity {


    private UserSession Session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.intro1_screen3);

        Session = new UserSession(getApplicationContext());
        if (Build.VERSION.SDK_INT >= 21) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(getResources().getColor(R.color.colorPrimary));
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setNavigationBarColor(getResources().getColor(R.color.colorPrimary));
        }

        findViewById(R.id.m_us).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Session.LoginType(AppContants.TYPEUSER);
                Intent intent = new Intent(Activity_Intro_T.this, Activity_Login.class);
                startActivity(intent);
                finish();
            }
        });

        findViewById(R.id.m_rs).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Session.LoginType(AppContants.TYPERASTAURANT);
                Intent intent = new Intent(Activity_Intro_T.this, Activity_Login.class);
                startActivity(intent);
                finish();
            }
        });

    }


}