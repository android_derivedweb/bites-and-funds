package com.bites.funds1.Activity;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.bites.funds1.API.BuyBiteRequest;
import com.bites.funds1.API.CreateVoucherRequest;
import com.bites.funds1.API.GetRastaurantDetailsRequest;
import com.bites.funds1.R;
import com.bites.funds1.Utils.ServerUtils;
import com.bites.funds1.Utils.UserSession;
import com.bites.funds1.Utils.AppContants;
import com.bumptech.glide.Glide;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.stripe.android.Stripe;
import com.stripe.android.TokenCallback;
import com.stripe.android.model.Card;
import com.stripe.android.model.Token;
import com.stripe.android.view.CardMultilineWidget;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class Activity_FindRestaurant extends AppCompatActivity {

    String[] country = {"*--Please select one--*", "50", "100", "200", "Custom Amount"};
    String[] country1 = {"*--Please select one--*", "62.5 (Pay 50 USD)", "100 (Pay 80 USD)", "200 (Pay 160 USD)", "Custom Amount"};
    String[] country2 = {"*--Please select one--*", "50", "100", "200", "Custom Amount"};
    private UserSession Session;
    private RequestQueue requestQueue;
    private ImageView Image;
    private TextView Title,Type,Description,Location;
    private String Id;
    private String Amount = "";
    private int TypeId = 1;

    int REQUEST_CODE = 0077;

    public String stripe_token = "";

    private RelativeLayout payment_layout;
    private ScrollView scrollView;
    CardMultilineWidget cardMultilineWidget;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_find_restaurant);


        Session = new UserSession(getApplicationContext());

        if (Build.VERSION.SDK_INT >= 21) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(getResources().getColor(R.color.colorPrimary));
        }

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setNavigationBarColor(getResources().getColor(R.color.colorPrimary));
        }

        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        requestQueue = Volley.newRequestQueue(Activity_FindRestaurant.this);//Creating the RequestQueue

        final Spinner spin = (Spinner) findViewById(R.id.amount);
        final TextView txt1 = (TextView) findViewById(R.id.txt1);
        final TextView txt2 = (TextView) findViewById(R.id.txt2);
        final TextView text1 = (TextView) findViewById(R.id.text1);
        final TextView text2 = (TextView) findViewById(R.id.text2);
        final EditText edt_custom = (EditText) findViewById(R.id.edt_custom);
        final TextView cmt_txt = (TextView) findViewById(R.id.cmt_txt);

        Image = findViewById(R.id.image);
        Title = findViewById(R.id.title);
        Type = findViewById(R.id.type);
        Location = findViewById(R.id.location);
        Description = findViewById(R.id.description);



        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();

        if(bundle != null){
            Id = bundle.getString("Id");
        }


        payment_layout = findViewById(R.id.payment_layout);
        scrollView = findViewById(R.id.scrollView);

        cardMultilineWidget = findViewById(R.id.card_input_widget);



        txt1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i = new Intent(Activity_FindRestaurant.this, Activity_FindRestaurantSecond.class);
                i.putExtra("Value",1);
                i.putExtra("Id",Id);
                startActivity(i);
              /*
                TypeId = 1;
                ArrayAdapter aa = new ArrayAdapter(FindRestaurant.this, android.R.layout.simple_spinner_item, country1);
                aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                //Setting the ArrayAdapter data on the Spinner
                spin.setAdapter(aa);

                spin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                        if (i == 4) {
                            edt_custom.setVisibility(View.VISIBLE);
                            cmt_txt.setVisibility(View.VISIBLE);

                        } else {
                            edt_custom.setVisibility(View.GONE);
                            cmt_txt.setVisibility(View.GONE);
                        }
                        Amount = country[i];

                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                });
                txt1.setTextColor(getResources().getColor(R.color.white));
                txt1.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                txt2.setTextColor(getResources().getColor(R.color.black));
                txt2.setBackgroundColor(getResources().getColor(R.color.white));
                text1.setText("Choose the number of BITE to buy");
                text2.setText("Note : You pay 80 cents per BITE. Each BITE is worth USD 1 when redeemed in a restaurant.");
            */}
        });
        txt2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i = new Intent(Activity_FindRestaurant.this, Activity_FindRestaurantSecond.class);
                i.putExtra("Value",2);
                i.putExtra("Id",Id);
                startActivity(i);
               /* TypeId = 2;
                ArrayAdapter aa = new ArrayAdapter(FindRestaurant.this, android.R.layout.simple_spinner_item, country);
                aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                //Setting the ArrayAdapter data on the Spinner
                spin.setAdapter(aa);

                spin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                        if (i == 4) {
                            edt_custom.setVisibility(View.VISIBLE);
                            cmt_txt.setVisibility(View.VISIBLE);

                        } else {
                            edt_custom.setVisibility(View.GONE);
                            cmt_txt.setVisibility(View.GONE);
                        }
                        Amount = country2[i];

                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                });
                txt2.setTextColor(getResources().getColor(R.color.white));
                txt2.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                txt1.setTextColor(getResources().getColor(R.color.black));
                txt1.setBackgroundColor(getResources().getColor(R.color.white));
                text1.setText("Number of BITE to load on voucher\n" +
                        "\n" +
                        "       Number of BITE available in Wallet : 5,360");
                text2.setText("Note : 1 BITE = 1USD\n" +
                        "\n" +
                        "--> Pay up to 50% of the check in BITE\n" +
                        "--> mojito shot");
       */     }
        });


//Creating the ArrayAdapter instance having the country list
        ArrayAdapter aa = new ArrayAdapter(this, android.R.layout.simple_spinner_item, country1);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //Setting the ArrayAdapter data on the Spinner
        spin.setAdapter(aa);

        spin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i == 4) {
                    edt_custom.setVisibility(View.VISIBLE);
                    cmt_txt.setVisibility(View.VISIBLE);

                } else {
                    edt_custom.setVisibility(View.GONE);
                    cmt_txt.setVisibility(View.GONE);
                }
                Amount = country[i];

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        findViewById(R.id.profile).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showPopup2();
            }
        });
        findViewById(R.id.profile).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showPopup2();
            }
        });




        GetRastaurantDetails(Id);

        findViewById(R.id.create).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if(!Amount.equals("*--Please select one--*")){
                    if(Amount.equals("Custom Amount")){
                        Amount = edt_custom.getText().toString();
                    }

                    if(Session.getLoginType().equals(AppContants.TYPEUSER)){
                        if(TypeId==1){
                            //  Intent intent = new Intent(getApplicationContext(), Activity_AddPayment.class);
                            //  startActivityForResult(intent,REQUEST_CODE);
                            //Buy(Id,Amount);
                            payment_layout.setVisibility(View.VISIBLE);
                            scrollView.setBackgroundColor(getResources().getColor(R.color.gray));

                        }else {
                            PostCreate(Id,Amount);
                        }

                    }
                }


            }
        });

        findViewById(R.id.done).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                payment_layout.setVisibility(View.GONE);
                scrollView.setBackgroundColor(getResources().getColor(R.color.white));
                saveCard();
            }
        });

    }

    private void saveCard() {

        Card card =  cardMultilineWidget.getCard();
        if(card == null){
            Toast.makeText(getApplicationContext(),"Invalid card",Toast.LENGTH_SHORT).show();
        }else {
            if (!card.validateCard()) {
                // Do not continue token creation.
                Toast.makeText(getApplicationContext(), "Invalid card", Toast.LENGTH_SHORT).show();
            } else {
                CreateToken(card);
            }
        }
    }

    private void CreateToken( Card card) {
        Stripe stripe = new Stripe(getApplicationContext(), getString(R.string.publishablekey));
        stripe.createToken(
                card,
                new TokenCallback() {
                    public void onSuccess(Token token) {

                        // Send token to your server
                       /* Log.e("Stripe Token", token.getId());
                        Intent intent = new Intent();
                        intent.putExtra("card",token.getCard().getLast4());
                        intent.putExtra("stripe_token",token.getId());
                        intent.putExtra("cardtype",token.getCard().getBrand());
                        setResult(0077,intent);
                        finish();*/

                        stripe_token = token.getId();

                        PostBuy(Id,Amount,stripe_token);
                    }
                    public void onError(Exception error) {

                        // Show localized error message
                        Toast.makeText(getApplicationContext(),
                                error.getLocalizedMessage(),
                                Toast.LENGTH_LONG
                        ).show();
                    }
                }
        );
    }



    @Override
    public void onBackPressed() {
        if(payment_layout.getVisibility()==View.VISIBLE){
            payment_layout.setVisibility(View.GONE);
            scrollView.setBackgroundColor(getResources().getColor(R.color.white));

        }else {
            finish();
        }
    }




    private void showPopup2() {
        // custom dialog
        final Dialog dialog = new Dialog(Activity_FindRestaurant.this);
        dialog.setContentView(R.layout.login_dialog);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        dialog.findViewById(R.id.close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        dialog.findViewById(R.id.restaurant).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();

                Session.logout();
                Intent i = new Intent(Activity_FindRestaurant.this, Activity_Intro_F.class);
                startActivity(i);
                finish();
            }
        });

        dialog.findViewById(R.id.user).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                Session.logout();
                Intent i = new Intent(Activity_FindRestaurant.this, Activity_Intro_F.class);
                startActivity(i);
                finish();
            }
        });
        dialog.show();
    }


    public void GetRastaurantDetails(String Id) {

        final KProgressHUD progressDialog = KProgressHUD.create(Activity_FindRestaurant.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();

        GetRastaurantDetailsRequest loginRequest = new GetRastaurantDetailsRequest(Id,new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("Response", response + " null");
                progressDialog.dismiss();

                JSONObject jsonObject = null;
                try {
                    jsonObject = new JSONObject(response);
                    // Toast.makeText(AllCategories.this,jsonObject.getString("ResponseMsg"),Toast.LENGTH_SHORT).show();
                    if (jsonObject.getString("ResponseCode").equals("200")) {

                        JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                        Title.setText(jsonObject1.getString("restaurant_name").toUpperCase());
                        Type.setText(jsonObject1.getString("restaurant_type"));
                        Description.setText(jsonObject1.getString("description"));
                        Location.setText(jsonObject1.getString("city"));



                        Glide.with(Activity_FindRestaurant.this).load(jsonObject1.getString("restaurant_image")).placeholder(R.drawable.placeholder).into(Image);

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                progressDialog.dismiss();
                if (error instanceof ServerError)
                    Toast.makeText(Activity_FindRestaurant.this, "Server Error", Toast.LENGTH_SHORT).show();
                else if (error instanceof TimeoutError)
                    Toast.makeText(Activity_FindRestaurant.this, "Connection Timed Out", Toast.LENGTH_SHORT).show();
                else if (error instanceof NetworkError)
                    Toast.makeText(Activity_FindRestaurant.this, "Bad Network Connection", Toast.LENGTH_SHORT).show();
            }
        }){@Override
        public Map<String, String> getHeaders() throws AuthFailureError {
            Map<String, String> params = new HashMap<String, String>();
            params.put("Accept", "application/json");
            params.put("Authorization","Bearer "+ Session.getAPIToken());
            return params;
        }};
        loginRequest.setTag("TAG");loginRequest.setShouldCache(false);
        requestQueue.add(loginRequest);

    }

    public void Create(String Id,String Amount) {

        Log.e("SessionResponse", Session.getAPIToken() + " null");

        final KProgressHUD progressDialog = KProgressHUD.create(Activity_FindRestaurant.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();

       /* AndroidNetworking.post(ServerUtils.BASE_URL+"user/create-voucher")
                .addBodyParameter("restaurant_id",Id)
                .addBodyParameter("voucher_amount",Amount)
                .addHeaders("Accept","application/json")
                .addHeaders("Authorization","Bearer "+Session.getAPIToken())
                .build()
                .getAsString(new StringRequestListener() {
                    @Override
                    public void onResponse(String response) {
                        progressDialog.dismiss();
                        Log.e("Response", response + " voucher");
                        JSONObject jsonObject = null;
                        try {
                            jsonObject = new JSONObject(response);
                            if (jsonObject.getString("ResponseCode").equals("200")) {

                                Toast.makeText(Activity_FindRestaurant.this,jsonObject.getString("ResponseMsg"),Toast.LENGTH_SHORT).show();

                            }

                            Toast.makeText(Activity_FindRestaurant.this,jsonObject.getString("ResponseMsg"),Toast.LENGTH_SHORT).show();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        progressDialog.dismiss();
                        Log.e("Response", anError.getErrorCode() + " voucher");
                    }
                });*/

        CreateVoucherRequest loginRequest = new CreateVoucherRequest(Id,Amount,new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                progressDialog.dismiss();
                Log.e("Response", response + " voucher");
                JSONObject jsonObject = null;
                try {
                    jsonObject = new JSONObject(response);
                    if (jsonObject.getString("ResponseCode").equals("200")) {

                        Toast.makeText(Activity_FindRestaurant.this,jsonObject.getString("ResponseMsg"),Toast.LENGTH_SHORT).show();

                    }

                    Toast.makeText(Activity_FindRestaurant.this,jsonObject.getString("ResponseMsg"),Toast.LENGTH_SHORT).show();

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                progressDialog.dismiss();
                if (error instanceof ServerError)
                    Toast.makeText(Activity_FindRestaurant.this, "Server Error", Toast.LENGTH_SHORT).show();
                else if (error instanceof TimeoutError)
                    Toast.makeText(Activity_FindRestaurant.this, "Connection Timed Out", Toast.LENGTH_SHORT).show();
                else if (error instanceof NetworkError)
                    Toast.makeText(Activity_FindRestaurant.this, "Bad Network Connection", Toast.LENGTH_SHORT).show();
            }
        }){@Override
        public Map<String, String> getHeaders() throws AuthFailureError {
            Map<String, String> params = new HashMap<String, String>();
            params.put("Accept", "application/json");
            params.put("Authorization","Bearer "+ Session.getAPIToken());
            return params;
        }};
        loginRequest.setTag("TAG");
        requestQueue.add(loginRequest);

    }


    private void PostCreate(String Id,String Amount) {

        final KProgressHUD progressDialog = KProgressHUD.create(Activity_FindRestaurant.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();

        final HashMap<String, String> postParams = new HashMap<String, String>();
        postParams.put("restaurant_id", Id);
        postParams.put("voucher_amount", Amount);


        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                ServerUtils.BASE_URL+"user/create-voucher", new JSONObject(postParams),
                new com.android.volley.Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        progressDialog.dismiss();
                        Log.e("Response", response + " voucher");
                        try {

                            if (response.getString("ResponseCode").equals("200")) {

                                Toast.makeText(Activity_FindRestaurant.this, response.getString("ResponseMsg"),Toast.LENGTH_SHORT).show();

                            }

                            Toast.makeText(Activity_FindRestaurant.this, response.getString("ResponseMsg"),Toast.LENGTH_SHORT).show();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new com.android.volley.Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("Error" ,""+ error.getLocalizedMessage());
                Toast.makeText(Activity_FindRestaurant.this, ""+error.getLocalizedMessage(), Toast.LENGTH_SHORT).show();

                Session.logout();
                Intent i = new Intent(getApplicationContext(), Activity_Intro_F.class);
                finish();
                startActivity(i);
                progressDialog.dismiss();

            }
        }) {

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                params.put("Authorization","Bearer "+ Session.getAPIToken());
                return params;
            }
        };

        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(30000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        // Adding request to request queue
        requestQueue.add(jsonObjReq);
    }


    public void Buy(String Id, String Amount,String stripe_token) {

        Log.e("SessionResponse",stripe_token+ " ---- " +  Session.getAPIToken() + " --- " + Amount + "--- " + Id);



        final KProgressHUD progressDialog = KProgressHUD.create(Activity_FindRestaurant.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();


/*
        AndroidNetworking.post(ServerUtils.BASE_URL+"user/buy-bite")
                .addBodyParameter("restaurant_id",Id)
                .addBodyParameter("stripe_token",stripe_token)
                .addBodyParameter("bite_payment_amount",Amount)
                .addHeaders("Accept","application/json")
                .addHeaders("Authorization","Bearer "+Session.getAPIToken())
                .build()
                .getAsString(new StringRequestListener() {
                    @Override
                    public void onResponse(String response) {
                        progressDialog.dismiss();
                        Log.e("Response", response + " bite");
                        JSONObject jsonObject = null;
                        try {
                            jsonObject = new JSONObject(response);
                            if (jsonObject.getString("ResponseCode").equals("200")) {

                                Toast.makeText(Activity_FindRestaurant.this,jsonObject.getString("ResponseMsg"),Toast.LENGTH_SHORT).show();

                            }

                            Toast.makeText(Activity_FindRestaurant.this,jsonObject.getString("ResponseMsg"),Toast.LENGTH_SHORT).show();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        progressDialog.dismiss();
                        Log.e("Response", anError.getErrorCode() + " bite");
                    }
                });*/
        BuyBiteRequest loginRequest = new BuyBiteRequest(Id,Amount,stripe_token,new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                progressDialog.dismiss();
                Log.e("Response", response + " bite");
                JSONObject jsonObject = null;
                try {
                    jsonObject = new JSONObject(response);
                    if (jsonObject.getString("ResponseCode").equals("200")) {

                        Toast.makeText(Activity_FindRestaurant.this,jsonObject.getString("ResponseMsg"),Toast.LENGTH_SHORT).show();

                    }

                    Toast.makeText(Activity_FindRestaurant.this,jsonObject.getString("ResponseMsg"),Toast.LENGTH_SHORT).show();

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                progressDialog.dismiss();
                if (error instanceof ServerError)
                    Toast.makeText(Activity_FindRestaurant.this, "Server Error", Toast.LENGTH_SHORT).show();
                else if (error instanceof TimeoutError)
                    Toast.makeText(Activity_FindRestaurant.this, "Connection Timed Out", Toast.LENGTH_SHORT).show();
                else if (error instanceof NetworkError)
                    Toast.makeText(Activity_FindRestaurant.this, "Bad Network Connection", Toast.LENGTH_SHORT).show();
            }
        }){@Override
        public Map<String, String> getHeaders() throws AuthFailureError {
            Map<String, String> params = new HashMap<String, String>();
            params.put("Accept", "application/json");
            params.put("Authorization","Bearer "+Session.getAPIToken());
            return params;
        }};
        loginRequest.setTag("TAG");
        requestQueue.add(loginRequest);

    }


    private void PostBuy(String Id, String Amount,String stripe_token) {

        final KProgressHUD progressDialog = KProgressHUD.create(Activity_FindRestaurant.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();

        final HashMap<String, String> postParams = new HashMap<String, String>();
        postParams.put("restaurant_id", Id);
        postParams.put("stripe_token", stripe_token);
        postParams.put("bite_payment_amount", Amount);

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                ServerUtils.BASE_URL+"user/buy-bite", new JSONObject(postParams),
                new com.android.volley.Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("ResponseNet", response + " null");

                        progressDialog.dismiss();
                        Log.e("Response", response + " bite");
                        try {
                            if (response.getString("ResponseCode").equals("200")) {


                                Intent i = new Intent(Activity_FindRestaurant.this, Activity_successPayment.class);
                                i.putExtra("transaction_id", response.getJSONObject("data").getString("transaction_id"));
                                startActivity(i);
                                finish();
                             //   Toast.makeText(Activity_FindRestaurant.this, response.getString("ResponseMsg"),Toast.LENGTH_SHORT).show();

                            }

                            Toast.makeText(Activity_FindRestaurant.this, response.getString("ResponseMsg"),Toast.LENGTH_SHORT).show();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new com.android.volley.Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("Error" ,""+ error.getLocalizedMessage());
                Toast.makeText(Activity_FindRestaurant.this, ""+error.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                Session.logout();
                Intent i = new Intent(getApplicationContext(), Activity_Intro_F.class);
                finish();
                startActivity(i);
                progressDialog.dismiss();

            }
        }) {

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                params.put("Authorization","Bearer "+ Session.getAPIToken());
                return params;
            }
        };

        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(30000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        // Adding request to request queue
        requestQueue.add(jsonObjReq);
    }




    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == resultCode){


            stripe_token = data.getStringExtra("stripe_token");

            PostBuy(Id,Amount,stripe_token);


        }

    }




}