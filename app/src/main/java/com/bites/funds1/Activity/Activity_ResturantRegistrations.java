package com.bites.funds1.Activity;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.bites.funds1.API.GetCityRequest;
import com.bites.funds1.API.GetStatesRequest;
import com.bites.funds1.API.GetTypeRequest;
import com.bites.funds1.API.RastaurantRegistrationRequest;
import com.bites.funds1.R;
import com.bites.funds1.Utils.ServerUtils;
import com.bites.funds1.Utils.UserSession;
import com.bites.funds1.Utils.WalletHelper;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class Activity_ResturantRegistrations extends AppCompatActivity {

    private TextView register;
    private String mCityId,mTypeId;
    private Spinner mType,mCity,mState;
    private RequestQueue requestQueue;
    private EditText mName,mEmail,mPassword,mPassword2;
    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    private UserSession Session;
    private WalletHelper wallet;
    private String Crypto_token;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_rastaurant);


        if (Build.VERSION.SDK_INT >= 21) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(getResources().getColor(R.color.colorPrimary));
        }

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setNavigationBarColor(getResources().getColor(R.color.colorPrimary));
        }
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        requestQueue = Volley.newRequestQueue(Activity_ResturantRegistrations.this);//Creating the RequestQueue
        Session = new UserSession(Activity_ResturantRegistrations.this);



        mType = (Spinner) findViewById(R.id.type);
        mCity = (Spinner) findViewById(R.id.city);
        mState = (Spinner) findViewById(R.id.state);
        mName = (EditText) findViewById(R.id.r_name);
        mEmail = (EditText) findViewById(R.id.r_email);
        mPassword = (EditText) findViewById(R.id.password);
        mPassword2 = (EditText) findViewById(R.id.password2);

        Long tsLong = System.currentTimeMillis()/1000;
        String ts = tsLong.toString();

        wallet = new WalletHelper(ts, this);

        Log.e("Crypto_token", wallet.getAddress());
        register = findViewById(R.id.submit1);
        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (mName.getText().toString().isEmpty()) {
                    Toast.makeText(Activity_ResturantRegistrations.this, "Please Enter Your Rastaurant Name", Toast.LENGTH_SHORT).show();
                }else if (mEmail.getText().toString().isEmpty()) {
                    Toast.makeText(Activity_ResturantRegistrations.this, "Please Enter Your Email", Toast.LENGTH_SHORT).show();
                } else if (!mEmail.getText().toString().trim().matches(emailPattern)) {
                    Toast.makeText(Activity_ResturantRegistrations.this, "Invalid email address", Toast.LENGTH_SHORT).show();
                } else if (mPassword.getText().toString().isEmpty()) {
                    Toast.makeText(Activity_ResturantRegistrations.this, "Please Enter Your Password", Toast.LENGTH_SHORT).show();
                }else if (!mPassword.getText().toString().equals(mPassword2.getText().toString())) {
                    Toast.makeText(Activity_ResturantRegistrations.this, "Your Password does not match", Toast.LENGTH_SHORT).show();
                } else {

                  /*  RastaurantRegistration(mName.getText().toString(),
                            mCityId,
                            mEmail.getText().toString(),
                            mPassword.getText().toString(),
                            mTypeId);
*/
                    PostRegs(mName.getText().toString(),
                            mCityId,
                            mEmail.getText().toString(),
                            mPassword.getText().toString(),
                            mTypeId,wallet.getAddress());
                }

            }
        });

        findViewById(R.id.register).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        GetType();
        GetStates();

    }




    private void GetType()  {

        final KProgressHUD progressDialog = KProgressHUD.create(Activity_ResturantRegistrations.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();

        GetTypeRequest loginRequest = new GetTypeRequest(new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("Response", response + " null");
                progressDialog.dismiss();



                try {

                    JSONObject jsonObject = new JSONObject(response);

                    JSONArray jsonObject1 = jsonObject.getJSONArray("data");

                    String[] City = new String[jsonObject1.length()];
                    final String[] CityId = new String[jsonObject1.length()];

                    for (int i = 0; i < jsonObject1.length(); i++) {
                        JSONObject object = jsonObject1.getJSONObject(i);
                        CityId[i] = object.getString("restaurant_type_id");
                        City[i] = object.getString("restaurant_type");
                    }


                    ArrayAdapter<String> adapter_age = new ArrayAdapter<String>(Activity_ResturantRegistrations.this,
                            R.layout.spiinerlayout, City);
                    mType.setAdapter(adapter_age);
                    mType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view,
                                                   int position, long id) {
                            mTypeId = CityId[position];
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {
                            // TODO Auto-generated method stub
                        }
                    });

                }catch (Exception e){

                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                progressDialog.dismiss();
                if (error instanceof ServerError)
                    Toast.makeText(Activity_ResturantRegistrations.this, "Server Error", Toast.LENGTH_SHORT).show();
                else if (error instanceof TimeoutError)
                    Toast.makeText(Activity_ResturantRegistrations.this, "Connection Timed Out", Toast.LENGTH_SHORT).show();
                else if (error instanceof NetworkError)
                    Toast.makeText(Activity_ResturantRegistrations.this, "Bad Network Connection", Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                // params.put("Accept", "application/json");
                // params.put("Authorization","Bearer "+ Session.getAPIToken());
                return params;
            }};
        loginRequest.setTag("TAG");loginRequest.setShouldCache(false);
        requestQueue.add(loginRequest);

    }

    private void GetCity(String id)  {

        final KProgressHUD progressDialog = KProgressHUD.create(Activity_ResturantRegistrations.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();


        GetCityRequest loginRequest = new GetCityRequest(id,new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("Response", response + " null");
                progressDialog.dismiss();



                try {

                    JSONObject jsonObject = new JSONObject(response);

                    JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                    JSONArray jsonObject12 = jsonObject1.getJSONArray("data");

                    String[] City = new String[jsonObject12.length()];
                    final String[] CityId = new String[jsonObject12.length()];

                    for (int i = 0; i < jsonObject12.length(); i++) {
                        JSONObject object = jsonObject12.getJSONObject(i);
                        CityId[i] = object.getString("city_id");
                        City[i] = object.getString("city");
                    }


                    ArrayAdapter<String> adapter_age = new ArrayAdapter<String>(Activity_ResturantRegistrations.this,
                            R.layout.spiinerlayout, City);
                    mCity.setAdapter(adapter_age);
                    mCity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view,
                                                   int position, long id) {
                            mCityId = CityId[position];
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {
                            // TODO Auto-generated method stub
                        }
                    });

                }catch (Exception e){

                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                progressDialog.dismiss();
                if (error instanceof ServerError)
                    Toast.makeText(Activity_ResturantRegistrations.this, "Server Error", Toast.LENGTH_SHORT).show();
                else if (error instanceof TimeoutError)
                    Toast.makeText(Activity_ResturantRegistrations.this, "Connection Timed Out", Toast.LENGTH_SHORT).show();
                else if (error instanceof NetworkError)
                    Toast.makeText(Activity_ResturantRegistrations.this, "Bad Network Connection", Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                // params.put("Accept", "application/json");
                // params.put("Authorization","Bearer "+ Session.getAPIToken());
                return params;
            }};
        loginRequest.setTag("TAG");
        loginRequest.setShouldCache(false);
        requestQueue.add(loginRequest);

    }


    private void GetStates()  {

        final KProgressHUD progressDialog = KProgressHUD.create(Activity_ResturantRegistrations.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();


        GetStatesRequest loginRequest = new GetStatesRequest(new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("Response", response + " null");
                progressDialog.dismiss();



                try {

                    JSONObject jsonObject = new JSONObject(response);

                    JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                    JSONArray jsonObject12 = jsonObject1.getJSONArray("data");

                    String[] City = new String[jsonObject12.length()];
                    final String[] CityId = new String[jsonObject12.length()];

                    for (int i = 0; i < jsonObject12.length(); i++) {
                        JSONObject object = jsonObject12.getJSONObject(i);
                        CityId[i] = object.getString("state_id");
                        City[i] = object.getString("state_name");
                    }


                    ArrayAdapter<String> adapter_age = new ArrayAdapter<String>(Activity_ResturantRegistrations.this,
                            R.layout.spiinerlayout, City);
                    mState.setAdapter(adapter_age);
                    mState.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view,
                                                   int position, long id) {

                            GetCity(CityId[position]);
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {
                            // TODO Auto-generated method stub
                        }
                    });

                }catch (Exception e){

                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                progressDialog.dismiss();
                if (error instanceof ServerError)
                    Toast.makeText(Activity_ResturantRegistrations.this, "Server Error", Toast.LENGTH_SHORT).show();
                else if (error instanceof TimeoutError)
                    Toast.makeText(Activity_ResturantRegistrations.this, "Connection Timed Out", Toast.LENGTH_SHORT).show();
                else if (error instanceof NetworkError)
                    Toast.makeText(Activity_ResturantRegistrations.this, "Bad Network Connection", Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                // params.put("Accept", "application/json");
                // params.put("Authorization","Bearer "+ Session.getAPIToken());
                return params;
            }};
        loginRequest.setTag("TAG");
        loginRequest.setShouldCache(false);
        requestQueue.add(loginRequest);

    }




    private void RastaurantRegistration(String Name, String mCityId,String Email,String Password,String mTypeId)  {

        Log.e("data",Email);
        final KProgressHUD progressDialog = KProgressHUD.create(Activity_ResturantRegistrations.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();

     /*   AndroidNetworking.post(ServerUtils.BASE_URL+"restaurant/register")
                .addBodyParameter("restaurant_name",Name)
                .addBodyParameter("city_id",mCityId)
                .addBodyParameter("email",Email)
                .addBodyParameter("password",Password)
                .addBodyParameter("restaurant_type",mTypeId)
                .build()
                .getAsString(new StringRequestListener() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("ResponseNet", response + " null");

                        progressDialog.dismiss();



                        JSONObject jsonObject = null;
                        try {


                            jsonObject = new JSONObject(response);

                            if(jsonObject.getInt("ResponseCode")==422){
                                Toast.makeText(Activity_ResturantRegistrations.this,jsonObject.getJSONObject("errors").getJSONArray("email").getString(0),Toast.LENGTH_SHORT).show();

                            }else {


                                //Session.createLoginSession(jsonObject.getJSONObject("data").getString("restaurant_id"),
                       *//*         jsonObject.getJSONObject("data").getString("restaurant_image"),
                                jsonObject.getJSONObject("data").getString("restaurant_name"),
                                jsonObject.getJSONObject("data").getString("restaurant_name"),
                                jsonObject.getJSONObject("data").getString("email"),
                                "",
                                jsonObject.getJSONObject("data").getString("api_token")
                        );

                        Intent i = new Intent(RastaurantRegisterActivity.this, UserSideActivity.class);
                        startActivity(i);
                        finish();*//*

                                showPopup2();
                                Toast.makeText(Activity_ResturantRegistrations.this, jsonObject.getString("ResponseMsg"), Toast.LENGTH_SHORT).show();
                            }

                        }catch (Exception e){
                            Toast.makeText(Activity_ResturantRegistrations.this, e.getMessage(), Toast.LENGTH_SHORT).show();

                        }

                    }

                    @Override
                    public void onError(ANError anError) {

                    }
                });
*/

        RastaurantRegistrationRequest loginRequest = new RastaurantRegistrationRequest(Name,mCityId,Email,Password,mTypeId,Crypto_token,new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("ResponseNet", response + " null");

                progressDialog.dismiss();



                JSONObject jsonObject = null;
                try {


                    jsonObject = new JSONObject(response);

                    if(jsonObject.getInt("ResponseCode")==422){
                        Toast.makeText(Activity_ResturantRegistrations.this,jsonObject.getJSONObject("errors").getJSONArray("email").getString(0),Toast.LENGTH_SHORT).show();

                    }else {


                        //Session.createLoginSession(jsonObject.getJSONObject("data").getString("restaurant_id"),
                       /*         jsonObject.getJSONObject("data").getString("restaurant_image"),
                                jsonObject.getJSONObject("data").getString("restaurant_name"),
                                jsonObject.getJSONObject("data").getString("restaurant_name"),
                                jsonObject.getJSONObject("data").getString("email"),
                                "",
                                jsonObject.getJSONObject("data").getString("api_token")
                        );

                        Intent i = new Intent(RastaurantRegisterActivity.this, UserSideActivity.class);
                        startActivity(i);
                        finish();*/

                        showPopup2();
                        Toast.makeText(Activity_ResturantRegistrations.this, jsonObject.getString("ResponseMsg"), Toast.LENGTH_SHORT).show();
                    }

                }catch (Exception e){
                    Toast.makeText(Activity_ResturantRegistrations.this, e.getMessage(), Toast.LENGTH_SHORT).show();

                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                progressDialog.dismiss();
                if (error instanceof ServerError)
                    Toast.makeText(Activity_ResturantRegistrations.this, "Server Error", Toast.LENGTH_SHORT).show();
                else if (error instanceof TimeoutError)
                    Toast.makeText(Activity_ResturantRegistrations.this, "Connection Timed Out", Toast.LENGTH_SHORT).show();
                else if (error instanceof NetworkError)
                    Toast.makeText(Activity_ResturantRegistrations.this, "Bad Network Connection", Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                 params.put("Accept", "application/json");
                 params.put("Authorization","Bearer "+ Session.getAPIToken());
                return params;
            }};
        loginRequest.setTag("TAG");
        loginRequest.setShouldCache(false);
        requestQueue.add(loginRequest);

    }


    private void PostRegs(String Name, String mCityId,String Email,String Password,String mTypeId,String Crypto_token) {

        final KProgressHUD progressDialog = KProgressHUD.create(Activity_ResturantRegistrations.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();

        Log.e("Crypto_token", Crypto_token);
        final HashMap<String, String> postParams = new HashMap<String, String>();
        postParams.put("restaurant_name", Name);
        postParams.put("city_id", mCityId);
        postParams.put("email", Email);
        postParams.put("password", Password);
        postParams.put("restaurant_type", mTypeId);
        postParams.put("crypto_wallet_token", Crypto_token);


        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                ServerUtils.BASE_URL+"restaurant/register", new JSONObject(postParams),
                new com.android.volley.Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("ResponseNet", response + " null");

                        progressDialog.dismiss();



                        try {



                            if(response.getInt("ResponseCode")==422){
                                Toast.makeText(Activity_ResturantRegistrations.this,response.getJSONObject("errors").getJSONArray("email").getString(0),Toast.LENGTH_SHORT).show();

                            }else {


                                //Session.createLoginSession(jsonObject.getJSONObject("data").getString("restaurant_id"),
                       /*         jsonObject.getJSONObject("data").getString("restaurant_image"),
                                jsonObject.getJSONObject("data").getString("restaurant_name"),
                                jsonObject.getJSONObject("data").getString("restaurant_name"),
                                jsonObject.getJSONObject("data").getString("email"),
                                "",
                                jsonObject.getJSONObject("data").getString("api_token")
                        );

                        Intent i = new Intent(RastaurantRegisterActivity.this, UserSideActivity.class);
                        startActivity(i);
                        finish();*/

                                showPopup2();
                                Toast.makeText(Activity_ResturantRegistrations.this, response.getString("ResponseMsg"), Toast.LENGTH_SHORT).show();
                            }

                        }catch (Exception e){
                            Toast.makeText(Activity_ResturantRegistrations.this, e.getMessage(), Toast.LENGTH_SHORT).show();

                        }
                    }
                }, new com.android.volley.Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("Error" ,""+ error.getLocalizedMessage());
                Toast.makeText(Activity_ResturantRegistrations.this, ""+error.getLocalizedMessage(), Toast.LENGTH_SHORT).show();

                progressDialog.dismiss();
                Session.logout();
                Intent i = new Intent(getApplicationContext(), Activity_Intro_F.class);
                finish();
                startActivity(i);
            }
        }) {

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                params.put("Authorization","Bearer "+ Session.getAPIToken());
                return params;
            }
        };

        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(30000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        // Adding request to request queue
        requestQueue.add(jsonObjReq);
    }


    private void showPopup2() {
        // custom dialog
        final Dialog dialog = new Dialog(Activity_ResturantRegistrations.this);
        dialog.setContentView(R.layout.register_dialog);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(true);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);


        dialog.findViewById(R.id.done).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                finish();
            }
        });



        dialog.show();
    }

    private void generateWallet(String Id) {


        Log.e("Crypto_token",wallet.getAddress());
        // Toast.makeText(Activity_Login.this,wallet.getAddress(),Toast.LENGTH_SHORT).show();

    }

}