package com.bites.funds1.Activity;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.bites.funds1.API.UserAmountRequest;
import com.bites.funds1.Fragment.Fragment_About;
import com.bites.funds1.Fragment.Fragment_ContactUs;
import com.bites.funds1.Fragment.Fragment_Dashboard;
import com.bites.funds1.Fragment.Fragment_Home;
import com.bites.funds1.Fragment.Fragment_User_FundRises;
import com.bites.funds1.Fragment.Fragment_User_Profile;
import com.bites.funds1.Fragment.Fragment_User_Redeemded_VoucherList;
import com.bites.funds1.Fragment.Fragment_User_VoucherHistory;
import com.bites.funds1.R;
import com.bites.funds1.Utils.UserSession;
import com.bites.funds1.Utils.AppContants;
import com.bumptech.glide.Glide;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class Activity_UserSide extends AppCompatActivity {

    private UserSession Session;
    private RequestQueue requestQueue;
    private TextView bite_bought,usd_paid;
    public static LinearLayout bottom_bar;
    public static boolean IsBuyBITE = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.navigation_view_user_side);


        if (Build.VERSION.SDK_INT >= 21) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(getResources().getColor(R.color.colorPrimary));
        }

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setNavigationBarColor(getResources().getColor(R.color.colorPrimary));
        }

        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        Session = new UserSession(getApplicationContext());

        requestQueue = Volley.newRequestQueue(Activity_UserSide.this);//Creating the RequestQueue

        ImageView drawer_menu = findViewById(R.id.menu);
        ImageView user_img = findViewById(R.id.user_img);
        final TextView user_name = findViewById(R.id.user_name);
        bottom_bar = findViewById(R.id.bottom_bar);
        bite_bought = findViewById(R.id.bite_bought);
        usd_paid = findViewById(R.id.usd_paid);

        Glide.with(Activity_UserSide.this).load(Session.getProfilePic()).circleCrop().placeholder(R.drawable.placeholder).into(user_img);
        user_name.setText(Session.getFirstName() + " " +Session.getLastName());

        GetAmount();

        LinearLayout HOME = findViewById(R.id.home);
        LinearLayout LISTING = findViewById(R.id.listing);
        LinearLayout HISTORY = findViewById(R.id.history);
        LinearLayout PROFILE = findViewById(R.id.profile);
        LinearLayout ln_profile2 = findViewById(R.id.ln_profile2);
        LinearLayout ln_cr_voucher = findViewById(R.id.ln_cr_voucher);
        LinearLayout ln_rd_voucher = findViewById(R.id.ln_rd_voucher);
        LinearLayout ln_bite = findViewById(R.id.ln_bite);
        final ImageView logo = findViewById(R.id.logo);
        final TextView name = findViewById(R.id.name);
        LinearLayout ln_about = findViewById(R.id.ln_about);
        LinearLayout ln_contact = findViewById(R.id.ln_contact);
        LinearLayout ln_dashboard = findViewById(R.id.ln_dashboard);
        LinearLayout ln_transaction = findViewById(R.id.ln_transaction);

        final DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawer.openDrawer(Gravity.LEFT);
            }
        });

        addFragment(R.id.nav_host_fragment, new Fragment_Dashboard(), "Fragment");

        HOME.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                replaceFragment(R.id.nav_host_fragment, new Fragment_Dashboard(), "Fragment");
            }
        });

        findViewById(R.id.usd).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                replaceFragment(R.id.nav_host_fragment, new Fragment_User_FundRises(), "Fragment");
                drawer.closeDrawer(Gravity.LEFT);
            }
        });

        LISTING.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                IsBuyBITE = true;
                Intent i = new Intent(Activity_UserSide.this, Activity_FindRestaurantMap.class);
                startActivity(i);
            }
        });

        HISTORY.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                IsBuyBITE = false;
                Intent i = new Intent(Activity_UserSide.this, Activity_FindRestaurantMap.class);
                startActivity(i);
            }
        });

        PROFILE.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                replaceFragment(R.id.nav_host_fragment, new Fragment_User_Profile(), "Fragment");
            }
        });

        ln_bite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*findViewById(R.id.bottom_bar).setVisibility(View.GONE);
                logo.setVisibility(View.GONE);
                name.setVisibility(View.VISIBLE);
                name.setText("USER PAID BITE BOUGHT");*/
                Intent i = new Intent(Activity_UserSide.this, Activity_FindRestaurantMap.class);
                startActivity(i);

                //     replaceFragment(R.id.nav_host_fragment,new Bites_fragment(),"Fragment");
                drawer.closeDrawer(Gravity.LEFT);
            }
        });

        ln_about.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onClick(View view) {
                logo.setVisibility(View.GONE);
                name.setVisibility(View.VISIBLE);
                name.setText("ABOUT US");
                bottom_bar.setVisibility(View.GONE);
                replaceFragment(R.id.nav_host_fragment, new Fragment_About(), "Fragment");
                drawer.closeDrawer(Gravity.LEFT);
            }
        });
        ln_contact.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onClick(View view) {
                findViewById(R.id.bottom_bar).setVisibility(View.GONE);
                logo.setVisibility(View.GONE);
                name.setVisibility(View.VISIBLE);
                name.setText("CONTACT US");
                replaceFragment(R.id.nav_host_fragment, new Fragment_ContactUs(), "Fragment");
                drawer.closeDrawer(Gravity.LEFT);
            }
        });

        ln_dashboard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                findViewById(R.id.bottom_bar).setVisibility(View.VISIBLE);
                logo.setVisibility(View.VISIBLE);
                name.setVisibility(View.GONE);
                replaceFragment(R.id.nav_host_fragment, new Fragment_Dashboard(), "Fragment");
                drawer.closeDrawer(Gravity.LEFT);
            }
        });

        ln_profile2.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("RtlHardcoded")
            @Override
            public void onClick(View view) {
                findViewById(R.id.bottom_bar).setVisibility(View.GONE);
                replaceFragment(R.id.nav_host_fragment, new Fragment_User_Profile(), "Fragment");
                drawer.closeDrawer(Gravity.LEFT);
            }
        });

        ln_cr_voucher.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("RtlHardcoded")
            @Override
            public void onClick(View view) {
                IsBuyBITE = false;
                Intent i = new Intent(Activity_UserSide.this, Activity_FindRestaurantMap.class);
                startActivity(i);
                drawer.closeDrawer(Gravity.LEFT);
            }
        });

        ln_rd_voucher.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onClick(View view) {
                findViewById(R.id.bottom_bar).setVisibility(View.GONE);
                logo.setVisibility(View.GONE);
                name.setVisibility(View.VISIBLE);
                name.setText("REDEEMED VOUCHER LIST");
                drawer.closeDrawer(Gravity.LEFT);

                Fragment_User_Redeemded_VoucherList fragobj = new Fragment_User_Redeemded_VoucherList();
                Bundle bundle = new Bundle();
                bundle.putString("VouchersDetails", Fragment_Dashboard.active_voucher.getText().toString());
                fragobj.setArguments(bundle);
                replaceFragment(R.id.nav_host_fragment,fragobj, "Fragment");
            }
        });


        ln_transaction.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onClick(View view) {
                findViewById(R.id.bottom_bar).setVisibility(View.GONE);
                logo.setVisibility(View.GONE);
                name.setVisibility(View.VISIBLE);
                name.setText("REDEEMED VOUCHER HISTORY");
                replaceFragment(R.id.nav_host_fragment, new Fragment_User_VoucherHistory(), "Fragment");
                drawer.closeDrawer(Gravity.LEFT);
            }
        });

        findViewById(R.id.ln_profile).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showPopup2();
            }
        });

        findViewById(R.id.ln_home).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                logo.setVisibility(View.VISIBLE);
                name.setVisibility(View.GONE);
                replaceFragment(R.id.nav_host_fragment,new Fragment_Home(),"Fragment");
                drawer.closeDrawer(Gravity.LEFT);
            }
        });

        findViewById(R.id.ln_logout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Session.logout();
                Intent i = new Intent(Activity_UserSide.this, Activity_Intro_F.class);
                startActivity(i);
                finish();

            }
        });

    }


    protected void addFragment(@IdRes int containerViewId,
                               @NonNull Fragment fragment,
                               @NonNull String fragmentTag) {
        getSupportFragmentManager()
                .beginTransaction()
                .add(containerViewId, fragment, fragmentTag)
                .disallowAddToBackStack()
                .commit();
    }

    protected void replaceFragment(@IdRes int containerViewId,
                                   @NonNull Fragment fragment,
                                   @NonNull String fragmentTag) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(containerViewId, fragment, fragmentTag)
                .disallowAddToBackStack()
                .commit();

    }

    private void showPopup2() {
        // custom dialog
        final Dialog dialog = new Dialog(Activity_UserSide.this);
        dialog.setContentView(R.layout.login_dialog);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        dialog.findViewById(R.id.close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        dialog.findViewById(R.id.restaurant).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();



                Session.logout();
                Intent i = new Intent(Activity_UserSide.this, Activity_Intro_F.class);
                startActivity(i);
                finish();
            }
        });

        dialog.findViewById(R.id.user).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                Session.logout();
                Intent i = new Intent(Activity_UserSide.this, Activity_Intro_F.class);
                startActivity(i);
                finish();
            }
        });


        dialog.show();
    }


    private void GetAmount()  {

        final KProgressHUD progressDialog = KProgressHUD.create(Activity_UserSide.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();

        UserAmountRequest loginRequest = new UserAmountRequest(new Response.Listener<String>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(String response) {
                Log.e("Response", response + " null");
                progressDialog.dismiss();



                try {

                    JSONObject jsonObject = new JSONObject(response);

                    JSONObject jsonObject1 = jsonObject.getJSONObject("data");

                    bite_bought.setText(jsonObject1.getString("bite_bought") + " BITE bought" );
                    usd_paid.setText(jsonObject1.getString("usd_paid") + " USD Paid" );



                }catch (Exception e){

                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                progressDialog.dismiss();
                if (error instanceof ServerError)
                    Toast.makeText(Activity_UserSide.this, "Server Error", Toast.LENGTH_SHORT).show();
                else if (error instanceof TimeoutError)
                    Toast.makeText(Activity_UserSide.this, "Connection Timed Out", Toast.LENGTH_SHORT).show();
                else if (error instanceof NetworkError)
                    Toast.makeText(Activity_UserSide.this, "Bad Network Connection", Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                // params.put("Accept", "application/json");
                 params.put("Authorization","Bearer "+ Session.getAPIToken());
                return params;
            }};
        loginRequest.setTag("TAG");loginRequest.setShouldCache(false);
        requestQueue.add(loginRequest);

    }


    @Override
    protected void onResume() {
        super.onResume();

        GetAmount();
    }
}