package com.bites.funds1.Activity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.bites.funds1.API.RastaurantNewPasswordRequest;
import com.bites.funds1.API.UserNewPasswordRequest;
import com.bites.funds1.R;
import com.bites.funds1.Utils.UserSession;
import com.bites.funds1.Utils.AppContants;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class Activity_NewPassword extends AppCompatActivity {

    private TextView submit;
    private EditText Password,Email;
    private UserSession Session;
    private RequestQueue requestQueue;

    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_newpassword);


        if (Build.VERSION.SDK_INT >= 21) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(getResources().getColor(R.color.colorPrimary));
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setNavigationBarColor(getResources().getColor(R.color.colorPrimary));
        }

        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        final String EmailId;
        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if(extras == null) {
                EmailId= null;
            } else {
                EmailId= extras.getString("Email");
            }
        } else {
            EmailId= (String) savedInstanceState.getSerializable("Email");
        }



        submit = findViewById(R.id.submit);
        Email = findViewById(R.id.email);
        Password = findViewById(R.id.password);
        requestQueue = Volley.newRequestQueue(Activity_NewPassword.this);//Creating the RequestQueue

        Session = new UserSession(Activity_NewPassword.this);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if(Email.getText().toString().isEmpty()){
                    Toast.makeText(Activity_NewPassword.this,"Please Enter Your Password",Toast.LENGTH_SHORT).show();
                }else if(Password.getText().toString().isEmpty()){
                    Toast.makeText(Activity_NewPassword.this,"Please Enter Your Password",Toast.LENGTH_SHORT).show();
                }else if(!Password.getText().toString().equals(Email.getText().toString())){
                    Toast.makeText(Activity_NewPassword.this,"Your Password does not match",Toast.LENGTH_SHORT).show();
                }else {
                    if(Session.getLoginType().equals(AppContants.TYPEUSER)){
                        UserLogin(EmailId,Email.getText().toString(),Password.getText().toString());
                    }else {
                        RastaurantLogin(EmailId,Email.getText().toString(),Password.getText().toString());

                    }
                }



            }
        });


        findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }


    private void UserLogin(String EmailId,String Email, String Password)  {

        final KProgressHUD progressDialog = KProgressHUD.create(Activity_NewPassword.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();

        UserNewPasswordRequest loginRequest = new UserNewPasswordRequest(EmailId,Email,Password,new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("Response", response + " null");
                progressDialog.dismiss();


                JSONObject jsonObject = null;
                try {
                    jsonObject = new JSONObject(response);


                    Intent i = new Intent(Activity_NewPassword.this, Activity_Login.class);
                    startActivity(i);
                    finish();

                    Toast.makeText(Activity_NewPassword.this,jsonObject.getString("ResponseMsg"),Toast.LENGTH_SHORT).show();

                }catch (Exception e){

                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                progressDialog.dismiss();
                if (error instanceof ServerError)
                    Toast.makeText(Activity_NewPassword.this, "Server Error", Toast.LENGTH_SHORT).show();
                else if (error instanceof TimeoutError)
                    Toast.makeText(Activity_NewPassword.this, "Connection Timed Out", Toast.LENGTH_SHORT).show();
                else if (error instanceof NetworkError)
                    Toast.makeText(Activity_NewPassword.this, "Bad Network Connection", Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                // params.put("Accept", "application/json");
               // params.put("Authorization","Bearer "+ Session.getAPIToken());
                return params;
            }};
        loginRequest.setTag("TAG");loginRequest.setShouldCache(false);
        requestQueue.add(loginRequest);

    }

    private void RastaurantLogin(String EmailId,String Email, String Password)  {

        final KProgressHUD progressDialog = KProgressHUD.create(Activity_NewPassword.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();

        RastaurantNewPasswordRequest loginRequest = new RastaurantNewPasswordRequest(EmailId,Email,Password,new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("Response", response + " null");
                progressDialog.dismiss();


                JSONObject jsonObject = null;
                try {
                    jsonObject = new JSONObject(response);

                    Intent i = new Intent(Activity_NewPassword.this, Activity_Login.class);
                    startActivity(i);
                    finish();

                    Toast.makeText(Activity_NewPassword.this,jsonObject.getString("ResponseMsg"),Toast.LENGTH_SHORT).show();

                }catch (Exception e){

                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                progressDialog.dismiss();
                if (error instanceof ServerError)
                    Toast.makeText(Activity_NewPassword.this, "Server Error", Toast.LENGTH_SHORT).show();
                else if (error instanceof TimeoutError)
                    Toast.makeText(Activity_NewPassword.this, "Connection Timed Out", Toast.LENGTH_SHORT).show();
                else if (error instanceof NetworkError)
                    Toast.makeText(Activity_NewPassword.this, "Bad Network Connection", Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                // params.put("Accept", "application/json");
                // params.put("Authorization","Bearer "+ Session.getAPIToken());
                return params;
            }};
        loginRequest.setTag("TAG");loginRequest.setShouldCache(false);
        requestQueue.add(loginRequest);

    }


}